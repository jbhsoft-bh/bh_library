﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Deployment.Application;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Windows.Forms;

namespace BH_Library.Utils
{
    public static class Extension
    {
        public static List<T> ConvertToEntity<T>(this DataTable dataTable) where T : new()
        {
            List<T> resultT = new List<T>();

            foreach (DataRow dRow in dataTable.Rows)
            {
                resultT.Add(ConvertToEntity<T>(dRow));
            }
            return resultT;
        }

        private static T ConvertToEntity<T>(this DataRow tableRow) where T : new()
        {
            // Create a new type of the entity I want
            Type t = typeof(T);
            T returnObject = new T();

            foreach (DataColumn col in tableRow.Table.Columns)
            {
                string colName = col.ColumnName;

                // Look for the object's property with the columns name, ignore case
                PropertyInfo pInfo = t.GetProperty(colName.ToLower(),
                    BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);

                // did we find the property ?
                if (pInfo != null)
                {
                    object val = tableRow[colName];

                    // is this a Nullable<> type
                    bool IsNullable = (Nullable.GetUnderlyingType(pInfo.PropertyType) != null);
                    if (IsNullable)
                    {
                        if (val is System.DBNull)
                        {
                            val = null;
                        }
                        else
                        {
                            // Convert the db type into the T we have in our Nullable<T> type
                            val = System.Convert.ChangeType
                    (val, Nullable.GetUnderlyingType(pInfo.PropertyType));
                        }
                    }
                    else
                    {
                        // Convert the db type into the type of the property in our entity
                        if (val is System.DBNull)
                            val = null;
                        else
                            val = System.Convert.ChangeType(val, pInfo.PropertyType);
                    }
                    // Set the value of the property with the value from the db
                    pInfo.SetValue(returnObject, val, null);
                }
            }

            // return the entity object with values
            return returnObject;
        }

        /// <summary>
        ///     List -> Data (List에 자료가 하나 이상이 있어야 함.. 없으면 NULL 반환) Author by 근하
        /// </summary>
        /// <param name="list"></param>
        /// <param name="cloneTableWhenEmpty">list의 Count가 0일때 구조를 복제할 테이블입니다.</param>
        /// <returns></returns>
        public static DataTable ToTable<T>(this IEnumerable<T> list, DataTable cloneTableWhenEmpty = null)
        {
            DataTable dt = null;

            if (typeof(T).Equals(typeof(DataRow)))
            {
                int rowCount = list.Count();

                if (rowCount > 0)
                {
                    dt = (list.First() as DataRow).Table.Clone();

                    foreach (T row in list)
                    {
                        DataRow dataRow = row as DataRow;
                        if (dataRow != null)
                        {
                            dt.ImportRow(dataRow);
                        }
                    }
                }
                else
                {
                    if (cloneTableWhenEmpty != null)
                        return cloneTableWhenEmpty.Clone();
                    else
                        return null;
                }
            }
            else
            {
                dt = new DataTable();
                Type listType = typeof(T);
                MemberInfo[] miArray = listType.GetMembers(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);

                foreach (MemberInfo mi in miArray)
                {
                    if (mi.MemberType == MemberTypes.Property)
                    {
                        PropertyInfo pi = mi as PropertyInfo;
                        Type t = pi.PropertyType;
                        if (pi.PropertyType.IsValueType)
                        {
                            t = Nullable.GetUnderlyingType(pi.PropertyType);
                            if (t == null)
                            {
                                t = pi.PropertyType;
                            }

                            dt.Columns.Add(pi.Name, t);
                        }
                        else
                        {
                            if (new[] { typeof(string), typeof(DateTime) }.Contains(pi.PropertyType))
                            {
                                dt.Columns.Add(pi.Name, pi.PropertyType);
                            }
                        }
                    }
                    else if (mi.MemberType == MemberTypes.Field)
                    {
                        FieldInfo fi = mi as FieldInfo;
                        Type t = fi.FieldType;
                        if (fi.FieldType.IsValueType)
                        {
                            t = Nullable.GetUnderlyingType(fi.FieldType);
                            if (t == null)
                            {
                                t = fi.FieldType;
                            }

                            dt.Columns.Add(fi.Name, t);
                        }
                        else
                        {
                            if (new[] { typeof(string), typeof(DateTime) }.Contains(fi.FieldType))
                            {
                                dt.Columns.Add(fi.Name, fi.FieldType);
                            }
                        }
                    }
                }

                foreach (object rec in list)
                {
                    int i = 0;
                    object[] fieldValues = new object[dt.Columns.Count];
                    foreach (DataColumn c in dt.Columns)
                    {
                        MemberInfo mi = listType.GetMember(c.ColumnName)[0];

                        if (mi.MemberType == MemberTypes.Property)
                        {
                            PropertyInfo pi = mi as PropertyInfo;
                            object val = pi.GetValue(rec, null);
                            fieldValues[i] = (val == null ? DBNull.Value : val);
                        }
                        else if (mi.MemberType == MemberTypes.Field)
                        {
                            FieldInfo fi = mi as FieldInfo;
                            object val = fi.GetValue(rec);
                            fieldValues[i] = (val == null ? DBNull.Value : val);
                        }

                        i++;
                    }
                    dt.Rows.Add(fieldValues);
                }
            }

            return dt;
        }

        #region ValidationCheck

        /// <summary>
        ///     올바른 사업자번호 형식이 맞는지 검사합니다.
        /// </summary>
        /// <param name="maskedBusinessNo"></param>
        /// <returns></returns>
        public static bool IsValidBusinessNoMask(this string maskedBusinessNo)
        {
            bool result = false;
            if (!string.IsNullOrWhiteSpace(maskedBusinessNo))
            {
                Regex regex = new Regex(@"\d{3}-\d{2}-\d{5}");
                if (regex.IsMatch(maskedBusinessNo))
                {
                    result = true;
                }
            }

            return result;
        }

        /// <summary>
        ///     올바른 주민번호 형식이 맞는지 검사합니다.
        /// </summary>
        /// <param name="socialSecurityNo"></param>
        /// <returns></returns>
        public static bool IsValidSocialSecurityNoMask(this string socialSecurityNo)
        {
            bool result = false;
            if (!string.IsNullOrWhiteSpace(socialSecurityNo))
            {
                Regex regex = new Regex(@"\d{6}-\d{7}");
                if (regex.IsMatch(socialSecurityNo))
                {
                    result = true;
                }
            }

            return result;
        }

        /// <summary>
        ///     문자열이 이메일주소의 형식에 맞는지 검사하여 그 결과를 반환합니다.
        /// </summary>
        /// <param name="emailAddress">대상문자열</param>
        /// <param name="showResultOnShowMessagebox">실패시 메시지박스를 보여줄지 여부 true:보여줌, false:보여주지않음</param>
        /// <returns>
        ///     true: 유효함
        ///     false: 유효하지않음
        /// </returns>
        public static bool IsValidEmail(this string emailAddress, bool showResultOnShowMessagebox = false)
        {
            bool result = Regex.IsMatch(emailAddress, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
            if (showResultOnShowMessagebox && result == false)
            {
                MessageBox.Show("올바르지 않은 이메일 주소 입니다.");
            }
            return result;
        }

        /// <summary>
        ///     유효한 사업자번호인지 검사합니다.
        /// </summary>
        /// <param name="bizNo">사업자번호 문자열</param>
        /// <returns></returns>
        public static bool IsValidBusinessNo(this string bizNo)
        {
            if (bizNo == null)
                return false;

            bizNo = bizNo.Replace(" ", "");
            bizNo = bizNo.Replace("-", "");

            if (bizNo.Length != 10)
                return false;

            if (!bizNo.IsNumeric())
                return false;

            int nBCK = 0;
            nBCK += int.Parse(bizNo.Substring(0, 1)) * 1;
            nBCK += int.Parse(bizNo.Substring(1, 1)) * 3;
            nBCK += int.Parse(bizNo.Substring(2, 1)) * 7;
            nBCK += int.Parse(bizNo.Substring(3, 1)) * 1;
            nBCK += int.Parse(bizNo.Substring(4, 1)) * 3;
            nBCK += int.Parse(bizNo.Substring(5, 1)) * 7;
            nBCK += int.Parse(bizNo.Substring(6, 1)) * 1;
            nBCK += int.Parse(bizNo.Substring(7, 1)) * 3;
            nBCK += int.Parse(bizNo.Substring(8, 1)) * 5 / 10;
            nBCK += int.Parse(bizNo.Substring(8, 1)) * 5 % 10;
            nBCK += int.Parse(bizNo.Substring(9, 1));

            if (nBCK % 10 == 0)
                return true;
            else
                return false;
        }

        /// <summary>
        ///     유효한 법인번호인지 검사합니다.
        /// </summary>
        /// <param name="corporationNo">법인번호 문자열</param>
        /// <returns></returns>
        public static bool IsValidCorporationNo(this string corporationNo)
        {
            if (corporationNo == null)
                return false;

            corporationNo = corporationNo.Replace(" ", "");
            corporationNo = corporationNo.Replace("-", "");

            if (corporationNo.Length != 13)
                return false;

            if (!corporationNo.IsNumeric())
                return false;

            int[] arr_rule = { 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2 };
            int total = 0;

            for (int i = 0; i < 12; i++)
                total += int.Parse(corporationNo.Substring(i, 1)) * arr_rule[i];

            if ((10 - (total % 10)) == int.Parse(corporationNo.Substring(12, 1)))
                return true;
            else
                return false;
        }

        /// <summary>
        ///     유효한 주민등록번호인지 검사합니다.
        /// </summary>
        /// <param name="residentNo">주민등록번호 문자열</param>
        /// <returns></returns>
        public static bool IsValidSocialSecurityNo(this string residentNo)
        {
            if (residentNo == null)
                return false;

            residentNo = residentNo.Replace(" ", ""); // 공백 제거
            residentNo = residentNo.Replace("-", ""); // 문자 '-' 제거

            // 자릿수 체크
            if (residentNo.Length != 13)
                return false;

            if (!residentNo.IsNumeric())
                return false;

            int sum = 0;
            for (int i = 0; i < residentNo.Length - 1; i++)
            {
                char c = residentNo[i];

                //숫자로 이루어져 있는가?
                if (!char.IsNumber(c))
                {
                    return false;
                }
                else
                {
                    if (i < residentNo.Length)
                    {
                        //지정된 숫자로 각 자리를 나눈 후 더한다.
                        sum += int.Parse(c.ToString()) * ((i % 8) + 2);
                    }
                }
            }

            // 검증코드와 결과 값이 같은가?
            if (!((((11 - (sum % 11)) % 10).ToString()) == ((residentNo[residentNo.Length - 1]).ToString())))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        ///     유효한 외국인등록번호인지 검사합니다.
        /// </summary>
        /// <param name="foreignerNo">외국인등록번호 문자열</param>
        /// <returns></returns>
        public static bool IsValidForeignerNo(this string foreignerNo)
        {
            if (foreignerNo == null)
                return false;

            foreignerNo = foreignerNo.Replace(" ", ""); // 공백 제거
            foreignerNo = foreignerNo.Replace("-", ""); // 문자 '-' 제거

            // 자릿수 체크
            if (foreignerNo.Length != 13)
                return false;

            if (!foreignerNo.IsNumeric())
                return false;

            if (int.Parse(foreignerNo.Substring(7, 2)) % 2 != 0)
                return false;

            int sum = 0;
            for (int i = 0; i < 12; i++)
            {
                sum += int.Parse(foreignerNo.Substring(i, 1)) * ((i % 8) + 2);
            }

            if ((((11 - (sum % 11)) % 10 + 2) % 10) == int.Parse(foreignerNo.Substring(12, 1)))
                return true;
            else
                return false;
        }

        /// <summary>
        ///     문자열이 숫자인지 반환합니다.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsNumeric(this string value)
        {
            if (value == null)
            {
                return false;
            }
            else
            {
                decimal parseValue = decimal.Zero;
                return decimal.TryParse(value, out parseValue);
            }
        }

        #endregion ValidationCheck

        #region IEnumerable

        /// <summary>
        ///     리스트의 문자열을 지정한 구분자로 구분하여 하나의 문자열로 반환합니다.
        /// </summary>
        /// <param name="strings">하나의 문자열로 만들 문자열 리스트입니다.</param>
        /// <param name="seperator">구분자</param>
        /// <param name="head">문자열 앞부분에 붙일 head 문자열</param>
        /// <param name="tail">문자열 뒷부분에 붙일 tail 문자열</param>
        /// <returns></returns>
        public static string Flatten(this IEnumerable<string> strings, string seperator, string head = "", string tail = "")
        {
            // If the collection is null, or if it contains zero elements,
            // then return an empty string.
            if (strings == null || strings.Count() == 0)
                return String.Empty;

            // Build the flattened string
            var flattenedString = new StringBuilder();

            flattenedString.Append(head);
            foreach (var s in strings)
                flattenedString.AppendFormat("{0}{1}", s, seperator); // Add each element with the given seperator.
            flattenedString.Remove(flattenedString.Length - seperator.Length, seperator.Length); // Remove the last seperator
            flattenedString.Append(tail);

            // Return the flattened string
            return flattenedString.ToString();
        }

        #endregion IEnumerable



        #region decimal

        public static decimal RoundOrTruncate(this decimal value, bool isRound, int decimals)
        {
            if (isRound)
                return Math.Round(value, decimals, MidpointRounding.AwayFromZero);
            else
            {
                decimal tempValue = value * (decimal)Math.Pow(10, decimals);
                tempValue = Math.Truncate(tempValue);
                return tempValue / (decimal)Math.Pow(10, decimals);
            }
        }

        #endregion decimal

        #region string

        public static string GetBusinessNoMasked(this string businessNo)
        {
            if (string.IsNullOrWhiteSpace(businessNo))
                return businessNo;

            StringBuilder sb = new StringBuilder();
            for (int idx = 0; idx < businessNo.Length; idx++)
            {
                sb.Append(businessNo[idx]);
                if (idx == 2 || idx == 4)
                    sb.Append('-');
            }

            return sb.ToString();
        }

        public static string GetSocialSecurityNoMasked(this string socialSecurityNo)
        {
            if (string.IsNullOrWhiteSpace(socialSecurityNo))
                return socialSecurityNo;

            StringBuilder sb = new StringBuilder();
            for (int idx = 0; idx < socialSecurityNo.Length; idx++)
            {
                sb.Append(socialSecurityNo[idx]);
                if (idx == 5)
                    sb.Append('-');
            }

            return sb.ToString();
        }

        public static string MultiReplace(this string value, List<string> oldValues, string newValue)
        {
            foreach (string oldValue in oldValues)
                value = value.Replace(oldValue, newValue);

            return value;
        }

        #endregion string

        #region object

        /// <summary>
        ///     DefaultValueAttribute가 붙어있는 프로퍼티에 DefaultValue값을 셋팅합니다.
        /// </summary>
        /// <param name="object"></param>
        /// <param name="overwrite">해당 Property에 이미 값이 있더라도 덮어쓸지를 결정합니다.</param>
        public static void DefaultValueToProperty(this object @object, bool overwrite = false)
        {
            Type objectType = @object.GetType();

            var propertyInfos = objectType.GetProperties().Where(x => x.CanWrite);
            foreach (PropertyInfo propertyInfo in propertyInfos)
            {
                string originalValue = propertyInfo.GetValue(@object, null) as string;
                if ((overwrite || string.IsNullOrEmpty(originalValue)))
                    propertyInfo.SetValue(@object, propertyInfo.GetDefaultValueAttributeValue(), null);
            }
        }

        #endregion object

        #region MemberInfo

        public static T GetCustomAttribute<T>(this MemberInfo memberInfo) where T : Attribute
        {
            List<T> attributes = memberInfo.GetCustomAttributes<T>();
            if (attributes.Any())
                return attributes.First();
            else
                return null;
        }

        public static List<T> GetCustomAttributes<T>(this MemberInfo memberInfo) where T : Attribute
        {
            object[] attributes = memberInfo.GetCustomAttributes(typeof(T), false);
            if (attributes == null || attributes.Length == 0)
                return new List<T>();
            else
                return attributes.ToList().ConvertAll(x => (T)x);
        }

        #endregion MemberInfo

        #region PropertyInfo

        public static object GetDefaultValueAttributeValue(this PropertyInfo propertyInfo)
        {
            DefaultValueAttribute attribute = propertyInfo.GetCustomAttribute<DefaultValueAttribute>();

            if (attribute != null)
            {
                Type defaultValueType = attribute.Value.GetType();
                Type propertyType = propertyInfo.PropertyType;
                if (defaultValueType.Equals(propertyType) == false)
                {
                    string msgFormat = "프로퍼티의 타입({0})과 DefaultValue의 타입({1})이 일치하지 않습니다.";
                    throw new Exception(string.Format(msgFormat, propertyType.Name, defaultValueType.Name));
                }

                return attribute.Value;
            }
            else
                return null;
        }

        #endregion PropertyInfo

        #region Type

        /// <summary>
        ///     해당 Property의 타입이 제네릭리스트인지 반환합니다.
        /// </summary>
        /// <param name="propertyInfo"></param>
        /// <returns></returns>
        public static bool IsGenericList(this Type type)
        {
            return
                type.IsGenericType &&
                type.GetGenericTypeDefinition().Equals(typeof(List<>));
        }

        #endregion Type

        #region Exception

        /// <summary>
        /// 예외메시지를 가져옵니다.(내부 예외가 있을 경우 내부예외 메세지 반환)
        /// </summary>
        /// <param name="exception"></param>
        /// <returns></returns>
        public static string GetInnerMessage(this Exception exception)
        {
            string message = string.Empty;

            if (exception != null)
            {
                if (exception.InnerException != null)
                    message = exception.InnerException.Message;
                else
                    message = exception.Message;
            }

            return message;
        }

        #endregion Exception

        public static void SetColumnsOrder(this DataTable table, params string[] columnNames)
        {
            int columnIndex = 0;
            foreach (var columnName in columnNames)
            {
                if (table.Columns.Contains(columnName))
                {
                    table.Columns[columnName].SetOrdinal(columnIndex);
                    columnIndex++;
                }
            }
        }

        public static string GetQueryStringParameters(this string paramName)
        {
            Dictionary<string, string> GetParams = GetQueryStringParameters();
            return GetParams[paramName].ToStringEx();
        }

        public static Dictionary<string, string> GetQueryStringParameters()
        {
            Dictionary<string, string> nameValueTable = new Dictionary<string, string>();

            try
            {
                if (ApplicationDeployment.IsNetworkDeployed)
                {
                    string url = AppDomain.CurrentDomain.SetupInformation.ActivationArguments.ActivationData[0];
                    string queryString = ApplicationDeployment.CurrentDeployment.ActivationUri.Query;
                    if (string.Empty == queryString) return (nameValueTable);

                    int nIndex = queryString.IndexOf("?");

                    if (nIndex > -1) queryString = queryString.Remove(nIndex, 1);
                    queryString = HttpUtility.UrlDecode(queryString);
                    string[] nameValuePairs = queryString.Split('&');
                    if (nameValuePairs != null && nameValuePairs.Length > 0)
                    {
                        foreach (string pair in nameValuePairs)
                        {
                            string[] vars = pair.Split('=');
                            if (!nameValueTable.ContainsKey(vars[0]))
                            {
                                nameValueTable.Add(vars[0], vars[1]);
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
            }

            return (nameValueTable);
        }
    }
}