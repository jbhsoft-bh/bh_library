﻿using System.Net;

namespace BH_Library.Utils
{
    public class NetworkUtil
    {
        /// <summary>
        /// 자신의 IP인지 여부
        /// </summary>
        /// <param name="Ip"></param>
        /// <returns></returns>
        public static bool IsLocaIp(string Ip)
        {
            bool retVal = false;

            // Then using host name, get the IP address list..
            IPHostEntry ipEntry = System.Net.Dns.GetHostEntry(Dns.GetHostName());
            IPAddress[] addr = ipEntry.AddressList;
            for (int i = 0; i < addr.Length; i++)
            {
                if (addr[i].ToString() == Ip) retVal = true;
            }
            return retVal;
        }
    }
}