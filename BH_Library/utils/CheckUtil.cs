﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace BH_Library.Utils
{
    /// <summary>
    /// 유효성 검사를 위한 메서드를 제공합니다.
    /// </summary>
    public class CheckUtil
    {
        /// <summary>
        /// 주민등록번호의 유효성을 체크합니다.
        /// </summary>
        /// <param name="regNo1">주민번호 앞자리</param>
        /// <param name="regNo2">주민번호 뒷자리</param>
        /// <returns>유효성 체크 결과를 나타내는 bool값 입니다.</returns>
        public static bool CheckRegNo(string regNo1, string regNo2)
        {
            bool bRet = false;

            if (regNo1.Length == 6 && regNo2.Length == 7)
            {
                bRet = CheckRegNo(regNo1 + regNo2);
            }

            return bRet;
        }

        /// <summary>
        /// 해당 텍스가 지정한 byte를 초과하는지 여부를 확인하여 반환합니다.
        /// </summary>
        /// <param name="originText">검사할 문자열</param>
        /// <param name="size">최대 byte 크기</param>
        /// <returns>true: 초과안함
        /// false: 초과함
        /// </returns>
        public static bool CheckOverByteSize(string originText, int size)
        {
            string ret = originText.ToStringEx();
            int inputTextCnt = Encoding.UTF8.GetByteCount(ret);
            if (inputTextCnt > size)
                return false;
            return true;
        }

        /// <summary>
        /// 주민등록번호의 유효성을 체크합니다.
        /// </summary>
        /// <param name="regNo">유효성을 체크할 주민등록번호('-'는 입력하지 않습니다)입니다.</param>
        /// <returns>유효성 체크 결과를 나타내는 bool값입니다.</returns>
        public static bool CheckRegNo(string regNo)
        {
            // 공백 제거
            regNo = regNo.Replace(" ", "");

            // 문자 '-' 제거
            regNo = regNo.Replace("-", "");

            // 자릿수 체크
            if (regNo.Length != 13)
            {
                return false;
            }

            int sum = 0;

            for (int i = 0; i < regNo.Length - 1; i++)
            {
                char c = regNo[i];

                //숫자로 이루어져 있는가?
                if (!char.IsNumber(c))
                {
                    return false;
                }
                else
                {
                    if (i < regNo.Length)
                    {
                        //지정된 숫자로 각 자리를 나눈 후 더한다.
                        sum += int.Parse(c.ToString()) * ((i % 8) + 2);
                    }
                }
            }

            // 검증코드와 결과 값이 같은가?
            if (!((((11 - (sum % 11)) % 10).ToString()) == ((regNo[regNo.Length - 1]).ToString())))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// 사업자번호의 유효성을 체크합니다.
        /// </summary>
        /// <param name="bizNo1">사업자번호 앞자리(3자리)</param>
        /// <param name="bizNo2">사업자번호 중간자리(2자리)</param>
        /// <param name="bizNo3">사업자번호 뒷자리(5자리)</param>
        /// <returns>유효성 체크 결과를 나타내는 bool값 입니다.</returns>
        public static bool CheckBizNo(string bizNo1, string bizNo2, string bizNo3)
        {
            bool bRet = false;

            if (bizNo1.Length == 3 && bizNo2.Length == 2 && bizNo3.Length == 5)
            {
                bRet = CheckBizNo(bizNo1 + bizNo2 + bizNo3);
            }

            return bRet;
        }

        /// <summary>
        /// 사업자번호의 유효성을 체크합니다.
        /// </summary>
        /// <param name="bizNo">사업자번호 (10자리)</param>
        /// <returns>유효성 체크 결과를 나타내는 bool값 입니다.</returns>
        public static bool CheckBizNo(string bizNo)
        {
            bool bRet = false;

            int nBCK = 0;

            if (bizNo.Length != 10)
            {
                bRet = false;
            }
            else
            {
                nBCK += int.Parse(bizNo.ToString().Substring(0, 1)) * 1;
                nBCK += int.Parse(bizNo.ToString().Substring(1, 1)) * 3;
                nBCK += int.Parse(bizNo.ToString().Substring(2, 1)) * 7;
                nBCK += int.Parse(bizNo.ToString().Substring(3, 1)) * 1;
                nBCK += int.Parse(bizNo.ToString().Substring(4, 1)) * 3;
                nBCK += int.Parse(bizNo.ToString().Substring(5, 1)) * 7;
                nBCK += int.Parse(bizNo.ToString().Substring(6, 1)) * 1;
                nBCK += int.Parse(bizNo.ToString().Substring(7, 1)) * 3;
                nBCK += int.Parse(bizNo.ToString().Substring(8, 1)) * 5 / 10;
                nBCK += int.Parse(bizNo.ToString().Substring(8, 1)) * 5 % 10;
                nBCK += int.Parse(bizNo.ToString().Substring(9, 1));
            }

            if (nBCK % 10 == 0)
            {
                bRet = true;
            }

            return bRet;
        }

        /// <summary>
        /// 날짜 유효성 체크
        /// </summary>
        /// <param name="year">년</param>
        /// <param name="month">월</param>
        /// <param name="day">일</param>
        /// <returns>날짜의 유효성 체크 결과를 나타내는 bool값 입니다.</returns>
        public static bool CheckDate(int year, int month, int day)
        {
            bool bRet = false;

            if (!(year > 1000 && year <= 9999) /*1000~ 9999 까지*/)
            {
                return bRet;
            }

            if (!(month > 0 && month <= 12) /*1~ 12 까지*/)
            {
                return bRet;
            }

            int to = DateTime.DaysInMonth(year, month);

            if (!(day > 0 && day <= to) /*1~ 마지막 일까지*/)
            {
                return bRet;
            }

            return true;
        }

        /// <summary>
        /// 지정한 범위 내의 값인지를 체크합니다
        /// </summary>
        /// <typeparam name="T">검사할 Type</typeparam>
        /// <param name="min">범위의 최저값</param>
        /// <param name="max">범위의 최대값</param>
        /// <param name="target">검사할 값</param>
        /// <returns>범위 내의 값인지 체크한 결과를 나타내는 bool값 입니다.</returns>
        public static bool BL<T>(T min, T max, T target)
        {
            bool bRet = false;

            try
            {
                bRet = (Comparer<T>.Default.Compare(min, target) <= 0 && Comparer<T>.Default.Compare(target, max) <= 0);
            }
            catch
            {
            }

            return bRet;
        }

        /// <summary>
        /// 입력한 월이 어떤 년도에 속하는지 체크합니다(중간법인일 경우 사용)
        /// </summary>
        /// <param name="month">조회하고자 하는 월</param>
        /// <param name="startDate">회계시작일자</param>
        /// <param name="endDate">회계종료일자</param>
        /// <returns>해당월의 소속년도를 반환, 소속년도를 찾지못하였을 경우 string.Empty를 반환</returns>
        public static string InMonthFromYear(string month, string startDate, string endDate)
        {
            string fromYear = string.Empty;

            if (month.Length > 0 && startDate.Length >= 6 && endDate.Length >= 6)
            {
                int startDateYearMonth = startDate.Substring(0, 6).ToIntEx();
                int endDateYearMonth = endDate.Substring(0, 6).ToIntEx();
                int intMonth = month.ToIntEx();

                if (startDateYearMonth > 0 && endDateYearMonth > 0 && BL<int>(1, 12, intMonth))
                {
                    int startDateYear = startDate.Substring(0, 4).ToIntEx();
                    int endDateYear = endDate.Substring(0, 4).ToIntEx();

                    if (BL<int>(startDateYearMonth, endDateYearMonth, (startDateYear * 100 + intMonth)))
                    {
                        fromYear = startDateYear.ToString();
                    }
                    else if (BL<int>(startDateYearMonth, endDateYearMonth, (endDateYear * 100 + intMonth)))
                    {
                        fromYear = endDateYear.ToString();
                    }
                }
            }

            return fromYear;
        }

        /// <summary>
        /// 해당 Row가 완전히 비어 있는 Row 인지를 판단합니다.(iqxtragrid에 의해 성성된 빈 row인지 판단)
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public static bool CheckEmptyRow(DataRow row)
        {
            bool blResult = true;

            foreach (DataColumn col in row.Table.Columns)
            {
                if (string.IsNullOrEmpty(row[col.ColumnName].ToStringNullEx()) == false)
                {
                    blResult = false;
                    break;
                }
            }
            return blResult;
        }

        /// <summary>
        /// 그리드 뷰에서 현재 행이 필수값을 모두 채워서
        /// Insert 또는 Update를 할 수 있는 상태인지
        /// 또는 행이 모두 비어 있는 상태인지를 확인합니다.
        /// </summary>
        /// <param name="row">대상이 대는 DataRow</param>
        /// <param name="KeyColumns">필수값인 ColumnName 리스트</param>
        /// <param name="AllCheck">true: 필수값 상관없이 모든 컬럼을 체크하여 비어 있는 값이 있는지 확인
        /// false: 필수값인 컬럼만 검사하여 비어 있는 값이 있는지 확인</param>
        /// <returns>true: Insert Or Update 가능(필요한 컬럼에 모두 값이 있음)
        /// false: Insert Or Update 불가능(필요한 컬럼 중 일부 또는 모두 값이 없음)</returns>
        public static bool CheckEnableInsertOrUpdate(DataRow row, List<string> KeyColumns, bool AllCheck = false)
        {
            if (AllCheck)
            {
                foreach (DataColumn column in row.Table.Columns)
                {
                    if (string.IsNullOrEmpty(row[column.ColumnName].ToStringEx()))
                        return false;
                }
                return true;
            }
            else
            {
                foreach (string keyColumn in KeyColumns)
                {
                    if (string.IsNullOrEmpty(row[keyColumn].ToStringEx()))
                        return false;
                }
                return true;
            }
        }

        /// <summary>
        /// 파라메터로 입력 받은 값이 숫자인지 확인합니다.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static bool CheckNumber(object obj)
        {
            bool blResult = false;
            decimal _decimal = 0m;
            blResult = decimal.TryParse(obj.ToString().Trim(), out _decimal);
            return blResult;
        }

        /// <summary>
        /// 사업자등록번호, 법인등록번호, 주민번호, 외국인등록번호 유효성 검사
        /// </summary>
        /// <param name="str"></param>
        /// <returns>01 사업자등록번호, 02 주민등록번호 , 03 외국인등록번호, 04 법인등록번호</returns>
        public static string GetBSCFNUMKind(string str)
        {
            string strResult = "";
            str = str.ToString();
            str = str.Replace(" ", "").Replace("-", "");
            //사업자등록번호
            if (str.Length == 10)
            {
                if (IsAvailableCNO(str))
                    strResult = "01";
            }
            //법인등록번호 또는 주민등록번호 또는 외국인 등록번호
            else if (str.Length == 13)
            {
                if (IsAvailableRRN(str))
                    strResult = "02";
                else if (IsAvailableCorporNo(str))
                    strResult = "04";
                else if (IsAvailableForeignNO(str))
                    strResult = "03";
                else if (str == "9999999999999")
                    strResult = "99";
            }
            return strResult;
        }

        public static bool IsCorpNum(string str)
        {
            bool result = false;
            str = str.ToStringEx();
            str = str.Replace(" ", "").Replace("-", "");
            if (IsAvailableCorporNo(str))
                result = true;
            return result;
        }

        /// <summary>
        /// 사업자등록번호 유효성 검사
        /// </summary>
        /// <param name="strCustID"></param>
        /// <returns></returns>
        private static bool IsAvailableCNO(string strCustID)
        {
            try
            {
                strCustID = strCustID.Replace(" ", "").Replace("-", "");
                //곱할 수
                string strCheckID = "137137135";
                string CustID = strCustID.Replace("-", "").Trim();
                //마지막 번호와 체크해야함
                Decimal iLast = Convert.ToDecimal(CustID.Substring(9, 1));
                Decimal ret = 0;
                Decimal ick = 0;
                Decimal iCust = 0;
                for (int i = 0; i < 9; i++)
                {
                    ick = Convert.ToInt32(strCheckID.Substring(i, 1));
                    iCust = Convert.ToInt32(CustID.Substring(i, 1));
                    if (i < 8)
                        ret = ret + (ick * iCust);
                    else if (i == 8)
                        ret = ret + Decimal.Truncate((ick * iCust) / 10) + Decimal.Remainder((ick * iCust), 10);
                }

                ret = Decimal.Remainder(ret, 10);
                if (ret == 0) ret = 0;
                else ret = 10 - ret;

                if (ret == iLast)
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// 법인등록번호 유효성 검사
        /// </summary>
        /// <param name="corp_no"></param>
        /// <returns></returns>
        private static bool IsAvailableCorporNo(string strCorp)
        {
            strCorp = strCorp.Replace(" ", "").Replace("-", "");
            if (strCorp.Length != 13)
                return false;
            int sum = 0;
            for (int i = 0; i < strCorp.Length - 1; i++)
            {
                char c = strCorp[i];
                if (!char.IsNumber(c))
                    return false;
                else
                    if (i < strCorp.Length)//지정된 숫자로 각 자리를 나눈 후 더한다.
                    sum += ((i % 2) + 1) * int.Parse(c.ToString());
            }
            //합을 10으로 나누어 나머지를 구한 뒤 10에서 나머지를 뺀 값이 오류검색번호(10일때는 0을 오류검색번호로)
            if (strCorp[strCorp.Length - 1].ToString() != ((10 - (sum % 10)) % 10).ToString())
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// 주민등록번호 유효성 검사
        /// </summary>
        /// <param name="RRN"></param>
        /// <returns></returns>
        private static bool IsAvailableRRN(string RRN)
        {
            RRN = RRN.Replace(" ", "").Replace("-", "");
            if (RRN.Length != 13)
                return false;
            int sum = 0;
            for (int i = 0; i < RRN.Length - 1; i++)
            {
                char c = RRN[i];
                if (!char.IsNumber(c))
                    return false;
                else
                    if (i < RRN.Length)//지정된 숫자로 각 자리를 나눈 후 더한다.
                    sum += int.Parse(c.ToString()) * ((i % 8) + 2);
            }
            if (!((((11 - (sum % 11)) % 10).ToString()) == ((RRN[RRN.Length - 1]).ToString())))
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// 외국인번호 유효성 검사
        /// 9999999999999 로 들어오는 번호는 외국인번호로 간주하여 유효성 검사를 true로 반환시킵니다.
        /// </summary>
        /// <param name="reg_no"></param>
        /// <returns></returns>
        private static bool IsAvailableForeignNO(string reg_no)
        {
            reg_no = reg_no.Replace(" ", "").Replace("-", "");

            if (reg_no.Length != 13)
                return false;
            if (reg_no == "9999999999999")
                return true;

            int sum = 0;
            int odd = 0;

            int[] buf = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

            for (int i = 0; i < 13; i++)
            {
                buf[i] = Convert.ToInt32(reg_no.Substring(i, 1));
            }

            odd = buf[7] * 10 + buf[8];
            if (odd % 2 != 0)
                return false;

            if ((buf[11] != 6) && (buf[11] != 7) && (buf[11] != 8) && (buf[11] != 9))
                return false;

            int[] multipliers = new int[] { 2, 3, 4, 5, 6, 7, 8, 9, 2, 3, 4, 5 };
            for (int i = 0; i < 12; i++)
            {
                sum += (buf[i] *= multipliers[i]);
            }

            sum = 11 - (sum % 11);
            if (sum >= 10)
                sum -= 10;
            sum += 2;
            if (sum >= 10)
                sum -= 10;
            if (sum != buf[12])
                return false;
            else
                return true;
        }

        /// <summary>
        /// 해당 문자열이 전화번호인지 확인
        /// 숫자와 - 가 아닌 문자가 들어가 있거나
        /// 전화번호의 형식에 어긋나는 경우
        /// false반환
        /// </summary>
        /// <param name="phone_no"></param>
        /// <returns></returns>
        public static bool CheckTelephoneNum(string phone_no)
        {
            try
            {
                string[] str2Array = new string[] { "02" };
                string[] str3Array = new string[] { "051", "053", "032", "062", "042", "052", "044", "031", "033", "043", "041", "063", "061", "054", "055", "064", "010", "011", "016", "017", "018", "019", "050" };
                string[] str4Array = new string[] { "1877", "1811", "1688", "1666", "1599", "1800", "1544", "1644", "1833", "1522", "1661", "1800", "1566", "1600", "1670", "1855" };

                string origin = phone_no.ToStringEx().Trim().Replace("-", "");
                char[] splitPno = origin.ToCharArray();
                foreach (char ch in splitPno)
                {
                    if (!(ch == '-' || char.IsDigit(ch)))
                        return false;
                }
                // 최대 11
                // 최대 10
                // 최대 9

                origin = origin.Replace("-", "");
                if (!(origin.Length == 8 || origin.Length == 9 || origin.Length == 10 || origin.Length == 11))
                    return false;

                foreach (string str in str2Array)
                {
                    if (origin.StartsWith(str))
                    {
                        if (origin.Length == 10 || origin.Length == 9)
                            return true;
                        else
                            return false;
                    }
                }

                foreach (string str in str3Array)
                {
                    if (origin.StartsWith(str))
                    {
                        if (origin.Length == 11 || origin.Length == 10)
                            return true;
                        else
                            return false;
                    }
                }

                foreach (string str in str4Array)
                {
                    if (origin.StartsWith(str))
                    {
                        if (origin.Length == 8)
                            return true;
                        else
                            return false;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return false;
        }

        public static DataTable GetDifferentRecords(DataTable FirstDataTable, DataTable SecondDataTable)
        {
            //Create Empty Table
            DataTable ResultDataTable = new DataTable("ResultDataTable");
            FirstDataTable.TableName = "t1";
            SecondDataTable.TableName = "t2";
            //use a Dataset to make use of a DataRelation object
            using (DataSet ds = new DataSet())
            {
                //Add tables
                ds.Tables.AddRange(new DataTable[] { FirstDataTable.Copy(), SecondDataTable.Copy() });

                int firstColumnsCnt = ds.Tables[0].Columns.Count;
                if (ds.Tables[0].Columns.Contains("CheckValueCustomColumm#$"))
                    firstColumnsCnt--;
                //Get Columns for DataRelation
                DataColumn[] firstColumns = new DataColumn[firstColumnsCnt];
                for (int i = 0; i < firstColumnsCnt; i++)
                {
                    firstColumns[i] = ds.Tables[0].Columns[i];
                }
                firstColumns = firstColumns.OrderBy(x => x.ColumnName).ToArray();

                DataColumn[] secondColumns = new DataColumn[ds.Tables[1].Columns.Count];
                for (int i = 0; i < secondColumns.Length; i++)
                {
                    secondColumns[i] = ds.Tables[1].Columns[i];
                }
                secondColumns = secondColumns.OrderBy(x => x.ColumnName).ToArray();

                //Create DataRelation
                DataRelation r1 = new DataRelation(string.Empty, firstColumns, secondColumns, false);
                ds.Relations.Add(r1);

                DataRelation r2 = new DataRelation(string.Empty, secondColumns, firstColumns, false);
                ds.Relations.Add(r2);

                //Create columns for return table
                for (int i = 0; i < FirstDataTable.Columns.Count; i++)
                {
                    ResultDataTable.Columns.Add(FirstDataTable.Columns[i].ColumnName, FirstDataTable.Columns[i].DataType);
                }

                //If FirstDataTable Row not in SecondDataTable, Add to ResultDataTable.
                ResultDataTable.BeginLoadData();
                foreach (DataRow parentrow in ds.Tables[0].Rows)
                {
                    DataRow[] childrows = parentrow.GetChildRows(r1);
                    if (childrows == null || childrows.Length == 0)
                        ResultDataTable.LoadDataRow(parentrow.ItemArray, true);
                }

                //If SecondDataTable Row not in FirstDataTable, Add to ResultDataTable.
                foreach (DataRow parentrow in ds.Tables[1].Rows)
                {
                    DataRow[] childrows = parentrow.GetChildRows(r2);
                    if (childrows == null || childrows.Length == 0)
                        ResultDataTable.LoadDataRow(parentrow.ItemArray, true);
                }
                ResultDataTable.EndLoadData();
            }

            return ResultDataTable;
        }

        public static bool IsDifferentTable(DataTable FirstDataTable, DataTable SecondDataTable)
        {
            DataTable dt;
            dt = GetDifferentRecords(FirstDataTable, SecondDataTable);

            if (dt.Rows.Count == 0)
                return true;
            else
                return false;
        }
    }
}