﻿using System;
using System.Windows.Forms;

namespace BH_Library.Utils
{
    public class BhMsgBox
    {
        #region 공통 메세지 박스

        public static DialogResult ShowWithButtonIcon(string text, string caption = "확인", MessageBoxButtons buttons = MessageBoxButtons.OKCancel, MessageBoxIcon icon = MessageBoxIcon.Information)
        {
            DialogResult dialogResult = MessageBox.Show(text, caption, buttons, icon);
            return dialogResult;
        }

        public static void EmptyOnRequireValue(string text)
        {
            MessageBox.Show("아래 항목은 필수값 입니다.\r\n" + text, "저장 불가 - 필수값 없음", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static DialogResult Question(string text, string caption = "질문", MessageBoxDefaultButton defaultButton = MessageBoxDefaultButton.Button1)
        {
            return MessageBox.Show(text, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question, defaultButton);
        }

        public static DialogResult Question(IWin32Window owner, string text, string caption = "질문", MessageBoxDefaultButton defaultButton = MessageBoxDefaultButton.Button1)
        {
            return MessageBox.Show(owner, text, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question, defaultButton);
        }

        public static DialogResult QuestionWithCancel(string text, string caption = "질문", MessageBoxDefaultButton defaultButton = MessageBoxDefaultButton.Button1)
        {
            return MessageBox.Show(text, caption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, defaultButton);
        }

        public static DialogResult QuestionWithCancel(IWin32Window owner, string text, string caption = "질문", MessageBoxDefaultButton defaultButton = MessageBoxDefaultButton.Button1)
        {
            return MessageBox.Show(owner, text, caption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, defaultButton);
        }

        public static DialogResult Confirm(string text, string caption = "확인", MessageBoxDefaultButton defaultButton = MessageBoxDefaultButton.Button1)
        {
            return MessageBox.Show(text, caption, MessageBoxButtons.OKCancel, MessageBoxIcon.Information, defaultButton);
        }

        public static DialogResult Confirm(IWin32Window owner, string text, string caption = "확인", MessageBoxDefaultButton defaultButton = MessageBoxDefaultButton.Button1)
        {
            return MessageBox.Show(owner, text, caption, MessageBoxButtons.OKCancel, MessageBoxIcon.Information, defaultButton);
        }

        public static DialogResult ShowError(string text, string caption = "오류")
        {
            return Error(text, caption);
        }

        public static void ShowErrorNotice(Exception ex, string text = "오류가 발생하였습니다. \r\n자세한 사항은 콘솔창을 통해 확인하세요")
        {
            MessageBox.Show(text, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            Console.WriteLine(ex.Message);
        }

        public static DialogResult Error(string text, string caption = "오류")
        {
            return MessageBox.Show(text, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static DialogResult Error(IWin32Window owner, string text, string caption = "오류")
        {
            return MessageBox.Show(owner, text, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static DialogResult Alarm(string text, string caption = "알림")
        {
            return MessageBox.Show(text, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public static DialogResult Alarm(IWin32Window owner, string text, string caption = "알림")
        {
            return MessageBox.Show(owner, text, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public static DialogResult Warning(string text, string caption = "경고")
        {
            return MessageBox.Show(text, caption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        public static DialogResult Warning(IWin32Window owner, string text, string caption = "경고")
        {
            return MessageBox.Show(owner, text, caption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        #endregion 공통 메세지 박스

        #region 마감 메세지 관련 메세지 박스

        public const string AlarmMessageForClosedData =
@"선택한 자료는 마감된 자료이므로 해당기능을 사용할 수 없습니다.
[마감풀기] 후 다시 진행하시기 바랍니다.";

        public static DialogResult AlarmForClosedData(IWin32Window owner)
        {
            if (owner == null)
                return BhMsgBox.Alarm(AlarmMessageForClosedData);
            else
                return BhMsgBox.Alarm(owner, AlarmMessageForClosedData);
        }

        #endregion 마감 메세지 관련 메세지 박스
    }
}