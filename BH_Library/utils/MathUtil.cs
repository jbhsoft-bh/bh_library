﻿using System;

namespace BH_Library.Utils
{
    /// <summary>
    /// 절사에 관련된 기능을 제공합니다.
    /// </summary>
    public class MathUtil
    {
        /// <summary>
        /// 절사시, 소수점 이하 값에 대한 처리방법을 나타내는 나열자입니다.
        /// </summary>
        public enum CuttingMethod
        {
            /// <summary>
            /// 반올림
            /// </summary>
            Round,

            /// <summary>
            /// 올림
            /// </summary>
            RoundUp,

            /// <summary>
            /// 내림
            /// </summary>
            RoundDown,

            /// <summary>
            /// 절사
            /// </summary>
            Cut
        }

        /// <summary>
        /// 소수점 부분을 절사합니다.
        /// </summary>
        /// <param name="val">절사할 decimal타입의 값입니다.</param>
        /// <returns>가공된 결과값을 리턴합니다.</returns>
        public static decimal CutTheDecimal(decimal val)
        {
            return decimal.Truncate(val);
        }

        /// <summary>
        /// 소수점 부분을 절사합니다.
        /// </summary>
        /// <param name="val">절사할 double타입의 값입니다.</param>
        /// <returns>가공된 결과값을 리턴합니다.</returns>
        public static double CutTheDecimal(double val)
        {
            return Math.Truncate(val);
        }

        /// <summary>
        /// 반올림/올림/내림/절사를 위한 메소드
        /// </summary>
        /// <param name="originValue">처리하고자 하는 값</param>
        /// <param name="decimalLength">소수점 자리수</param>
        /// <param name="method">처리방법</param>
        /// <returns>가공된 결과값을 리턴합니다.</returns>
        public static decimal CutTheDecimal(decimal originValue, int decimalLength, CuttingMethod method)
        {
            decimal retVal = decimal.Zero;
            bool isMinusValue = false;

            if (originValue < decimal.Zero)
            {
                isMinusValue = true;
            }

            originValue = originValue * (decimal)Math.Pow(10d, (double)decimalLength);

            switch (method)
            {
                case CuttingMethod.Round:
                    originValue = Math.Round(Math.Abs(originValue), MidpointRounding.AwayFromZero);

                    if (isMinusValue)
                    {
                        originValue *= -1m;
                    }
                    break;

                case CuttingMethod.RoundUp:
                    originValue = Math.Ceiling(originValue);

                    if (isMinusValue)
                    {
                        originValue *= -1m;
                    }
                    break;

                case CuttingMethod.RoundDown:
                    originValue = Math.Floor(originValue);

                    if (isMinusValue)
                    {
                        originValue *= -1m;
                    }
                    break;

                case CuttingMethod.Cut:
                    originValue = Math.Truncate(originValue);
                    break;

                default:
                    originValue = decimal.Zero;
                    break;
            }

            retVal = originValue / (decimal)Math.Pow(10d, (double)decimalLength);

            return retVal;
        }

        /// <summary>
        /// 밀리미터 값을 1/100 인치로 환산합니다.
        /// </summary>
        /// <param name="millimeter"></param>
        /// <returns></returns>
        public static int GetPercentInch(decimal millimeter)
        {
            return (int)((millimeter / 25.4m) * 100);
        }

        /// <summary>
        /// 1/100 인치값을 밀리미터로 환산합니다.
        /// </summary>
        /// <param name="PercentInch"></param>
        /// <returns></returns>
        public static int GetMilimeter(decimal PercentInch)
        {
            int value = (int)((PercentInch * 25.4m) / 100);
            if (value > 0)
                value++;
            return value;
        }
    }
}