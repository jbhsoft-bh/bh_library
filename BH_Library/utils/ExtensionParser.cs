﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;

namespace BH_Library.Utils
{
    public static class ExtensionParser
    {
        /// <summary>
        /// object 객체를 int형으로 변환. 변환실패 시 0 반환
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static int ToIntEx(this object o)
        {
            int val = 0;

            if (o != null)
            {
                int.TryParse(o.ToString(), out val);
            }

            return val;
        }

        /// <summary>
        /// object 객체를 long형으로 변환. 변환실패 시 0L 반환
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static long ParseLongEx(this object o)
        {
            long val = 0L;

            if (o != null)
            {
                long.TryParse(o.ToString(), out val);
            }

            return val;
        }

        /// <summary>
        /// object 객체를 double형으로 변환. 변환실패 시 0d 반환
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static double ToDoubleEx(this object o)
        {
            double val = 0d;

            if (o != null)
            {
                double.TryParse(o.ToString(), out val);
            }

            return val;
        }

        /// <summary>
        /// object 객체를 decimal형으로 변환. 변환실패 시 0m 반환
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static decimal ToDecimalEx(this object o)
        {
            decimal val = decimal.Zero;

            if (o != null)
            {
                decimal.TryParse(o.ToString(), out val);
            }

            return val;
        }

        /// <summary>
        /// object 객체를 float형으로 변환. 변환실패 시 0f 반환
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static float ToFloatEx(this object o)
        {
            float val = 0f;

            if (o != null)
            {
                float.TryParse(o.ToString(), out val);
            }

            return val;
        }

        /// <summary>
        /// object 객체를 string형으로 변환. 변환실패 시 string.Empty 반환
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static string ToStringEx(this object o)
        {
            string val = string.Empty;

            if (o != null)
            {
                val = o.ToString();
            }

            return val;
        }

        /// <summary>
        /// object 객체를 bool형으로 변환. 변환실패 시 false 반환
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static bool ToBoolEx(this object o)
        {
            bool val = false;

            if (o != null)
            {
                bool.TryParse(o.ToString(), out val);
            }

            return val;
        }

        public static object GetDefaultValue(Type t)
        {
            if (t.IsValueType)
            {
                return Activator.CreateInstance(t);
            }
            else
            {
                if (t.Equals(typeof(string)))
                {
                    return string.Empty;
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// object 객체를 int?형으로 변환. 변환실패 시 null 반환
        /// 2013.04.24 진병호
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static int? ToIntNullEx(this object o)
        {
            int? val = null;

            if (o != null)
            {
                int inVal = 0;
                bool blResult = int.TryParse(o.ToString(), out inVal);
                if (blResult)
                    val = inVal;
            }

            return val;
        }

        /// <summary>
        /// object 객체를 long형으로 변환. 변환실패 시 null 반환
        /// 2013.04.24 진병호
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static long? ToLongNullEx(this object o)
        {
            long? val = null;

            if (o != null)
            {
                long inVal = 0L;
                bool blResult = long.TryParse(o.ToString(), out inVal);
                if (blResult)
                    val = inVal;
            }

            return val;
        }

        public static long ToLongEx(this object o)
        {
            long val = 0L;

            if (o != null)
            {
                long inVal = 0L;
                bool blResult = long.TryParse(o.ToString(), out inVal);
                if (blResult)
                    val = inVal;
            }

            return val;
        }

        /// <summary>
        /// object 객체를 decimal형으로 변환. 변환실패 시 null 반환
        /// 2013.04.24 진병호
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static decimal? ToDecimalNullEx(this object o)
        {
            decimal? val = null;

            if (o != null)
            {
                decimal inVal = decimal.Zero;
                bool blResult = decimal.TryParse(o.ToString(), out inVal);
                if (blResult)
                    val = inVal;
            }

            return val;
        }

        /// <summary>
        /// object 객체를 float형으로 변환. 변환실패 시 null 반환
        /// 2013.04.24 진병호
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static float? ToFloatNullEx(this object o)
        {
            float? val = null;

            if (o != null)
            {
                float inVal = 0f;
                bool blResult = float.TryParse(o.ToString(), out inVal);
                if (blResult)
                    val = inVal;
            }

            return val;
        }

        public static char? ToCharNullEx(this object o)
        {
            char? var = null;
            if (o != null)
            {
                var = System.Convert.ToChar(o);
            }
            return var;
        }

        /// <summary>
        /// object 객체를 string형으로 변환. 변환실패 시 null 반환
        /// 2013.04.24 진병호
        /// </summary>
        /// <param name="o"></param>
        /// <param name="inCludeEmpty">문자열이 empty일 경우도 null로 변환 시키려면 true</param>
        /// <returns></returns>
        public static string ToStringNullEx(this object o, bool inCludeEmpty = false)
        {
            string val = null;

            if (o != null)
            {
                val = o.ToString();
            }

            if (o != null && inCludeEmpty && string.IsNullOrEmpty(val))
            {
                val = null;
            }

            return val;
        }

        /// <summary>
        /// object 객체를 DateTime? 형으로 변환
        /// </summary>
        /// <param name="o"></param>
        /// <returns>변환에 성공하면 DateTime 객체를 반환, 변환에 실패하면 null 반환</returns>
        public static DateTime? ToDateTimeEx(this object o)
        {
            DateTime? val = null;

            if (o != null && !System.Convert.IsDBNull(o))
            {
                try
                {
                    string[] allowFomattings = new string[] { "yyyyMMdd", "yyyy-MM-dd", "yyyy-MM-dd tt h:m:s" };
                    System.Globalization.CultureInfo cultureInfo = new System.Globalization.CultureInfo("ko-KR");
                    DateTime ret = DateTime.MinValue;
                    if (DateTime.TryParseExact(o.ToString(), allowFomattings, cultureInfo, System.Globalization.DateTimeStyles.None, out ret))
                    {
                        val = ret;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            return val;
        }

        /// <summary>
        /// object 객체를 DateTime 형으로 변환
        /// </summary>
        /// <param name="o"></param>
        /// <returns>변환에 성공하면 DateTime 객체를 반환, 변환에 실패하면 기본값 반환(0001-01-01 12:00:00)</returns>
        public static DateTime ToDateTimeDefaultEx(this object o)
        {
            DateTime val = DateTime.MinValue;

            if (o != null && !System.Convert.IsDBNull(o))
            {
                try
                {
                    string strValue = o.ToStringEx();
                    if (strValue.Length == 6) strValue += "01";
                    if (strValue.Length == 8)
                    {
                        int temp = 0;
                        if (int.TryParse(strValue, out temp))
                        {
                            val = new DateTime(strValue.Substring(0, 4).ToIntEx(), strValue.Substring(4, 2).ToIntEx(), strValue.Substring(6, 2).ToIntEx());
                            return val;
                        }
                    }

                    object valObj = TypeDescriptor.GetConverter(typeof(DateTime)).ConvertFromString(o.ToString());
                    if (valObj is DateTime)
                    {
                        val = (DateTime)valObj;
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

            return val;
        }

        public static byte[] ToByteArrayEx(this object o)
        {
            string strByte = o.ToStringEx();
            return Enumerable.Range(0, strByte.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(strByte.Substring(x, 2), 16))
                             .ToArray();
        }

        /// <summary>
        /// 파라미터를 숫자로 바꾼 후 #,#처리 하여 반환합니다.
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static string ToStringMoneyEx(this object o, bool WithDecimal = false, bool WonDisplay = false)
        {
            string format = "#,#";
            if (WithDecimal)
                format += ".#";
            return o.ToDecimalEx().ToString(format);
        }

        /// <summary>
        /// 실제 파일 크기를 바탕으로 B KB MB GB등으로 변경하여 리턴함
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        public static string GetFileSizeFormat(this object size)
        {
            string[] units = new[] { "Byte", "KB", "MB", "GB" };

            double dSize = size.ToDecimalEx().ToDoubleEx();
            int addIdx = 0;
            while (dSize > 1024)
            {
                dSize = dSize / 1024;
                addIdx++;
            }

            string resultFormat = "#,0";
            if (dSize != (int)dSize)
                resultFormat = resultFormat + "." + (addIdx > 2 ? "##" : "#");
            return string.Format("{0}{1}", dSize.ToString(resultFormat), units[addIdx]);
        }

        /// <summary>
        /// 매개변수로 받은 문자열 중
        /// \ / : * ? " < > |
        /// 문자가 들어오면
        /// _ 로 변환하여 리턴함
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static string ToEnableFileNameEx(this string fileName)
        {
            string result = fileName.ToStringEx();

            result = result.Replace("\\", "_")
                .Replace(@"\", "_")
                .Replace("/", "_")
                .Replace(":", "_")
                .Replace("?", "_")
                .Replace("\"", "_")
                .Replace("<", "_")
                .Replace(">", "_")
                .Replace("|", "_");
            return result;
        }

        public static string ToDecodingUTF8Ex(this string originValue)
        {
            string result = "";
            Encoding decodingType = Encoding.UTF8;
            byte[] arr = null;

            List<string> resultList = StringUtil.SplitByString(originValue, "=?UTF-8?B?").ToList();
            List<string> subList = null;
            resultList.ForEach(x =>
            {
                subList = StringUtil.SplitByString(x, "?=").ToList();
                subList.ForEach(z =>
                {
                    try
                    {
                        arr = System.Convert.FromBase64String(z);
                        result += decodingType.GetString(arr);
                    }
                    catch
                    {
                        result += z;
                    }
                });
            });
            return result;
        }

        /// <summary>
        /// 양력을 음력으로 변환
        /// </summary>
        /// <param name="solarDate"></param>
        /// <returns></returns>
        public static DateTime ConvertFromSolarDate(this string solarDate)
        {
            DateTime date = DateTime.Parse(solarDate);
            return ConvertFromSolarDate(date);
        }

        /// <summary>
        /// 양력을 음력으로 변환
        /// </summary>
        /// <param name="solarDate"></param>
        /// <returns></returns>
        public static DateTime ConvertFromSolarDate(this DateTime solarDate)
        {
            DateTime leapDate = DateTime.Now;
            KoreanLunisolarCalendar ksc = new KoreanLunisolarCalendar();
            int year = ksc.GetYear(solarDate);
            int month = ksc.GetMonth(solarDate);
            int day = ksc.GetDayOfMonth(solarDate);
            bool isLeapMonth;
            //윤달이 끼어 있으면
            if (ksc.GetMonthsInYear(year) > 12)
            {
                int leapMonth = ksc.GetLeapMonth(year);
                if (month >= leapMonth)
                {
                    isLeapMonth = (month == leapMonth);
                    month--;
                }
            }
            leapDate = new DateTime(year, month, day, 0, 0, 0, 0);
            return leapDate;
        }

        /// <summary>
        /// 음력을 양력으로 변환
        /// </summary>
        /// <param name="lunarDate"></param>
        /// <returns></returns>
        public static DateTime ConvertFromKoreaLunarDate(this string lunarDate)
        {
            DateTime date = DateTime.Parse(lunarDate);
            return ConvertFromKoreaLunarDate(date);
        }

        /// <summary>
        /// 음력을 양력으로 변환
        /// </summary>
        /// <param name="lunarDate"></param>
        /// <returns></returns>
        public static DateTime ConvertFromKoreaLunarDate(this DateTime lunarDate)
        {
            DateTime returnDate = new DateTime();
            KoreanLunisolarCalendar ksc = new KoreanLunisolarCalendar();
            int year = lunarDate.Year;
            int month = lunarDate.Month;
            int day = lunarDate.Day;
            if (ksc.GetMonthsInYear(year) > 12)
            {
                int leapMonth = ksc.GetLeapMonth(year);
                if (month >= leapMonth - 1)
                    returnDate = ksc.ToDateTime(year, month + 1, day, 0, 0, 0, 0);
                else
                    returnDate = ksc.ToDateTime(year, month, day, 0, 0, 0, 0);
            }
            else
                returnDate = ksc.ToDateTime(year, month, day, 0, 0, 0, 0);
            return returnDate;
        }
    }
}