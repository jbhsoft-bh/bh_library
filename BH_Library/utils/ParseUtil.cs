﻿namespace BH_Library.Utils
{
    ///// <summary>
    ///// DataType 변환에 대한 Convert 메서드를 제공합니다.
    ///// </summary>
    //public class ParseUtil
    //{
    //    /// <summary>
    //    /// object 객체를 int형으로 변환. 변환실패 시 0 반환
    //    /// </summary>
    //    /// <param name="o"></param>
    //    /// <returns></returns>
    //    public static int ParseInt(object o)
    //    {
    //        int val = 0;

    //        if (o != null)
    //        {
    //            int.TryParse(o.ToString(), out val);
    //        }

    //        return val;
    //    }

    //    /// <summary>
    //    /// object 객체를 long형으로 변환. 변환실패 시 0L 반환
    //    /// </summary>
    //    /// <param name="o"></param>
    //    /// <returns></returns>
    //    public static long ParseLong(object o)
    //    {
    //        long val = 0L;

    //        if (o != null)
    //        {
    //            long.TryParse(o.ToString(), out val);
    //        }

    //        return val;
    //    }

    //    /// <summary>
    //    /// object 객체를 double형으로 변환. 변환실패 시 0d 반환
    //    /// </summary>
    //    /// <param name="o"></param>
    //    /// <returns></returns>
    //    public static double ParseDouble(object o)
    //    {
    //        double val = 0d;

    //        if (o != null)
    //        {
    //            double.TryParse(o.ToString(), out val);
    //        }

    //        return val;
    //    }

    //    /// <summary>
    //    /// object 객체를 decimal형으로 변환. 변환실패 시 0m 반환
    //    /// </summary>
    //    /// <param name="o"></param>
    //    /// <returns></returns>
    //    public static decimal ParseDecimal(object o)
    //    {
    //        decimal val = decimal.Zero;

    //        if (o != null)
    //        {
    //            decimal.TryParse(o.ToString(), out val);
    //        }

    //        return val;
    //    }

    //    /// <summary>
    //    /// object 객체를 float형으로 변환. 변환실패 시 0f 반환
    //    /// </summary>
    //    /// <param name="o"></param>
    //    /// <returns></returns>
    //    public static float ParseFloat(object o)
    //    {
    //        float val = 0f;

    //        if (o != null)
    //        {
    //            float.TryParse(o.ToString(), out val);
    //        }

    //        return val;
    //    }

    //    /// <summary>
    //    /// object 객체를 string형으로 변환. 변환실패 시 string.Empty 반환
    //    /// </summary>
    //    /// <param name="o"></param>
    //    /// <returns></returns>
    //    public static string ParseString(object o)
    //    {
    //        string val = string.Empty;

    //        if (o != null)
    //        {
    //            val = o.ToString();
    //        }

    //        return val;
    //    }

    //    /// <summary>
    //    /// object 객체를 bool형으로 변환. 변환실패 시 false 반환
    //    /// </summary>
    //    /// <param name="o"></param>
    //    /// <returns></returns>
    //    public static bool ParseBool(object o)
    //    {
    //        bool val = false;

    //        if (o != null)
    //        {
    //            bool.TryParse(o.ToString(), out val);
    //        }

    //        return val;
    //    }

    //    ////난독화 정책으로 인해 dynamic형태는 쓰지않음
    //    ///// <summary>
    //    ///// 형변환을 합니다.
    //    ///// </summary>
    //    ///// <param name="type">변환하고자 하는 Type</param>
    //    ///// <param name="value">변환하고자 하는 값</param>
    //    ///// <returns></returns>
    //    //[Obsolete("더 이상 사용되지 않습니다. 절대 사용하지 마세요.")]
    //    //public static dynamic Parse(Type type, object value)
    //    //{
    //    //    dynamic retVal = GetDefaultValue(type);

    //    //    if (value != null)
    //    //    {
    //    //        if (value != DBNull.Value)
    //    //        {
    //    //            try
    //    //            {
    //    //                retVal = TypeDescriptor.GetConverter(type).ConvertFromString(value.ToString());
    //    //            }
    //    //            catch (Exception e)
    //    //            {
    //    //                throw e;
    //    //            }
    //    //        }
    //    //    }

    //    //    return retVal;
    //    //}

    //    public static object GetDefaultValue(Type t)
    //    {
    //        if (t.IsValueType)
    //        {
    //            return Activator.CreateInstance(t);
    //        }
    //        else
    //        {
    //            if (t.Equals(typeof(string)))
    //            {
    //                return string.Empty;
    //            }
    //            else
    //            {
    //                return null;
    //            }
    //        }
    //    }

    //    /// <summary>
    //    /// object 객체를 int?형으로 변환. 변환실패 시 null 반환
    //    /// 2013.04.24 진병호
    //    /// </summary>
    //    /// <param name="o"></param>
    //    /// <returns></returns>
    //    public static int? ParseIntNull(object o)
    //    {
    //        int? val = null;

    //        if (o != null)
    //        {
    //            int inVal = 0;
    //            bool blResult = int.TryParse(o.ToString(), out inVal);
    //            if (blResult)
    //                val = inVal;
    //        }

    //        return val;
    //    }

    //    /// <summary>
    //    /// object 객체를 long형으로 변환. 변환실패 시 null 반환
    //    /// 2013.04.24 진병호
    //    /// </summary>
    //    /// <param name="o"></param>
    //    /// <returns></returns>
    //    public static long? ParseLongNull(object o)
    //    {
    //        long? val = null;

    //        if (o != null)
    //        {
    //            long inVal = 0L;
    //            bool blResult = long.TryParse(o.ToString(), out inVal);
    //            if (blResult)
    //                val = inVal;
    //        }

    //        return val;
    //    }

    //    /// <summary>
    //    /// object 객체를 decimal형으로 변환. 변환실패 시 null 반환
    //    /// 2013.04.24 진병호
    //    /// </summary>
    //    /// <param name="o"></param>
    //    /// <returns></returns>
    //    public static decimal? ParseDecimalNull(object o)
    //    {
    //        decimal? val = null;

    //        if (o != null)
    //        {
    //            decimal inVal = decimal.Zero;
    //            bool blResult = decimal.TryParse(o.ToString(), out inVal);
    //            if (blResult)
    //                val = inVal;
    //        }

    //        return val;
    //    }

    //    /// <summary>
    //    /// object 객체를 float형으로 변환. 변환실패 시 null 반환
    //    /// 2013.04.24 진병호
    //    /// </summary>
    //    /// <param name="o"></param>
    //    /// <returns></returns>
    //    public static float? ParseFloatNull(object o)
    //    {
    //        float? val = null;

    //        if (o != null)
    //        {
    //            float inVal = 0f;
    //            bool blResult = float.TryParse(o.ToString(), out inVal);
    //            if (blResult)
    //                val = inVal;
    //        }

    //        return val;
    //    }

    //    public static char? ParseCharNull(object o)
    //    {
    //        char? var = null;
    //        if (o != null)
    //        {
    //            var = Convert.ToChar(o);
    //        }
    //        return var;
    //    }

    //    /// <summary>
    //    /// object 객체를 string형으로 변환. 변환실패 시 null 반환
    //    /// 2013.04.24 진병호
    //    /// </summary>
    //    /// <param name="o"></param>
    //    /// <param name="inCludeEmpty">문자열이 empty일 경우도 null로 변환 시키려면 true</param>
    //    /// <returns></returns>
    //    public static string ParseStringNull(object o, bool inCludeEmpty = false)
    //    {
    //        string val = null;

    //        if (o != null)
    //        {
    //            val = o.ToString();
    //        }

    //        if (o != null && inCludeEmpty && string.IsNullOrEmpty(val))
    //        {
    //            val = null;
    //        }

    //        return val;
    //    }

    //    /// <summary>
    //    /// object 객체를 DateTime? 형으로 변환
    //    /// </summary>
    //    /// <param name="o"></param>
    //    /// <returns>변환에 성공하면 DateTime 객체를 반환, 변환에 실패하면 null 반환</returns>
    //    public static DateTime? ParseDateTime(object o)
    //    {
    //        DateTime? val = null;

    //        if (o != null && !Convert.IsDBNull(o))
    //        {
    //            try
    //            {
    //                string[] allowFomattings = new string[] { "yyyyMMdd", "yyyy-MM-dd" };
    //                System.Globalization.CultureInfo cultureInfo = new System.Globalization.CultureInfo("ko-KR");
    //                DateTime ret = DateTime.MinValue;
    //                if (DateTime.TryParseExact(o.ToString(), allowFomattings, cultureInfo, System.Globalization.DateTimeStyles.None, out ret))
    //                {
    //                    val = ret;
    //                }
    //            }
    //            catch (Exception ex)
    //            {
    //                throw ex;
    //            }
    //        }

    //        return val;
    //    }

    //    /// <summary>
    //    /// object 객체를 DateTime 형으로 변환
    //    /// </summary>
    //    /// <param name="o"></param>
    //    /// <returns>변환에 성공하면 DateTime 객체를 반환, 변환에 실패하면 기본값 반환(0001-01-01 12:00:00)</returns>
    //    public static DateTime ParseDateTimeDefault(object o)
    //    {
    //        DateTime val = DateTime.MinValue;

    //        if (o != null && !Convert.IsDBNull(o))
    //        {
    //            try
    //            {
    //                object valObj = TypeDescriptor.GetConverter(typeof(DateTime)).ConvertFromString(o.ToString());
    //                if (valObj is DateTime)
    //                {
    //                    val = (DateTime)valObj;
    //                }
    //            }
    //            catch (Exception e)
    //            {
    //                throw e;
    //            }
    //        }

    //        return val;
    //    }
    //}
}