﻿using System;
using System.Windows.Forms;

namespace BH_Library.Utils
{
    [Obsolete("사용하지 말것, BhMsgBox 클래스를 사용할것")]
    public class MessageHelper
    {
        public static void Alram(string text, string caption = "")
        {
            MessageBox.Show(text, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public static void Show(string text, string caption = "확인")
        {
            MessageBox.Show(text, caption);
        }

        public static DialogResult Question(string text, string caption = "확인", MessageBoxButtons button = MessageBoxButtons.YesNo)
        {
            return MessageBox.Show(text, caption, button, MessageBoxIcon.Question);
        }

        public static void Info(string text, string caption = "확인")
        {
            MessageBox.Show(text, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public static void Warning(string text, string caption = "주의")
        {
            MessageBox.Show(text, caption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        public static void ShowError(string text, string caption = "오류")
        {
            Error(text, caption);
        }

        public static void ShowWarning(string text, string caption = "주의")
        {
            Warning(text, caption);
        }

        public static void EmptyOnRequireValue(string text)
        {
            MessageBox.Show("아래 항목은 필수값 입니다.\r\n" + text, "저장 불가 - 필수값 없음", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static void Error(string text, string caption = "오류")
        {
            MessageBox.Show(text, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static void ShowWithIcon(string text, string caption = "확인", MessageBoxIcon icon = MessageBoxIcon.Information)
        {
            MessageBox.Show(text, caption, MessageBoxButtons.OK, icon);
        }

        public static DialogResult ShowWithButton(string text, string caption = "확인", MessageBoxButtons buttons = MessageBoxButtons.OKCancel)
        {
            DialogResult dialogResult = MessageBox.Show(text, caption, buttons);
            return dialogResult;
        }

        public static DialogResult ShowWithButtonIcon(string text, string caption = "확인", MessageBoxButtons buttons = MessageBoxButtons.OKCancel, MessageBoxIcon icon = MessageBoxIcon.Information)
        {
            DialogResult dialogResult = MessageBox.Show(text, caption, buttons, icon);
            return dialogResult;
        }
    }
}