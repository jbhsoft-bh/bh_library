﻿using System.IO;

namespace BH_Library.Utils
{
    /// <summary>
    /// 파일과 관련된 유틸 메소드를 제공합니다.
    /// </summary>
    public class FileUtil
    {
        /// <summary>
        /// 스트림을 복사하는 기능을 제공합니다.
        /// </summary>
        /// <param name="input"></param>
        /// <param name="output"></param>
        public static void CopyStream(Stream input, Stream output)
        {
            byte[] buffer = new byte[32768];
            int read;
            while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                output.Write(buffer, 0, read);
            }
        }
    }
}