﻿using System;
using System.ComponentModel;
using System.Data;
using System.Reflection;

namespace BH_Library.Utils
{
    public static class DataTypeParser
    {
        /// <summary>
        /// DataTable를 T타입의 BindingList객체로 변환을 제공합니다.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="table"></param>
        /// <param name="parms"></param>
        /// <returns></returns>
        public static BindingList<T> DataTableToBindingList<T>(DataTable table, object[] parms = null)
        {
            if (table == null)
            {
                return null;
            }

            BindingList<DataRow> rows = new BindingList<DataRow>();

            foreach (DataRow row in table.Rows)
            {
                rows.Add(row);
            }

            return DataRowToBindingList<T>(rows, parms);
        }

        /// <summary>
        /// DataRow를 T타입의(기본생성자) BindingList객체로 변환을 제공합니다.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="rows"></param>
        /// <param name="parms"></param>
        /// <returns></returns>
        public static BindingList<T> DataRowToBindingList<T>(BindingList<DataRow> rows, object[] parms = null)
        {
            BindingList<T> list = null;

            if (rows != null)
            {
                list = new BindingList<T>();

                foreach (DataRow row in rows)
                {
                    T item = DataRowToObject<T>(row, parms);
                    list.Add(item);
                }
            }

            return list;
        }

        /// <summary>
        /// DataRow를 T타입의(기본생성자) 객체로 변환을 제공합니다.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="row"></param>
        /// <param name="parms"></param>
        /// <returns></returns>
        public static T DataRowToObject<T>(DataRow row, object[] parms = null)
        {
            T obj = default(T);
            if (row != null)
            {
                obj = (T)Activator.CreateInstance(typeof(T), parms);

                foreach (DataColumn column in row.Table.Columns)
                {
                    PropertyInfo prop = obj.GetType().GetProperty(column.ColumnName,
                        BindingFlags.IgnoreCase | BindingFlags.SetProperty | BindingFlags.Public | BindingFlags.Instance);

                    if (prop != null) // Object에 컬럼이름이 있을경우에만 처리
                    {
                        object value = row[column.ColumnName];

                        // Set Method가 없으면 그냥 넘어감(패스~)
                        if (prop.GetSetMethod() == null)
                            continue;

                        Type[] type = prop.PropertyType.GetGenericArguments();
                        if (type.Length > 0)
                        {
                            if (value.GetType() == type[0])
                            {
                                prop.SetValue(obj, value, null);
                            }
                            else
                            {
                                //// nullable Type에 대응하기 위하여 value 값이 DBNull일 경우
                                //// value 값을 null로 변경하여 ConvertorUtil에서 null이 반환되도록
                                //// 유도하는 로직 추가

                                if (type[0] == typeof(long))
                                    prop.SetValue(obj, value.ToLongNullEx(), null);
                                else if (type[0] == typeof(int))
                                    prop.SetValue(obj, value.ToIntNullEx(), null);
                                else if (type[0] == typeof(decimal))
                                    prop.SetValue(obj, value.ToDecimalNullEx(), null);
                                else if (type[0] == typeof(string))
                                    prop.SetValue(obj, value.ToStringNullEx(), null);
                            }
                        }
                        else
                        {
                            if (value.GetType() == prop.PropertyType)
                            {
                                prop.SetValue(obj, value, null);
                            }
                            else
                            {
                                if (prop.PropertyType == typeof(long))
                                    prop.SetValue(obj, value.ToLongNullEx(), null);
                                else if (prop.PropertyType == typeof(int))
                                    prop.SetValue(obj, value.ToIntNullEx(), null);
                                else if (prop.PropertyType == typeof(decimal))
                                    prop.SetValue(obj, value.ToDecimalNullEx(), null);
                                else if (prop.PropertyType == typeof(string))
                                    prop.SetValue(obj, value.ToStringNullEx(), null);
                            }
                        }
                    }
                }
            }

            return obj;
        }
    }
}