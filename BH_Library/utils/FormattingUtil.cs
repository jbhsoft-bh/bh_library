﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace BH_Library.Utils
{
    /// <summary>
    /// Formatting 관련 유틸리티를 제공하는 클래스 입니다.
    /// </summary>
    public class FormattingUtil
    {
        /// <summary>
        /// 입력된 문자열을 사업자번호 형식으로 변환합니다.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string ConvertBussinessNumber(string data)
        {
            StringBuilder strBuilder = new StringBuilder();

            if (data != null)
            {
                data = data.Replace("-", string.Empty);

                for (int i = 0; i < data.Length; i++)
                {
                    if (i == 3 || i == 5)
                    {
                        strBuilder.Append('-');
                    }

                    strBuilder.Append(data[i]);
                }
            }

            return strBuilder.ToString();
        }

        /// <summary>
        /// 입력된 문자열에 따라 주민번호 또는 사업자번호 형식으로 변환합니다.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string ConvertBusinessAndRegistrationNumber(string data)
        {
            string retVal = string.Empty;
            if (data.Length > 10)
            {
                retVal = ConvertRegistrationNumber(data);
            }
            else
            {
                retVal = ConvertBussinessNumber(data);
            }

            return retVal;
        }

        /// <summary>
        /// 입력된 문자열을 주민번호 형식으로 변환합니다.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string ConvertRegistrationNumber(string data)
        {
            StringBuilder strBuilder = new StringBuilder();

            if (data != null)
            {
                data = data.Replace("-", string.Empty);

                for (int i = 0; i < data.Length; i++)
                {
                    if (i == 6)
                    {
                        strBuilder.Append('-');
                    }

                    strBuilder.Append(data[i]);
                }
            }

            return strBuilder.ToString();
        }

        /// <summary>
        /// 입력된 문자열을 우편번호 형식으로 변환합니다.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string ConvertZipCode(string data)
        {
            StringBuilder strBuilder = new StringBuilder();

            if (data != null)
            {
                data = data.Replace("-", string.Empty);

                for (int i = 0; i < data.Length; i++)
                {
                    if (i == 3)
                    {
                        strBuilder.Append('-');
                    }

                    strBuilder.Append(data[i]);
                }
            }

            return strBuilder.ToString();
        }

        /// <summary>
        /// 입력된 문자열을 날짜형식(yyyy-MM-dd형식)으로 변경합니다.
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static string ConvertDateString(string date)
        {
            StringBuilder strBuilder = new StringBuilder();

            if (date != null)
            {
                for (int i = 0; i < date.Length; i++)
                {
                    strBuilder.Append(date[i]);

                    if (i == 3 || i == 5)
                    {
                        strBuilder.Append('-');
                    }
                }
            }

            return strBuilder.ToString();
        }

        /// <summary>
        /// 입력받은 데이터를 YYYYMMDD형식의 날짜 문자열로 변환합니다.
        /// </summary>
        /// <param name="items">년,월,일 정보를 담고있는 object 배열입니다.</param>
        /// <returns>가공된 YYYYMMDD형식의 문자열로 가공된 결과값을 리턴합니다.</returns>
        public static string ArrayToDateStr(object[] items)
        {
            string str, str2, str3;

            if (items.Length == 3)
            {
                str = "    ";
                str2 = "  ";
                str3 = "  ";
                if ((items[0] != null) && (items[0].ToString() != string.Empty))
                {
                    str = string.Format("{0,4}", items[0].ToString().Trim());
                }
                if ((items[1] != null) && (items[1].ToString() != string.Empty))
                {
                    str2 = string.Format("{0,2}", items[1].ToString().Trim());
                }
                if ((items[2] != null) && (items[2].ToString() != string.Empty))
                {
                    str3 = string.Format("{0,2}", items[2].ToString().Trim());
                }

                string res = string.Format("{0}{1}{2}", str, str2, str3);

                //res.Replace(' ', '0');

                return res;
            }
            return string.Empty;
        }

        /// <summary>
        /// 입력된 DateTime 값을 YYYYMMDD형식의 문자열로 변환합니다.
        /// </summary>
        /// <param name="dateTime">변환할 DateTime 형식의 값</param>
        /// <returns>YYYYMMDD형식으로 가공된 리턴값입니다.</returns>
        public static string DateToStr(DateTime dateTime)
        {
            return dateTime.ToString("yyyyMMdd");
        }

        /// <summary>
        /// 입력받은 데이터를 YYYY-MM-DD형식의 날짜 문자열로 변환합니다.
        /// </summary>
        /// <param name="yyyyMMdd">변환할 문자열입니다.</param>
        /// <returns>YYYY-MM-DD형식으로 가공된 리턴값입니다.</returns>
        public static string StringToDateStr(string yyyyMMdd)
        {
            if (yyyyMMdd.Length != 8)
            {
                return yyyyMMdd;
            }
            string date = "";

            date += yyyyMMdd.Substring(0, 4) + "-";
            date += yyyyMMdd.Substring(4, 2) + "-";
            date += yyyyMMdd.Substring(6, 2);

            return date;
        }

        /// <summary>
        /// 입력된 DateTime 값을 YYYY-MM-DD형식의 문자열로 변환합니다.
        /// </summary>
        /// <param name="dateTime">변환할 DateTime 형식의 값</param>
        /// <returns>YYYY-MM-DD형식으로 가공된 리턴값입니다.</returns>
        public static string DateToDateStr(DateTime dateTime)
        {
            return dateTime.ToString("yyyy-MM-dd");
        }

        /// <summary>
        /// 입력받은 YYYYMMDD형식의 날짜 문자열을 년/월/일 로 분리합니다.
        /// </summary>
        /// <param name="item">분리할 YYYYMMDD형식의 날짜 값입니다.</param>
        /// <returns>년/월/일/로 분리된 List가 리턴됩니다.</returns>
        public static List<string> DateSpliter(object item)
        {
            List<string> list = new List<string>();

            if ((((item != DBNull.Value) && (item != null)) && (item.ToString().Trim() != string.Empty)) && (item.ToString().Length == 8))
            {
                Match match =
                    new Regex(
                        @"(?<YEAR>\d{4})(?<MONTH>\s{1}\d{1}|\d{2})(?<DAY>\s{1}\d{1}|\d{2})(?x)",
                        RegexOptions.IgnorePatternWhitespace | RegexOptions.Compiled | RegexOptions.Multiline | RegexOptions.IgnoreCase).Match(item.ToString());

                if (match.Success)
                {
                    list.AddRange(new string[] { match.Groups["YEAR"].Value, match.Groups["MONTH"].Value, match.Groups["DAY"].Value });
                }
                else
                {
                    list.AddRange(new string[] { "", "", "" });
                }
                return list;
            }
            list.AddRange(new string[] { "", "", "" });
            return list;
        }

        /// <summary>
        /// 숫자형 FormatString 문자열을 생성합니다.
        /// </summary>
        /// <param name="length">데이터 길이</param>
        /// <param name="isShowZero">입력받은 데이터가 0일때 표시할 것인지..</param>
        /// <returns> 가공된 FormatString 문자열이 리턴됩니다.</returns>
        public static string GenerateNumericFormatString(int length, bool isShowZero)
        {
            string formatStr = string.Empty;

            if (length == 0)
            {
                length = 30;
            }

            for (int i = 0; i < length; i++)
            {
                if (isShowZero && i == length - 1)
                {
                    formatStr += "0";
                }
                else
                {
                    formatStr += "#";
                }
            }

            if (isShowZero)
            {
                formatStr = formatStr.Substring(0, formatStr.Length - 1) + "0";
            }

            return formatStr;
        }

        /// <summary>
        /// 금액형 FormatString 문자열을 생성합니다.(3자리마다 컴마)
        /// </summary>
        /// <param name="length">데이터 길이</param>
        /// <param name="isShowZero">입력받은 데이터가 0일때 표시할 것인지..</param>
        /// <returns>Formatting 처리된 금액형 문자열이 리턴됩니다.</returns>
        public static string GenerateCurrencyFormatString(int length, bool isShowZero)
        {
            string formatStr = string.Empty;

            if (length == 0)
            {
                length = 30;
            }

            for (int i = 0; i < length; i++)
            {
                formatStr += "#";
            }

            Regex regex = new Regex("(?<=#)(?=(###)+(?!#))");

            formatStr = regex.Replace(formatStr, ",");

            if (isShowZero)
            {
                formatStr = formatStr.Substring(0, formatStr.Length - 1) + "0";
            }

            return formatStr;
        }

        /// <summary>
        /// 소수점형 FormatString 문자열을 생성합니다.
        /// </summary>
        /// <param name="maxLength">데이터의 총 길이(정수부분+소수부분)</param>
        /// <param name="decimalLength">소수점 자릿수</param>
        /// <returns>Formatting 처리된 금액형 문자열이 리턴됩니다.</returns>
        public static string GenerateDecimalFormatString(int maxLength, int decimalLength)
        {
            string formatStr = string.Empty;

            if (maxLength == 0)
            {
                maxLength = 30;
            }

            formatStr = GenerateCurrencyFormatString(maxLength - decimalLength, true);

            if (decimalLength > 0)
            {
                formatStr += ".";
            }

            for (int i = 0; i < decimalLength; i++)
            {
                formatStr = formatStr.Insert(formatStr.Length, "0");
            }

            return formatStr;
        }

        /// <summary>
        /// split을 구분자로 문자열을 나눕니다.
        /// </summary>
        /// <param name="testString">원본 문자열</param>
        /// <param name="split">구분 문자열</param>
        /// <returns>구분자를 기준으로 분리한 string배열이 리턴됩니다.</returns>
        public static string[] SplitByString(string str, string split)
        {
            int offset = 0;
            int index = 0;
            int[] offsets = new int[str.Length + 1];

            while (index < str.Length)
            {
                int indexOf = str.IndexOf(split, index);
                if (indexOf != -1)
                {
                    offsets[offset++] = indexOf;
                    index = (indexOf + split.Length);
                }
                else
                {
                    index = str.Length;
                }
            }

            string[] final = new string[offset + 1];
            if (offset == 0)
            {
                final[0] = str;
            }
            else
            {
                offset--;
                final[0] = str.Substring(0, offsets[0]);
                for (int i = 0; i < offset; i++)
                {
                    final[i + 1] = str.Substring(offsets[i] + split.Length, offsets[i + 1] - offsets[i] - split.Length);
                }
                final[offset + 1] = str.Substring(offsets[offset] + split.Length);
            }
            return final;
        }

        /// <summary>
        /// 바 문자(-)를 제거한 사업자번호 길이 체크합니다.
        /// </summary>
        /// <param name="aBiz">체크할 사업자 번호입니다.</param>
        /// <returns>사업자 번호의 길이가 10자리면 true 아니면 false입니다.</returns>
        static public bool BizNumLengthCheck(string aBiz)
        {
            string tmp = aBiz.Trim().Replace("-", "");
            tmp = tmp.Replace("_", "");

            if (tmp.Length == 10) return true;
            else return false;
        }

        /// <summary>
        /// 넘어온 사업자번호를 가공합니다.
        /// </summary>
        /// <param name="aBiz">원본 문자열입니다.</param>
        /// <param name="addDash">사업자번호에 -를 붙일지, 뺄지 여부를 나타냅니다.</param>
        /// <returns>가공된 결과를 리턴합니다. 결과값의 길이가 10이 아닐경우 빈 문자열을 리턴합니다.</returns>
        static public string MakeBizNum(string aBiz, bool addDash)
        {
            if (addDash)
            {
                if (aBiz.Length == 10)
                    return aBiz.Substring(0, 3) + "-" + aBiz.Substring(3, 2) + "-" + aBiz.Substring(5, 5);
            }
            else
            {
                string tmp = aBiz.Trim().Replace("-", "");
                if (tmp.Length == 10)
                    return tmp;
            }

            return string.Empty;
        }

        /// <summary>
        /// 넘어온 주민등록번호를 가공합니다.
        /// </summary>
        /// <param name="aBiz">원본 문자열입니다.</param>
        /// <param name="addDash">주민등록번호에 -를 붙일지, 뺄지 여부를 나타냅니다.</param>
        /// <returns>가공된 결과를 리턴합니다. 결과값의 길이가 13이 아닐경우 빈 문자열을 리턴합니다.</returns>
        static public string MakeJuminNum(string aBiz, bool addDash)
        {
            if (addDash)
            {
                if (aBiz.Length == 13)
                    return aBiz.Substring(0, 6) + "-" + aBiz.Substring(6, 7);
            }
            else
            {
                string tmp = aBiz.Trim().Replace("-", "");
                if (tmp.Length == 13)
                    return tmp;
            }

            return string.Empty;
        }
    }
}