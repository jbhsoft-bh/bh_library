﻿namespace BH_Library.Utils
{
    /// <summary>
    /// MS-ACCESS QUERY 변환에 대한 Convert 메서드를 제공합니다.
    /// </summary>
    public class ConvertMdbQuery
    {
        /// <summary>
        /// 입력된 문자열을 분리하여 MS-ACCESS에서 사용할 쿼리로 분리된 string배열을 반환합니다.
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static string[] MdbQuery(string strQuery)
        {
            string[] MS_ACCESS_QUERY_SPLIT = StringUtil.SplitByString(strQuery, "--MS_ACCESS--");
            MS_ACCESS_QUERY_SPLIT[1] = MS_ACCESS_QUERY_SPLIT[1].Replace("\r\n", "");
            MS_ACCESS_QUERY_SPLIT[1] = MS_ACCESS_QUERY_SPLIT[1].Replace("\t", " ");
            MS_ACCESS_QUERY_SPLIT[1] = MS_ACCESS_QUERY_SPLIT[1].Replace("/*", "");
            MS_ACCESS_QUERY_SPLIT[1] = MS_ACCESS_QUERY_SPLIT[1].Replace("*/", "");
            string[] MS_ACCESS_QUERY = StringUtil.SplitByString(MS_ACCESS_QUERY_SPLIT[1], "--");

            if (MS_ACCESS_QUERY != null && MS_ACCESS_QUERY.Length > 0)
            {
                for (int idx = 0; idx < MS_ACCESS_QUERY.Length; idx++)
                {
                    MS_ACCESS_QUERY[idx] = MS_ACCESS_QUERY[idx].Trim();
                }
            }

            return MS_ACCESS_QUERY;
        }
    }
}