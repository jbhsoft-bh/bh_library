﻿using System;
using System.Windows.Forms;

namespace BH_Library.GlobalHock
{
    public class GlobalDetecting
    {
        public delegate void StopDetectingEventHandler(object sender, int stopSecond, DateTime dectectingTime);

        public event StopDetectingEventHandler StopDetecting;

        private int MaximunStopSecond = 600;
        private Timer timer = null;

        public GlobalDetecting(int maximunStopSecond)
        {
            MaximunStopSecond = maximunStopSecond;
            timer = new Timer();
            timer.Enabled = false;
            timer.Interval = MaximunStopSecond * 1000;
            timer.Stop();
            timer.Tick += timer_Tick;

            HookManager.KeyDown += HookManager_KeyEvent;
            HookManager.KeyUp += HookManager_KeyEvent;
            HookManager.MouseWheel += HookManager_MouseEvent;
            HookManager.MouseDoubleClick += HookManager_MouseEvent;
            HookManager.MouseDown += HookManager_MouseEvent;
            HookManager.MouseUp += HookManager_MouseEvent;
            HookManager.MouseClick += HookManager_MouseEvent;
            HookManager.MouseMove += HookManager_MouseEvent;
        }

        private void HookManager_MouseEvent(object sender, MouseEventArgs e)
        {
            ReStart();
        }

        private void HookManager_KeyEvent(object sender, KeyEventArgs e)
        {
            ReStart();
        }

        public void Stop()
        {
            timer.Stop();
            timer.Enabled = false;
        }

        public void Start()
        {
            timer.Start();
        }

        public void ChangeInterval(int second)
        {
            MaximunStopSecond = second * 1000;
        }

        public void ReStart()
        {
            if (timer.Enabled)
            {
                timer.Stop();
                timer.Start();
            }
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if (StopDetecting != null)
            {
                StopDetecting(this, MaximunStopSecond, DateTime.Now);
            }
        }
    }
}