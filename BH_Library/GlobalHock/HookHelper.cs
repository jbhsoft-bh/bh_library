﻿using BH_Library.Utils;
using System;
using System.Windows.Forms;

namespace BH_Library.GlobalHock
{
    public enum InputTypes
    {
        MouseMove,
        MouseClick,
        MouseDown,
        MouseUp,
        MouseDoubleClick,
        MouseWheel,
        KeyDown,
        KeyPress,
        Keyup
    }

    public class HookHelper
    {
        public delegate void DetectInputValueEventHendler(InputTypes type, string value);

        public event DetectInputValueEventHendler DetectInputValueEvent;

        public delegate void DetectSleepingEventHendler(DateTime LockTime);

        public event DetectSleepingEventHendler DetectSleepingEvent;

        private Timer SleepingTimer = null;

        /// <summary>
        /// 전역 후킹 프로세스 탐지기
        /// </summary>
        /// <param name="DetectsleepingSeconds">0이하일 경우 입력슬리핑 감지 안함</param>
        /// <param name="DetectMouseMove">마우스 포인터 무빙 감지여부</param>
        /// <param name="DetectMouseClick">마우스 버튼 클릭 감지 여부</param>
        /// <param name="DetectMouseDown">마우스 버튼 다운 감지 여부</param>
        /// <param name="DetectMouseUp">마우스 버튼 업 감지여부</param>
        /// <param name="DetectMouseDoubleClick">마우스 더블클릭 감지여부</param>
        /// <param name="DetectMouseWheel">마우스 휠 감지여부</param>
        /// <param name="DetectKeyDown">키 다운 감지여부</param>
        /// <param name="DetectKeyPress">키 프레스 감지 여부</param>
        /// <param name="DetectKeyup">키 업 감지여부</param>
        public HookHelper(int DetectsleepingSeconds, bool DetectMouseMove = true, bool DetectMouseClick = true, bool DetectMouseDown = true, bool DetectMouseUp = true, bool DetectMouseDoubleClick = true, bool DetectMouseWheel = true, bool DetectKeyDown = true, bool DetectKeyPress = true, bool DetectKeyup = true)
        {
            if (DetectsleepingSeconds > 0)
            {
                SleepingTimer = new Timer();
                SleepingTimer.Enabled = false;
                SleepingTimer.Interval = DetectsleepingSeconds * 1000;
                SleepingTimer.Tick += SleepingTimer_Tick;
            }

            if (DetectMouseMove)
                HookManager.MouseMoveExt += HookManager_MouseMove;
            if (DetectMouseClick)
                HookManager.MouseClick += HookManager_MouseClick;
            if (DetectMouseDown)
                HookManager.MouseDown += HookManager_MouseDown;
            if (DetectMouseUp)
                HookManager.MouseUp += HookManager_MouseUp;
            if (DetectMouseDoubleClick)
                HookManager.MouseDoubleClick += HookManager_MouseDoubleClick;
            if (DetectMouseWheel)
                HookManager.MouseWheel += HookManager_MouseWheel;
            if (DetectKeyDown)
                HookManager.KeyDown += HookManager_KeyDown;
            if (DetectKeyPress)
                HookManager.KeyPress += HookManager_KeyPress;
            if (DetectKeyup)
                HookManager.KeyUp += HookManager_KeyUp;
        }

        private void SleepingTimer_Tick(object sender, EventArgs e)
        {
            SleepingTimer.Stop();
            DetectSleepingEvent?.Invoke(DateTime.Now);
        }

        private void ReSetTimer()
        {
            if (SleepingTimer == null)
                return;
            SleepingTimer.Stop();
            SleepingTimer.Start();
        }

        private void HookManager_KeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            DetectInputValueEvent?.Invoke(InputTypes.Keyup, e.KeyCode.ToStringEx());
            ReSetTimer();
        }

        private void HookManager_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            DetectInputValueEvent?.Invoke(InputTypes.KeyPress, e.KeyChar.ToStringEx());
            ReSetTimer();
        }

        private void HookManager_MouseWheel(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            DetectInputValueEvent?.Invoke(InputTypes.MouseWheel, string.Format("Wheel={0:000}", e.Delta));
            ReSetTimer();
        }

        private void HookManager_MouseDoubleClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            DetectInputValueEvent?.Invoke(InputTypes.MouseDoubleClick, e.Button.ToStringEx());
            ReSetTimer();
        }

        private void HookManager_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            DetectInputValueEvent?.Invoke(InputTypes.MouseUp, e.Button.ToStringEx());
            ReSetTimer();
        }

        private void HookManager_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            DetectInputValueEvent?.Invoke(InputTypes.MouseDown, e.Button.ToStringEx());
            ReSetTimer();
        }

        private void HookManager_MouseClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            DetectInputValueEvent?.Invoke(InputTypes.MouseClick, e.Button.ToStringEx());
            ReSetTimer();
        }

        private void HookManager_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            DetectInputValueEvent?.Invoke(InputTypes.MouseMove, string.Format("x={0:0000}; y={1:0000}", e.X, e.Y));
            ReSetTimer();
        }

        private void HookManager_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            DetectInputValueEvent?.Invoke(InputTypes.KeyDown, e.KeyCode.ToStringEx());
            ReSetTimer();
        }
    }
}