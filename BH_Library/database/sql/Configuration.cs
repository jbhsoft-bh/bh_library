﻿using System.Configuration;

namespace BH_Library.Database.Sql
{
    // App.config
    // <?xml version="1.0" encoding="utf-8" ?>
    //    <configuration>
    //      <appSettings>
    //        <add key="defaultConnection" value="sqlCon" />
    //      </appSettings>
    //      <connectionStrings>
    //        <add name="sqlCon" connectionString="Data Source=10.91.152.51;Initial Catalog=DALC4NET_DB;User ID=sa;Password=sa" providerName="System.Data.SqlClient"/>
    //          <add name="mySqlCon" connectionString ="Server=localhost;Port=3306;Database=dalc4net_db;Uid=root;Pwd=P@ssw0rd;allow user variables=true" providerName="MySql.Data.MySqlClient"/>
    //          <add name="oracleCon" connectionString ="Server=localhost;Port=3306;Database=dalc4net_db;Uid=root;Pwd=P@ssw0rd;allow user variables=true" providerName="System.Data.OracleClient"/>
    //         <add name="accessCon" connectionString="Provider=Microsoft.Jet.OLEDB.4.0;Data Source=E:\Program Files\AccountPlus\Database\AccountPlus.mdb;Jet OLEDB:Database Password=admin;" providerName="System.Data.OleDb" />
    //      </connectionStrings>
    //      <system.data>
    //        <DbProviderFactories>
    //          <add name="MySQL Data Provider" invariant="MySql.Data.MySqlClient" description=".Net Framework Data Provider for MySQL" type="MySql.Data.MySqlClient.MySqlClientFactory, MySql.Data,Version=6.0.3.0,Culture=neutral,PublicKeyToken=c5687fc88969c44d"/>
    //        </DbProviderFactories>
    //      </system.data>
    //    </configuration>

    /// <summary>
    /// 데이타베이스 설정정보를 제공합니다.
    /// </summary>
    public class Configuration
    {
        private DatabaseType _databaseType = DatabaseType.MSSQL;
        private string _serverIp = "127.0.0.1";
        private int _port = 0;
        private string _userId = "sa";
        private string _userPassword = string.Empty;
        private string _databaseName = string.Empty;
        private string _databaseFile = string.Empty;
        private string _providerName = string.Empty;
        private string _connectionString = string.Empty;
        private string _defaultConnectionKey = string.Empty;
        private const string DEFAULT_CONNECTION_KEY = "defaultConnection";

        /// <summary>
        /// 데이타베이스 기본 연결키를 제공합니다.
        /// </summary>
        public string DefaultConnectionKey
        {
            get { return _defaultConnectionKey; }
            private set { _defaultConnectionKey = value; }
        }

        /// <summary>
        /// 데이타베이스 연결정보 문자을을 제공합니다.
        /// </summary>
        public string ConnectionString
        {
            get { return _connectionString; }
            private set { _connectionString = value; }
        }

        /// <summary>
        /// 데이타베이스 프로바이저 이름을 제공합니다.
        /// </summary>
        public string ProviderName
        {
            get { return _providerName; }
            private set { _providerName = value; }
        }

        /// <summary>
        /// 데이타베이스 타입을 제공하거나 설정합니다.
        /// </summary>
        public DatabaseType DatabaseType
        {
            get { return _databaseType; }
            set { _databaseType = value; }
        }

        /// <summary>
        /// 데이타베이스 서버 IP를 제공하거나 설정합니다.
        /// </summary>
        public string ServerIp
        {
            get { return _serverIp; }
            set { _serverIp = value; }
        }

        /// <summary>
        /// 데이타베이스 포트을 제공하거나 설정합니다.
        /// </summary>
        public int Port
        {
            get { return _port; }
            set { _port = value; }
        }

        /// <summary>
        /// 데이타베이스 사용자 아이디를 제공하거나 설정합니다.
        /// </summary>
        public string UserId
        {
            get { return _userId; }
            set { _userId = value; }
        }

        /// <summary>
        /// 데이타베이스 패스워드를 제공하거나 설정합니다.
        /// </summary>
        public string UserPassword
        {
            get { return _userPassword; }
            set { _userPassword = value; }
        }

        /// <summary>
        /// 데이타베이스 이름을 제공하거나 설정합니다.
        /// </summary>
        public string DatabaseName
        {
            get { return _databaseName; }
            set { _databaseName = value; }
        }

        /// <summary>
        /// 데이타베이스 파일이름을 제공하거나 설정합니다.
        /// </summary>
        public string DatabaseFile
        {
            get { return _databaseFile; }
            set { _databaseFile = value; }
        }

        /// <summary>
        /// 데이타베이스 연결정보의 기본 생성자를 제공합니다.
        /// App.config에서 연결정보를 가져옵니다.
        /// </summary>
        public Configuration()
        {
            _defaultConnectionKey = ConfigurationManager.AppSettings[DEFAULT_CONNECTION_KEY];
            _connectionString = ConfigurationManager.ConnectionStrings[_defaultConnectionKey].ConnectionString;
            _providerName = ConfigurationManager.ConnectionStrings[_defaultConnectionKey].ProviderName;
        }

        /// <summary>
        /// 데이타베이스 연결정보의 기본 생성자를 제공합니다.(MSSQL 기본)
        /// </summary>
        /// <param name="connectString">데이타베이스연결 문자열</param>
        public Configuration(string connectString)
        {
            _databaseType = DatabaseType.MSSQL;
            _connectionString = connectString;
            _providerName = "System.Data.SqlClient";
        }

        /// <summary>
        /// 데이타베이스 연결정보의 기본 생성자를 제공합니다.
        /// </summary>
        /// <param name="databaseType">데이타베이스 타입</param>
        /// <param name="connectString">데이타베이스연결 문자열</param>
        public Configuration(DatabaseType databaseType, string connectString)
        {
            _databaseType = databaseType;
            _connectionString = connectString;
            switch (databaseType)
            {
                // MS SQL 연결
                case DatabaseType.MSSQL:
                    _providerName = "System.Data.SqlClient";
                    break;

                // MySQL 연결
                case DatabaseType.MySQL:
                    _providerName = "MySql.Data.MySqlClient";
                    break;

                // Oracle 연결
                case DatabaseType.Oracle:
                    _providerName = "System.Data.OracleClient";
                    break;

                // MSAcess 연결
                case DatabaseType.MSAccess:
                    _providerName = "System.Data.OleDb";
                    break;
            }
        }

        /// <summary>
        /// 데이타베이스 연결정보의 기본 생성자를 제공합니다.
        /// </summary>
        /// <param name="databaseType">데이타베이스 타입</param>
        /// <param name="serverIp">데이타베이스 서버 IP</param>
        /// <param name="databaseName">데이타베이스 이름</param>
        /// <param name="userId">사용자 아이디</param>
        /// <param name="userPassword">사용자 패스워드</param>
        /// <param name="port">포트</param>
        /// <param name="databaseFile">데이타베이스 파일(파일데이타베이스 일 경우)</param>
        public Configuration(DatabaseType databaseType, string serverIp = null, string databaseName = null, string userId = null, string userPassword = null, int port = 0, string databaseFile = null)
        {
            _databaseType = databaseType;
            _serverIp = serverIp;
            _databaseName = databaseName;
            _userId = userId;
            _userPassword = userPassword;
            _port = port;
            _databaseFile = databaseFile;

            switch (databaseType)
            {
                // MS SQL 연결
                case DatabaseType.MSSQL:
                    _providerName = "System.Data.SqlClient";
                    _connectionString = string.Format("Data Source={0}{1};Initial Catalog={2};User ID={3};Password={4}",
                        serverIp,
                        "," + port,
                        databaseName,
                        userId,
                        userPassword);
                    break;

                // MySQL 연결
                case DatabaseType.MySQL:
                    _providerName = "MySql.Data.MySqlClient";
                    _connectionString = string.Format("Server={0};Port={1};Database={2};Uid={3};Pwd={4};allow user variables=true",
                        serverIp,
                        port,
                        databaseName,
                        userId,
                        userPassword);
                    break;

                // Oracle 연결
                case DatabaseType.Oracle:
                    _providerName = "System.Data.OracleClient";
                    _connectionString = string.Format("Data Source={0};User Id={1};Password={2};Pooling=False;", // 정확치 않음
                        serverIp,
                        userId,
                        userPassword);
                    break;

                // MSAcess 연결
                case DatabaseType.MSAccess:
                    _providerName = "System.Data.OleDb";
                    _connectionString = string.Format(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Jet OLEDB:Database Password={1};", // 정확치 않음
                        databaseFile,
                        userPassword);
                    break;
            }
        }
    }

    /// <summary>
    /// 데이타베이스 타입을 제공합니다.
    /// </summary>
    public enum DatabaseType
    {
        MSSQL = 1,
        Oracle = 2,
        MySQL = 3,
        MSAccess = 4
    }
}