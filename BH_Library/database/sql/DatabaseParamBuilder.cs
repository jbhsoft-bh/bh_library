﻿using System.Collections.Generic;
using System.Data.Common;

namespace BH_Library.Database.Sql
{
    internal class DatabaseParamBuilder
    {
        private string _providerName = string.Empty;
        private AssemblyProvider _assemblyProvider = null;

        internal DatabaseParamBuilder(string providerName)
        {
            _assemblyProvider = new AssemblyProvider(providerName);
            _providerName = providerName;
        }

        internal DbParameter GetParameter(DatabaseParameter parameter)
        {
            DbParameter dbParam = GetParameter();
            dbParam.ParameterName = parameter.Name;
            dbParam.Value = parameter.Value;
            dbParam.Direction = parameter.ParamDirection;
            dbParam.DbType = parameter.Type;

            return dbParam;
        }

        internal List<DbParameter> GetParameterCollection(DatabaseParameterCollection parameterCollection)
        {
            List<DbParameter> dbParamCollection = new List<DbParameter>();
            DbParameter dbParam = null;
            foreach (DatabaseParameter param in parameterCollection.Parameters)
            {
                dbParam = GetParameter(param);
                dbParamCollection.Add(dbParam);
            }

            return dbParamCollection;
        }

        #region Private Methods

        private DbParameter GetParameter()
        {
            //string typeName = AssemblyProvider.GetInstance().GetParameterType();
            //IDbDataParameter dbParam = (IDbDataParameter)AssemblyProvider.GetInstance().DBProviderAssembly.CreateInstance(typeName);
            //return dbParam;

            //DatabaseParameter dbParam = AssemblyProvider.GetInstance(this._providerName).Factory.CreateParameter();
            DbParameter dbParam = _assemblyProvider.Factory.CreateParameter();
            return dbParam;
        }

        #endregion Private Methods
    }
}