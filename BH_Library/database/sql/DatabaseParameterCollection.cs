﻿using System.Collections.Generic;

namespace BH_Library.Database.Sql
{
    public class DatabaseParameterCollection
    {
        private List<DatabaseParameter> _parameterCollection = new List<DatabaseParameter>();

        /// <summary>
        /// Adds a DatabaseParameter to the ParameterCollection
        /// </summary>
        /// <param name="parameter">Parameter to be added</param>
        public void Add(DatabaseParameter parameter)
        {
            _parameterCollection.Add(parameter);
        }

        /// <summary>
        /// Removes parameter from the Parameter Collection
        /// </summary>
        /// <param name="parameter">Parameter to be removed</param>
        public void Remove(DatabaseParameter parameter)
        {
            _parameterCollection.Remove(parameter);
        }

        /// <summary>
        /// Removes all the parameters from the Parameter Collection
        /// </summary>
        public void RemoveAll()
        {
            _parameterCollection.RemoveRange(0, _parameterCollection.Count - 1);
        }

        /// <summary>
        /// Removes parameter from the specified index.
        /// </summary>
        /// <param name="index">Index from which parameter is supposed to be removed</param>
        public void RemoveAt(int index)
        {
            _parameterCollection.RemoveAt(index);
        }

        /// <summary>
        /// Gets list of parameters
        /// </summary>
        internal List<DatabaseParameter> Parameters
        {
            get
            {
                return _parameterCollection;
            }
        }
    }
}