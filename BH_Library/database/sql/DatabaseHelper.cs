using System;
using System.Data;

namespace BH_Library.Database.Sql
{
    /// <summary>
    /// 데이타베이스을 이용한 관리기능을 제공합니다.
    /// </summary>
    public class DatabaseHelper
    {
        private ConnectionManager _connectionManager = null;
        private CommandBuilder _commandBuilder = null;
        private DataAdapterManager _dbAdapterManager = null;
        private static DatabaseHelper _databaseHelper = null;
        private IDbConnection _connection = null;
        private string _providerName = string.Empty;
        private AssemblyProvider _assemblyProvider = null;

        #region Constructor

        /// <summary>
        /// 데이타베이스 연결을 지원합니다.
        /// </summary>
        /// <param name="configuration">데이타베이스 연결 설정정보</param>
        public DatabaseHelper(Configuration configuration)
        {
            // 데이타베이스 연결 초기화
            _connectionManager = new ConnectionManager(configuration.ConnectionString, configuration.ProviderName);
            _commandBuilder = new CommandBuilder(configuration.ProviderName);
            _dbAdapterManager = new DataAdapterManager(configuration.ProviderName);
            _connection = _connectionManager.GetConnection();
            _providerName = _connectionManager.ProviderName;
            _assemblyProvider = new AssemblyProvider(_providerName);
        }

        /// <summary>
        /// 데이타베이스 연결을 위한 단일 인스턴스를 제공합니다.
        /// </summary>
        /// <param name="configuration">데이타베이스 연결 설정정보</param>
        /// <returns></returns>
        public static DatabaseHelper GetInstance(Configuration configuration)
        {
            if (_databaseHelper == null)
                _databaseHelper = new DatabaseHelper(configuration);

            return _databaseHelper;
        }

        #endregion Constructor

        #region Transaction Methods

        /// <summary>
        /// 트랜잭션을 시작하는 기능을 제공합니다.
        /// 트랜젹션 처리 완료후 CommitRansaction을 호출하십시요
        /// </summary>
        public IDbTransaction BeginTransaction()
        {
            return GetConnection().BeginTransaction();
        }

        /// <summary>
        /// 데이타베이스의 변경이 완료된 후의 Commit기능을 제공합니다.
        /// </summary>
        /// <param name="transaction">Database Transcation to be committed</param>
        public void CommitTransaction(IDbTransaction transaction)
        {
            try
            {
                transaction.Commit();
            }
            catch (Exception err)
            {
                throw err;
            }
        }

        /// <summary>
        /// 데이타베이스 변경이 실패되었을대의 Rollback기능을 제공합니다.
        /// </summary>
        /// <param name="transaction">Database Transaction to be rolled back</param>
        public void RollbackTransaction(IDbTransaction transaction)
        {
            try
            {
                transaction.Rollback();
            }
            catch (Exception err)
            {
                throw err;
            }
        }

        /// <summary>
        /// 데이타베이스 연결문자열을 제공합니다.
        /// </summary>
        public string ConnectionString
        {
            get
            {
                return _connectionManager.ConnectionString;
            }
        }

        /// <summary>
        /// 연결된 데이타베이스 이름을 제공합니다.
        /// </summary>
        public string Database
        {
            get
            {
                IDbConnection connection = _assemblyProvider.Factory.CreateConnection();
                connection.ConnectionString = ConnectionString;
                return connection.Database;
            }
        }

        /// <summary>
        /// 데이타베이스 프로바이더를 제공합니다.
        /// </summary>
        public string Provider
        {
            get
            {
                return _providerName;
            }
        }

        /// <summary>
        /// 데이타베이스 연결객체를 제공합니다.
        /// </summary>
        /// <returns>Database connection object in the opened state. </returns>
        public IDbConnection GetConnection()
        {
            return _connectionManager.GetConnection();
        }

        /// <summary>
        /// 데이타베이스 연결을 닫는기능을 지원합니다.
        /// </summary>
        /// <returns></returns>
        public bool Close()
        {
            bool retVal = true;

            if (_databaseHelper != null && _connection.State != ConnectionState.Closed)
            {
                _connection.Close();
                if (_connection.State == ConnectionState.Closed)
                {
                    retVal = true;
                    if (_connectionManager != null) _connectionManager = null;
                    if (_commandBuilder != null) _commandBuilder = null;
                    if (_dbAdapterManager != null) _dbAdapterManager = null;
                    if (_assemblyProvider != null) _assemblyProvider = null;
                    if (_connection != null) _connection = null;
                    if (_databaseHelper != null) _databaseHelper = null;
                }
                else
                {
                    retVal = false;
                }
            }

            return retVal;
        }

        #endregion Transaction Methods

        #region ExecuteScalar

        /// <summary>
        /// 프로시져 또는 쿼리문에 대한 실행결과를 오브젝트 형태로 제공합니다.
        /// </summary>
        /// <param name="commandText">Sql Command or Stored Procedure name</param>
        /// <param name="commandType">Type of command (i.e. Sql Command/ Stored Procedure name/ Table Direct)</param>
        /// <returns>A single value. (First row's first cell value, if more than one row and column is returned.)</returns>
        public object ExecuteScalar(string commandText, CommandType commandType)
        {
            return ExecuteScalar(commandText, (IDbTransaction)null, commandType);
        }

        /// <summary>
        /// 프로시져 또는 쿼리문에 대한 실행결과를 오브젝트 형태로 제공합니다.(트랜잭션 처리)
        /// </summary>
        /// <param name="commandText">Sql Command or Stored Procedure name</param>
        /// <param name="transaction">Current Database Transaction (Use Helper.Transaction to get transaction)</param>
        /// <param name="commandType">Text or Stored Procedure</param>
        /// <returns>A single value. (First row's first cell value, if more than one row and column is returned.)</returns>
        public object ExecuteScalar(string commandText, IDbTransaction transaction, CommandType commandType)
        {
            return ExecuteScalar(commandText, new DatabaseParameterCollection(), transaction, commandType);
        }

        /// <summary>
        /// 프로시져 또는 쿼리문에 대한 실행결과를 오브젝트 형태로 제공합니다.
        /// </summary>
        /// <param name="commandText">Sql Command or Stored Procedure name</param>
        /// <param name="param">Parameter to be associated</param>
        /// <param name="commandType">Type of command (i.e. Sql Command/ Stored Procedure name/ Table Direct)</param>
        /// <returns>A single value. (First row's first cell value, if more than one row and column is returned.)</returns>
        public object ExecuteScalar(string commandText, DatabaseParameter param, CommandType commandType)
        {
            return ExecuteScalar(commandText, param, null, commandType);
        }

        /// <summary>
        /// 프로시져 또는 쿼리문에 대한 실행결과를 오브젝트 형태로 제공합니다.(트랜잭션 처리)
        /// </summary>
        /// <param name="commandText">Sql Command or Stored Procedure name</param>
        /// <param name="param">Database parameter</param>
        /// <param name="transaction">Current Database Transaction (Use Helper.Transaction to get transaction)</param>
        /// <param name="commandType">Text or Stored Procedure</param>
        /// <returns>A single value. (First row's first cell value, if more than one row and column is returned.)</returns>
        public object ExecuteScalar(string commandText, DatabaseParameter param, IDbTransaction transaction, CommandType commandType)
        {
            DatabaseParameterCollection paramCollection = new DatabaseParameterCollection();
            paramCollection.Add(param);
            return ExecuteScalar(commandText, paramCollection, transaction, commandType);
        }

        /// <summary>
        /// 프로시져 또는 쿼리문에 대한 실행결과를 오브젝트 형태로 제공합니다.
        /// </summary>
        /// <param name="commandText">Sql Command or Stored Procedure name</param>
        /// <param name="paramCollection">Parameter collection to be associated</param>
        /// <param name="commandType">Type of command (i.e. Sql Command/ Stored Procedure name/ Table Direct)</param>
        /// <returns>A single value. (First row's first cell value, if more than one row and column is returned.)</returns>
        public object ExecuteScalar(string commandText, DatabaseParameterCollection paramCollection, CommandType commandType)
        {
            return ExecuteScalar(commandText, paramCollection, (IDbTransaction)null, commandType);
        }

        /// <summary>
        /// 프로시져 또는 쿼리문에 대한 실행결과를 오브젝트 형태로 제공합니다.(트랜잭션 처리)
        /// </summary>
        /// <param name="commandText">Sql Command or Stored Procedure name</param>
        /// <param name="paramCollection">Database parameter Collection</param>
        /// <param name="transaction">Current Database Transaction (Use Helper.Transaction to get transaction)</param>
        /// <param name="commandType">Text or Stored Procedure</param>
        /// <returns>A single value. (First row's first cell value, if more than one row and column is returned.)</returns>
        public object ExecuteScalar(string commandText, DatabaseParameterCollection paramCollection, IDbTransaction transaction, CommandType commandType)
        {
            object objScalar = null;
            IDbConnection connection = transaction != null ? transaction.Connection : _connectionManager.GetConnection();
            IDbCommand command = _commandBuilder.GetCommand(commandText, connection, paramCollection, commandType);
            command.Transaction = transaction;
            try
            {
                objScalar = command.ExecuteScalar();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (transaction == null)
                {
                    if (connection != null)
                    {
                        connection.Close();
                        connection.Dispose();
                    }
                }

                if (command != null)
                    command.Dispose();
            }
            return objScalar;
        }

        /// <summary>
        /// 쿼리문에 대한 실행결과를 오브젝트 형태로 제공합니다.
        /// </summary>
        /// <param name="commandText">Sql Command</param>
        /// <returns>A single value. (First row's first cell value, if more than one row and column is returned.)</returns>
        public object ExecuteScalar(string commandText)
        {
            return ExecuteScalar(commandText, (IDbTransaction)null);
        }

        /// <summary>
        /// 쿼리문에 대한 실행결과를 오브젝트 형태로 제공합니다.(트랜잭션 처리)
        /// </summary>
        /// <param name="commandText">Sql Command </param>
        /// <param name="transaction">Current Database Transaction (Use Helper.Transaction to get transaction)</param>
        /// <returns>A single value. (First row's first cell value, if more than one row and column is returned.)</returns>
        public object ExecuteScalar(string commandText, IDbTransaction transaction)
        {
            return ExecuteScalar(commandText, transaction, CommandType.Text);
        }

        /// <summary>
        /// 쿼리문에 대한 실행결과를 오브젝트 형태로 제공합니다.
        /// </summary>
        /// <param name="commandText">Sql Command</param>
        /// <param name="param">Parameter to be associated</param>
        /// <returns>A single value. (First row's first cell value, if more than one row and column is returned.)</returns>
        public object ExecuteScalar(string commandText, DatabaseParameter param)
        {
            return ExecuteScalar(commandText, param, (IDbTransaction)null);
        }

        /// <summary>
        /// 쿼리문에 대한 실행결과를 오브젝트 형태로 제공합니다.(트랜잭션 처리)
        /// </summary>
        /// <param name="commandText">Sql Command</param>
        /// <param name="param">Parameter to be associated</param>
        /// <param name="transaction">Database Transacion</param>
        /// <returns>A single value. (First row's first cell value, if more than one row and column is returned.)</returns>
        public object ExecuteScalar(string commandText, DatabaseParameter param, IDbTransaction transaction)
        {
            return ExecuteScalar(commandText, param, transaction, CommandType.Text);
        }

        /// <summary>
        /// 쿼리문에 대한 실행결과를 오브젝트 형태로 제공합니다.
        /// </summary>
        /// <param name="commandText">Sql Command</param>
        /// <param name="paramCollection">Parameter collection to be associated.</param>
        /// <returns>A single value. (First row's first cell value, if more than one row and column is returned.)</returns>
        public object ExecuteScalar(string commandText, DatabaseParameterCollection paramCollection)
        {
            return ExecuteScalar(commandText, paramCollection, null);
        }

        /// <summary>
        /// 쿼리문에 대한 실행결과를 오브젝트 형태로 제공합니다.(트랜잭션 처리)
        /// </summary>
        /// <param name="commandText">Sql Command</param>
        /// <param name="paramCollection">Database  Parameter Collection</param>
        /// <param name="transaction">Database Transacion (Use DatabaseHelper.Transaction property.)</param>
        /// <returns></returns>
        public object ExecuteScalar(string commandText, DatabaseParameterCollection paramCollection, IDbTransaction transaction)
        {
            return ExecuteScalar(commandText, paramCollection, transaction, CommandType.Text);
        }

        #endregion ExecuteScalar

        #region ExecuteNonQuery

        /// <summary>
        /// 프로시져 또는 쿼리문에 대한 실행결과에 대한 처리된 건수를 제공합니다.
        /// </summary>
        /// <param name="commandText">Sql Command or Stored Procedure name</param>
        /// <param name="commandType">Type of command (i.e. Sql Command/ Stored Procedure name/ Table Direct)</param>
        /// <returns>Number of rows affected.</returns>
        public int ExecuteNonQuery(string commandText, CommandType commandType)
        {
            return ExecuteNonQuery(commandText, (IDbTransaction)null, commandType);
        }

        /// <summary>
        /// 프로시져 또는 쿼리문에 대한 실행결과에 대한 처리된 건수를 제공합니다.(트랜잭션 처리)
        /// </summary>
        /// <param name="commandText">Sql Command or Stored Procedure name</param>
        /// <param name="transaction">Current Database Transaction (Use Helper.Transaction to get transaction)</param>
        /// <param name="commandType">Type of command (i.e. Sql Command/ Stored Procedure name/ Table Direct)</param>
        /// <returns>Number of rows affected.</returns>
        public int ExecuteNonQuery(string commandText, IDbTransaction transaction, CommandType commandType)
        {
            return ExecuteNonQuery(commandText, new DatabaseParameterCollection(), transaction, commandType);
        }

        /// <summary>
        /// 프로시져 또는 쿼리문에 대한 실행결과에 대한 처리된 건수를 제공합니다.
        /// </summary>
        /// <param name="commandText">Sql Command or Stored Procedure name</param>
        /// <param name="param">Parameter to be associated with the command</param>
        /// <param name="commandType">Type of command (i.e. Sql Command/ Stored Procedure name/ Table Direct)</param>
        /// <returns>Number of rows affected.</returns>
        public int ExecuteNonQuery(string commandText, DatabaseParameter param, CommandType commandType)
        {
            return ExecuteNonQuery(commandText, param, (IDbTransaction)null, commandType);
        }

        /// <summary>
        /// 프로시져 또는 쿼리문에 대한 실행결과에 대한 처리된 건수를 제공합니다.(트랜잭션 처리)
        /// </summary>
        /// <param name="commandText">Sql Command </param>
        /// <param name="param">Parameter to be associated with the command</param>
        /// <param name="transaction">Current Database Transaction (Use Helper.Transaction to get transaction)</param>
        /// <param name="commandType">Type of command (i.e. Sql Command/ Stored Procedure name/ Table Direct)</param>
        /// <returns>Number of rows affected.</returns>
        public int ExecuteNonQuery(string commandText, DatabaseParameter param, IDbTransaction transaction, CommandType commandType)
        {
            DatabaseParameterCollection paramCollection = new DatabaseParameterCollection();
            paramCollection.Add(param);
            return ExecuteNonQuery(commandText, paramCollection, transaction, commandType);
        }

        /// <summary>
        /// 프로시져 또는 쿼리문에 대한 실행결과에 대한 처리된 건수를 제공합니다.
        /// </summary>
        /// <param name="commandText">Sql Command or Stored Procedure name</param>
        /// <param name="paramCollection">Parameter collection to be associated with the command</param>
        /// <param name="commandType">Type of command (i.e. Sql Command/ Stored Procedure name/ Table Direct)</param>
        /// <returns>Number of rows effected.</returns>
        public int ExecuteNonQuery(string commandText, DatabaseParameterCollection paramCollection, CommandType commandType)
        {
            return ExecuteNonQuery(commandText, paramCollection, (IDbTransaction)null, commandType);
        }

        /// <summary>
        /// 프로시져 또는 쿼리문에 대한 실행결과에 대한 처리된 건수를 제공합니다.(트랜잭션 처리)
        /// </summary>
        /// <param name="commandText">Sql Command or Stored Procedure Name</param>
        /// <param name="paramCollection">Parameter Collection to be associated with the comman</param>
        /// <param name="transaction">Current Database Transaction (Use Helper.Transaction to get transaction)</param>
        /// <param name="commandType">Type of command (i.e. Sql Command/ Stored Procedure name/ Table Direct)</param>
        /// <returns>Number of rows affected.</returns>
        public int ExecuteNonQuery(string commandText, DatabaseParameterCollection paramCollection, IDbTransaction transaction, CommandType commandType)
        {
            int rowsAffected = 0;
            IDbConnection connection = transaction != null ? transaction.Connection : _connectionManager.GetConnection();
            IDbCommand command = _commandBuilder.GetCommand(commandText, connection, paramCollection, commandType);
            command.Transaction = transaction;

            try
            {
                rowsAffected = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (transaction == null)
                {
                    if (connection != null)
                    {
                        connection.Close();
                        connection.Dispose();
                    }
                }
                if (command != null)
                    command.Dispose();
            }
            return rowsAffected;
        }

        /// <summary>
        /// 쿼리문에 대한 실행결과에 대한 처리된 건수를 제공합니다.
        /// </summary>
        /// <param name="commandText">Sql Command</param>
        /// <returns>Number of rows effected.</returns>
        public int ExecuteNonQuery(string commandText)
        {
            return ExecuteNonQuery(commandText, (IDbTransaction)null);
        }

        /// <summary>
        /// 쿼리문에 대한 실행결과에 대한 처리된 건수를 제공합니다.(트랜잭션 처리)
        /// </summary>
        /// <param name="commandText">Sql Command</param>
        /// <param name="transaction">Current Database Transaction (Use Helper.Transaction to get transaction)</param>
        /// <returns>Number of rows affected.</returns>
        public int ExecuteNonQuery(string commandText, IDbTransaction transaction)
        {
            return ExecuteNonQuery(commandText, transaction, CommandType.Text);
        }

        /// <summary>
        /// 쿼리문에 대한 실행결과에 대한 처리된 건수를 제공합니다.
        /// </summary>
        /// <param name="commandText">Sql Command</param>
        /// <param name="param">Parameter to be associated with the command</param>
        /// <returns>Number of rows effected.</returns>
        public int ExecuteNonQuery(string commandText, DatabaseParameter param)
        {
            return ExecuteNonQuery(commandText, param, (IDbTransaction)null);
        }

        /// <summary>
        /// 프로시져 또는 쿼리문에 대한 실행결과에 대한 처리된 건수를 제공합니다.(트랜잭션 처리)
        /// </summary>
        /// <param name="commandText">Sql Command</param>
        /// <param name="param">Parameter to be associated with the command</param>
        /// <param name="transaction">Current Database Transaction (Use Helper.Transaction to get transaction)</param>
        /// <returns>Number of rows affected.</returns>
        public int ExecuteNonQuery(string commandText, DatabaseParameter param, IDbTransaction transaction)
        {
            return ExecuteNonQuery(commandText, param, transaction, CommandType.Text);
        }

        /// <summary>
        /// 쿼리문에 대한 실행결과에 대한 처리된 건수를 제공합니다.
        /// </summary>
        /// <param name="commandText">Sql Command</param>
        /// <param name="paramCollection">Parameter Collection to be associated with the command</param>
        /// <returns>Number of rows effected.</returns>
        public int ExecuteNonQuery(string commandText, DatabaseParameterCollection paramCollection)
        {
            return ExecuteNonQuery(commandText, paramCollection, (IDbTransaction)null);
        }

        /// <summary>
        /// 쿼리문에 대한 실행결과에 대한 처리된 건수를 제공합니다.(트랜잭션 처리)
        /// </summary>
        /// <param name="commandText">Sql Command</param>
        /// <param name="paramCollection">Parameter Collection to be associated with the command</param>
        /// <param name="transaction">Current Database Transaction (Use Helper.Transaction to get transaction)</param>
        /// <returns>Number of rows affected.</returns>
        public int ExecuteNonQuery(string commandText, DatabaseParameterCollection paramCollection, IDbTransaction transaction)
        {
            return ExecuteNonQuery(commandText, paramCollection, transaction, CommandType.Text);
        }

        #endregion ExecuteNonQuery

        #region GetDataSet

        /// <summary>
        /// 프로시져 또는 쿼리문에 대한 실행결과를 DataSet 형태로 제공합니다.
        /// </summary>
        /// <param name="commandText">Sql Command or Stored Procedure name</param>
        /// <param name="param">Parameter to be associated with the command</param>
        /// <param name="commandType">Type of command (i.e. Sql Command/ Stored Procedure name/ Table Direct)</param>
        /// <returns>Result in the form of DataSet</returns>
        public DataSet ExecuteDataSet(string commandText, DatabaseParameter param, CommandType commandType)
        {
            DatabaseParameterCollection paramCollection = new DatabaseParameterCollection();
            paramCollection.Add(param);
            return ExecuteDataSet(commandText, paramCollection, commandType);
        }

        /// <summary>
        /// 프로시져 또는 쿼리문에 대한 실행결과를 DataSet 형태로 제공합니다.
        /// </summary>
        /// <param name="commandText">Sql Command or Stored Procedure name</param>
        /// <param name="paramCollection">Parameter collection to be associated with the command</param>
        /// <param name="commandType">Type of command (i.e. Sql Command/ Stored Procedure name/ Table Direct)</param>
        /// <returns>Result in the form of DataSet</returns>
        public DataSet ExecuteDataSet(string commandText, DatabaseParameterCollection paramCollection, CommandType commandType)
        {
            DataSet dataSet = new DataSet();
            IDbConnection connection = _connectionManager.GetConnection();
            IDataAdapter adapter = _dbAdapterManager.GetDataAdapter(commandText, connection, paramCollection, commandType);

            try
            {
                adapter.Fill(dataSet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                    connection.Dispose();
                }
            }
            return dataSet;
        }

        /// <summary>
        /// 프로시져 또는 쿼리문에 대한 실행결과를 DataSet 형태로 제공합니다.
        /// </summary>
        /// <param name="commandText">Sql Command or Stored Procedure name</param>
        /// <param name="commandType">Type of command (i.e. Sql Command/ Stored Procedure name/ Table Direct)</param>
        /// <returns>Result in the form of DataSet</returns>
        public DataSet ExecuteDataSet(string commandText, CommandType commandType)
        {
            return ExecuteDataSet(commandText, new DatabaseParameterCollection(), commandType);
        }

        /// <summary>
        /// 쿼리문에 대한 실행결과를 DataSet 형태로 제공합니다.
        /// </summary>
        /// <param name="commandText">Sql Command </param>
        /// <returns>Result in the form of DataSet</returns>
        public DataSet ExecuteDataSet(string commandText)
        {
            return ExecuteDataSet(commandText, new DatabaseParameterCollection(), CommandType.Text);
        }

        /// <summary>
        /// 쿼리문에 대한 실행결과를 DataSet 형태로 제공합니다.
        /// </summary>
        /// <param name="commandText">Sql Command </param>
        /// <param name="param">Parameter to be associated with the command</param>
        /// <returns>Result in the form of DataSet</returns>
        public DataSet ExecuteDataSet(string commandText, DatabaseParameter param)
        {
            DatabaseParameterCollection paramCollection = new DatabaseParameterCollection();
            paramCollection.Add(param);
            return ExecuteDataSet(commandText, paramCollection);
        }

        /// <summary>
        /// 쿼리문에 대한 실행결과를 DataSet 형태로 제공합니다.
        /// </summary>
        /// <param name="commandText">Sql Command </param>
        /// <param name="paramCollection">Parameter collection to be associated with the command</param>
        /// <returns>Result in the form of DataSet</returns>
        public DataSet ExecuteDataSet(string commandText, DatabaseParameterCollection paramCollection)
        {
            return ExecuteDataSet(commandText, paramCollection, CommandType.Text);
        }

        #endregion GetDataSet

        #region ExecuteDataTable

        /// <summary>
        /// 쿼리문에 대한 실행결과를 데이타테이블 형태로 제공합니다.
        /// </summary>
        /// <param name="commandText">Sql Command or Stored Procedure name</param>
        /// <param name="tableName">Table name</param>
        /// <param name="paramCollection">Parameter collection to be associated with the Command or Stored Procedure.</param>
        /// <param name="commandType">Type of command (i.e. Sql Command/ Stored Procedure name/ Table Direct)</param>
        /// <returns>Result in the form of DataTable</returns>
        public DataTable ExecuteDataTable(string commandText, string tableName, DatabaseParameterCollection paramCollection, CommandType commandType)
        {
            DataTable dtReturn = new DataTable();
            IDbConnection connection = null;
            try
            {
                connection = _connectionManager.GetConnection();
                dtReturn = _dbAdapterManager.GetDataTable(commandText, paramCollection, connection, tableName, commandType);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                    connection.Dispose();
                }
            }
            return dtReturn;
        }

        /// <summary>
        /// 쿼리문에 대한 실행결과를 데이타테이블 형태로 제공합니다.
        /// </summary>
        /// <param name="commandText">Sql Command8 or Stored Procedure name</param>
        /// <param name="paramCollection">Parameter collection to be associated with the Command or Stored Procedure.</param>
        /// <param name="commandType">Type of command (i.e. Sql Command/ Stored Procedure name/ Table Direct)</param>
        /// <returns>Result in the form of DataTable</returns>
        public DataTable ExecuteDataTable(string commandText, DatabaseParameterCollection paramCollection, CommandType commandType)
        {
            return ExecuteDataTable(commandText, string.Empty, paramCollection, commandType);
        }

        /// <summary>
        /// 쿼리문에 대한 실행결과를 데이타테이블 형태로 제공합니다.
        /// </summary>
        /// <param name="commandText">Sql Command</param>
        /// <param name="tableName">Table name</param>
        /// <param name="paramCollection">Parameter collection to be associated with the Command.</param>
        /// <returns>Result in the form of DataTable</returns>
        public DataTable ExecuteDataTable(string commandText, string tableName, DatabaseParameterCollection paramCollection)
        {
            return ExecuteDataTable(commandText, tableName, paramCollection, CommandType.Text);
        }

        /// <summary>
        /// 쿼리문에 대한 실행결과를 데이타테이블 형태로 제공합니다.
        /// </summary>
        /// <param name="commandText">Sql Command</param>
        /// <param name="paramCollection">Parameter collection to be associated with the Command.</param>
        /// <returns>Result in the form of DataTable</returns>
        public DataTable ExecuteDataTable(string commandText, DatabaseParameterCollection paramCollection)
        {
            return ExecuteDataTable(commandText, string.Empty, paramCollection, CommandType.Text);
        }

        /// <summary>
        /// 쿼리문에 대한 실행결과를 데이타테이블 형태로 제공합니다.
        /// </summary>
        /// <param name="commandText">Sql Command or Stored Procedure Name</param>
        /// <param name="tableName">Table name</param>
        /// <param name="param">Parameter to be associated with the Command.</param>
        /// <param name="commandType">Type of command (i.e. Sql Command/ Stored Procedure name/ Table Direct)</param>
        /// <returns>Result in the form of DataTable</returns>
        public DataTable ExecuteDataTable(string commandText, string tableName, DatabaseParameter param, CommandType commandType)
        {
            DatabaseParameterCollection paramCollection = new DatabaseParameterCollection();
            paramCollection.Add(param);
            return ExecuteDataTable(commandText, tableName, paramCollection, commandType);
        }

        /// <summary>
        /// 쿼리문에 대한 실행결과를 데이타테이블 형태로 제공합니다.
        /// </summary>
        /// <param name="commandText">Sql Command or Stored Procedure Name</param>
        /// <param name="param">Parameter to be associated with the Command.</param>
        /// <param name="commandType">Type of command (i.e. Sql Command/ Stored Procedure name/ Table Direct)</param>
        /// <returns>Result in the form of DataTable</returns>
        public DataTable ExecuteDataTable(string commandText, DatabaseParameter param, CommandType commandType)
        {
            return ExecuteDataTable(commandText, string.Empty, param, commandType);
        }

        /// <summary>
        /// 쿼리문에 대한 실행결과를 데이타테이블 형태로 제공합니다.
        /// </summary>
        /// <param name="commandText">Sql Command</param>
        /// <param name="tableName">Table name</param>
        /// <param name="param">Parameter to be associated with the Command.</param>
        /// <returns>Result in the form of DataTable</returns>
        public DataTable ExecuteDataTable(string commandText, string tableName, DatabaseParameter param)
        {
            return ExecuteDataTable(commandText, tableName, param, CommandType.Text);
        }

        /// <summary>
        /// 쿼리문에 대한 실행결과를 데이타테이블 형태로 제공합니다.
        /// </summary>
        /// <param name="commandText">Sql Command</param>
        /// <param name="param">Parameter to be associated with the Command.</param>
        /// <returns>Result in the form of DataTable</returns>
        public DataTable ExecuteDataTable(string commandText, DatabaseParameter param)
        {
            return ExecuteDataTable(commandText, string.Empty, param, CommandType.Text);
        }

        /// <summary>
        /// 쿼리문에 대한 실행결과를 데이타테이블 형태로 제공합니다.
        /// </summary>
        /// <param name="commandText">Sql Command or Stored Procedure Name</param>
        /// <param name="tableName">Table name</param>
        /// <param name="commandType">Type of command (i.e. Sql Command/ Stored Procedure name/ Table Direct)</param>
        /// <returns>Result in the form of DataTable</returns>
        public DataTable ExecuteDataTable(string commandText, string tableName, CommandType commandType)
        {
            return ExecuteDataTable(commandText, tableName, new DatabaseParameterCollection(), commandType);
        }

        /// <summary>
        /// 쿼리문에 대한 실행결과를 데이타테이블 형태로 제공합니다.
        /// </summary>
        /// <param name="commandText">Sql Command or Stored Procedure Name</param>
        /// <param name="commandType">Type of command (i.e. Sql Command/ Stored Procedure name/ Table Direct)</param>
        /// <returns>Result in the form of DataTable</returns>
        public DataTable ExecuteDataTable(string commandText, CommandType commandType)
        {
            return ExecuteDataTable(commandText, string.Empty, commandType);
        }

        /// <summary>
        /// 쿼리문에 대한 실행결과를 데이타테이블 형태로 제공합니다.
        /// </summary>
        /// <param name="commandText">Sql Command</param>
        /// <param name="tableName">Table name</param>
        /// <returns>Result in the form of DataTable</returns>
        public DataTable ExecuteDataTable(string commandText, string tableName)
        {
            return ExecuteDataTable(commandText, tableName, CommandType.Text);
        }

        /// <summary>
        /// 쿼리문에 대한 실행결과를 데이타테이블 형태로 제공합니다.
        /// </summary>
        /// <param name="commandText">Sql Command</param>
        /// <returns></returns>
        public DataTable ExecuteDataTable(string commandText)
        {
            return ExecuteDataTable(commandText, string.Empty, CommandType.Text);
        }

        #endregion ExecuteDataTable

        #region ExcuteDataReader

        /// <summary>
        /// 프로시져 또는 쿼리문에 대한 실행결과를 DataReader 형태로 제공합니다.
        /// </summary>
        /// <param name="commandText">Sql Command or Stored Procedure Name</param>
        /// <param name="con">Database Connection object (DatabaseHelper.GetConnection() may be used)</param>
        /// <param name="commandType">Type of command (i.e. Sql Command/ Stored Procedure name/ Table Direct)</param>
        /// <returns>DataReader</returns>
        public IDataReader ExecuteDataReader(string commandText, IDbConnection connection, CommandType commandType)
        {
            return ExecuteDataReader(commandText, connection, new DatabaseParameterCollection(), commandType);
        }

        /// <summary>
        /// 쿼리문에 대한 실행결과를 DataReader 형태로 제공합니다.
        /// </summary>
        /// <param name="commandText">Sql Command</param>
        /// <param name="con">Database Connection object (DatabaseHelper.GetConnection() may be used)</param>
        /// <returns>DataReader</returns>
        public IDataReader ExecuteDataReader(string commandText, IDbConnection connection)
        {
            return ExecuteDataReader(commandText, connection, CommandType.Text);
        }

        /// <summary>
        /// 프로시져 또는 쿼리문에 대한 실행결과를 DataReader 형태로 제공합니다.
        /// </summary>
        /// <param name="commandText">Sql Command or Stored Procedure Name</param>
        /// <param name="con">Database Connection object (DatabaseHelper.GetConnection() may be used)</param>
        /// <param name="param">Parameter to be associated with the Sql Command or Stored Procedure.</param>
        /// <param name="commandType">Type of command (i.e. Sql Command/ Stored Procedure name/ Table Direct)</param>
        /// <returns>DataReader</returns>
        public IDataReader ExecuteDataReader(string commandText, IDbConnection connection, DatabaseParameter param, CommandType commandType)
        {
            DatabaseParameterCollection paramCollection = new DatabaseParameterCollection();
            paramCollection.Add(param);
            return ExecuteDataReader(commandText, connection, paramCollection, commandType);
        }

        /// <summary>
        /// 쿼리문에 대한 실행결과를 DataReader 형태로 제공합니다.
        /// </summary>
        /// <param name="commandText">Sql Command</param>
        /// <param name="con">Database Connection object (DatabaseHelper.GetConnection() may be used)</param>
        /// <param name="param">Parameter to be associated with the Sql Command.</param>
        /// <returns>DataReader</returns>
        public IDataReader ExecuteDataReader(string commandText, IDbConnection connection, DatabaseParameter param)
        {
            return ExecuteDataReader(commandText, connection, param, CommandType.Text);
        }

        /// <summary>
        /// 쿼리문에 대한 실행결과를 DataReader 형태로 제공합니다.
        /// </summary>
        /// <param name="commandText">Sql Command</param>
        /// <param name="con">Database Connection object (DatabaseHelper.GetConnection() may be used)</param>
        /// <param name="paramCollection">Parameter to be associated with the Sql Command or Stored Procedure.</param>
        /// <returns>DataReader</returns>
        public IDataReader ExecuteDataReader(string commandText, IDbConnection connection, DatabaseParameterCollection paramCollection)
        {
            return ExecuteDataReader(commandText, connection, paramCollection, CommandType.Text);
        }

        /// <summary>
        /// 프로시져 또는 쿼리문에 대한 실행결과를 DataReader 형태로 제공합니다.
        /// </summary>
        /// <param name="commandText">Sql Command or Stored Procedure Name</param>
        /// <param name="con">Database Connection object (DatabaseHelper.GetConnection() may be used)</param>
        /// <param name="paramCollection">Parameter to be associated with the Sql Command or Stored Procedure.</param>
        /// <param name="commandType">Type of command (i.e. Sql Command/ Stored Procedure name/ Table Direct)</param>
        /// <returns>DataReader</returns>
        public IDataReader ExecuteDataReader(string commandText, IDbConnection connection, DatabaseParameterCollection paramCollection, CommandType commandType)
        {
            IDataReader dataReader = null;
            IDbCommand command = _commandBuilder.GetCommand(commandText, connection, paramCollection, commandType);
            dataReader = command.ExecuteReader();
            command.Dispose();
            return dataReader;
        }

        /// <summary>
        /// 쿼리문에 대한 실행결과를 DataReader 형태로 제공합니다.(트랜잭션 지원)
        /// </summary>
        /// <param name="commandText">Sql Command</param>
        /// <param name="param">Database Parameter</param>
        /// <param name="transaction">Database Transaction (Use DatabaseHelper.Transaction property for getting the transaction.)</param>
        /// <returns>Data Reader</returns>
        public IDataReader ExecuteDataReader(string commandText, DatabaseParameter param, IDbTransaction transaction)
        {
            return ExecuteDataReader(commandText, param, transaction, CommandType.Text);
        }

        /// <summary>
        /// 프로시져 또는 쿼리문에 대한 실행결과를 DataReader 형태로 제공합니다.(트랜잭션 지원)
        /// </summary>
        /// <param name="commandText">Sql Command or Stored Proc Name</param>
        /// <param name="param">Database Parameter</param>
        /// <param name="transaction">Database Transaction (Use DatabaseHelper.Transaction property for getting the transaction.)</param>
        /// <param name="commandType">Text/ Stored Procedure</param>
        /// <returns>IDataReader</returns>
        public IDataReader ExecuteDataReader(string commandText, DatabaseParameter param, IDbTransaction transaction, CommandType commandType)
        {
            DatabaseParameterCollection paramCollection = new DatabaseParameterCollection();
            paramCollection.Add(param);
            return ExecuteDataReader(commandText, paramCollection, transaction, commandType);
        }

        /// <summary>
        /// 쿼리문에 대한 실행결과를 DataReader 형태로 제공합니다.(트랜잭션 지원)
        /// </summary>
        /// <param name="commandText">Sql Command </param>
        /// <param name="paramCollection">Database Parameter Collection</param>
        /// <param name="transaction">Database Transaction (Use DatabaseHelper.Transaction property for getting the transaction.)</param>
        /// <returns>IDataReader</returns>
        public IDataReader ExecuteDataReader(string commandText, DatabaseParameterCollection paramCollection, IDbTransaction transaction)
        {
            return ExecuteDataReader(commandText, paramCollection, transaction, CommandType.Text);
        }

        /// <summary>
        /// 프로시져 또는 쿼리문에 대한 실행결과를 DataReader 형태로 제공합니다.(트랜잭션 지원)
        /// </summary>
        /// <param name="commandText">Sql Command or Stored Proc name</param>
        /// <param name="paramCollection">Database Parameter Collection</param>
        /// <param name="transaction">Database Transaction (Use DatabaseHelper.Transaction property for getting the transaction.)</param>
        /// <param name="commandType">Text/ Stored Procedure</param>
        /// <returns>IDataReader</returns>
        public IDataReader ExecuteDataReader(string commandText, DatabaseParameterCollection paramCollection, IDbTransaction transaction, CommandType commandType)
        {
            IDataReader dataReader = null;
            IDbConnection connection = transaction.Connection;
            IDbCommand command = _commandBuilder.GetCommand(commandText, connection, paramCollection, commandType);
            command.Transaction = transaction;
            dataReader = command.ExecuteReader();
            command.Dispose();
            return dataReader;
        }

        #endregion ExcuteDataReader

        #region Methods to Prepare the Commands

        /// <summary>
        /// 쿼리문에 대한 실행결과를  Command 형태로 제공합니다.
        /// Use DisposeCommand method after use of the command.
        /// </summary>
        /// <param name="commandText">SQL Command or Stored Procedure name</param>
        /// <param name="commandType">Type of Command i.e. Text or Stored Procedure</param>
        /// <returns>Command ready for execute</returns>
        public IDbCommand GetCommand(string commandText, CommandType commandType)
        {
            IDbConnection connection = _connectionManager.GetConnection();
            IDbCommand command = _commandBuilder.GetCommand(commandText, connection, commandType);
            return command;
        }

        /// <summary>
        /// 프로시져 또는 쿼리문에 대한 실행결과를  Command 형태로 제공합니다.(트랜잭션 처리)
        /// </summary>
        /// <param name="commandText">SQL Command or Stored Procedure name</param>
        /// <param name="commandType">Type of Command i.e. Text or Stored Procedure</param>
        /// <returns>Command ready for execute</returns>
        public IDbCommand GetCommand(string commandText, IDbTransaction transaction, CommandType commandType)
        {
            IDbConnection connection = transaction != null ? transaction.Connection : _connectionManager.GetConnection();
            IDbCommand command = _commandBuilder.GetCommand(commandText, connection, commandType);
            return command;
        }

        /// <summary>
        /// 쿼리문에 대한 실행결과를  Command 형태로 제공합니다.
        /// </summary>
        /// <param name="commandText">SQL Command</param>
        /// <returns>Command ready for execute</returns>
        public IDbCommand GetCommand(string commandText)
        {
            return GetCommand(commandText, CommandType.Text);
        }

        /// <summary>
        /// 쿼리문에 대한 실행결과를  Command 형태로 제공합니다.(트랜잭션 처리)
        /// </summary>
        /// <param name="commandText"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public IDbCommand GetCommand(string commandText, IDbTransaction transaction)
        {
            return GetCommand(commandText, transaction, CommandType.Text);
        }

        /// <summary>
        /// 프로시져 또는 쿼리문에 대한 실행결과를  Command 형태로 제공합니다.
        /// </summary>
        /// <param name="commandText">SQL Command or Stored Procedure name</param>
        /// <param name="parameter">Database parameter</param>
        /// <param name="commandType">Type of Command i.e. Text or Stored Procedure</param>
        /// <returns>Command ready for execute</returns>
        public IDbCommand GetCommand(string commandText, DatabaseParameter parameter, CommandType commandType)
        {
            IDbConnection connection = _connectionManager.GetConnection();
            IDbCommand command = _commandBuilder.GetCommand(commandText, connection, parameter, commandType);
            return command;
        }

        /// <summary>
        /// 쿼리문에 대한 실행결과를  Command 형태로 제공합니다.
        /// <summary>
        /// <param name="commandText">SQL Command</param>
        /// <param name="parameter">Database parameter</param>
        /// <returns>Command ready for execute</returns>
        public IDbCommand GetCommand(string commandText, DatabaseParameter parameter)
        {
            return GetCommand(commandText, parameter, CommandType.Text);
        }

        /// <summary>
        /// 쿼리문에 대한 실행결과를  Command 형태로 제공합니다.(트랜잭션 처리)
        /// </summary>
        /// <param name="commandText"></param>
        /// <param name="parameter"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public IDbCommand GetCommand(string commandText, DatabaseParameter parameter, IDbTransaction transaction)
        {
            DatabaseParameterCollection paramCollection = new DatabaseParameterCollection();
            paramCollection.Add(parameter);
            return GetCommand(commandText, paramCollection, transaction, CommandType.Text);
        }

        /// <summary>
        /// 프로시져 또는 쿼리문에 대한 실행결과를  Command 형태로 제공합니다.
        /// </summary>
        /// <param name="commandText">SQL Command or Stored Procedure name</param>
        /// <param name="parameterCollection">Database parameter collection</param>
        /// <param name="commandType">Type of Command i.e. Text or Stored Procedure</param>
        /// <returns>Command ready for execute</returns>
        public IDbCommand GetCommand(string commandText, DatabaseParameterCollection parameterCollection, CommandType commandType)
        {
            IDbConnection connection = _connectionManager.GetConnection();
            IDbCommand command = _commandBuilder.GetCommand(commandText, connection, parameterCollection, commandType);
            return command;
        }

        /// <summary>
        /// 프로시져 또는 쿼리문에 대한 실행결과를  Command 형태로 제공합니다.(트랜잭션 처리)
        /// </summary>
        /// <param name="commandText"></param>
        /// <param name="parameterCollection"></param>
        /// <param name="transaction"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        public IDbCommand GetCommand(string commandText, DatabaseParameterCollection parameterCollection, IDbTransaction transaction, CommandType commandType)
        {
            IDbConnection connection = transaction != null ? transaction.Connection : _connectionManager.GetConnection();
            IDbCommand command = _commandBuilder.GetCommand(commandText, connection, parameterCollection, commandType);
            return command;
        }

        /// <summary>
        /// 쿼리문에 대한 실행결과를  Command 형태로 제공합니다.
        /// </summary>
        /// <param name="commandText">SQL Command</param>
        /// <param name="parameterCollection">Database parameter collection</param>
        /// <returns>Command ready for execute</returns>
        public IDbCommand GetCommand(string commandText, DatabaseParameterCollection parameterCollection)
        {
            return GetCommand(commandText, parameterCollection, CommandType.Text);
        }

        /// <summary>
        /// 프로시져 또는 쿼리문에 대한 실행결과를  Command 형태로 제공합니다.(트랜잭션 처리)
        /// </summary>
        /// <param name="commandText"></param>
        /// <param name="parameterCollection"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public IDbCommand GetCommand(string commandText, DatabaseParameterCollection parameterCollection, IDbTransaction transaction)
        {
            return GetCommand(commandText, parameterCollection, transaction, CommandType.Text);
        }

        #endregion Methods to Prepare the Commands

        #region Methods To Retrieve the Parameter Values

        /// <summary>
        /// 데이타베이스 Command로부터 지정된 파라메터이름에 대한 파라메터 객체를 제공합니다.
        /// </summary>
        /// <param name="parameterName">Name of the parameter</param>
        /// <param name="command">Command from which value to be retrieved</param>
        /// <returns>Parameter value</returns>
        public object GetParameterValue(string parameterName, IDbCommand command)
        {
            object retValue = null;
            IDataParameter param = (IDataParameter)command.Parameters[parameterName];
            retValue = param.Value;
            return retValue;
        }

        /// <summary>
        /// 데이타베이스 Command로부터 지정된 Index에 대한 파라메터 객체를 제공합니다.
        /// </summary>
        /// <param name="index">Index of the parameter</param>
        /// <param name="command">Command from which value to be retrieved</param>
        /// <returns>Parameter value</returns>
        public object GetParameterValue(int index, IDbCommand command)
        {
            object retValue = null;
            IDataParameter param = (IDataParameter)command.Parameters[index];
            retValue = param.Value;
            return retValue;
        }

        #endregion Methods To Retrieve the Parameter Values

        #region Methods to Dispose the Command

        /// <summary>
        /// 데이타베이스의 Command객체와 연결을 모두 닫는 기능을 제공합니다.
        /// </summary>
        /// <param name="command">Command which needs to be closed</param>
        public void DisposeCommand(IDbCommand command)
        {
            if (command == null)
                return;

            if (command.Connection != null)
            {
                command.Connection.Close();
                command.Connection.Dispose();
            }

            command.Dispose();
        }

        #endregion Methods to Dispose the Command
    }
}