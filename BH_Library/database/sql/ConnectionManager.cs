﻿using System;
using System.Data;

namespace BH_Library.Database.Sql
{
    /// <summary>
    /// 데이타베이스 연결을 관리합니다.
    /// </summary>
    public class ConnectionManager
    {
        private string _connectionName = string.Empty;
        private string _connectionString = string.Empty;
        private string _providerName = string.Empty;
        private AssemblyProvider _assemblyProvider = null;

        /// <summary>
        /// 데이타베이스 연결관리를 위한 기본 생성자를 제공합니다.
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="providerName"></param>
        public ConnectionManager(string connectionString, string providerName)
        {
            _connectionString = connectionString;
            _providerName = providerName;
            _connectionName = string.Empty;
            _assemblyProvider = new AssemblyProvider(_providerName);
        }

        /// <summary>
        /// 데이타베이스 연결문자열을 제공합니다.
        /// </summary>
        internal string ConnectionString
        {
            get
            {
                return _connectionString;
            }
        }

        /// <summary>
        /// 데이타베이스 프로바이더 이름을 제공합니다.
        /// </summary>
        internal string ProviderName
        {
            get
            {
                return _providerName;
            }
        }

        /// <summary>
        /// 데이타베이스 연결을 지원합니다.
        /// </summary>
        /// <returns></returns>
        internal IDbConnection GetConnection()
        {
            IDbConnection connection = _assemblyProvider.Factory.CreateConnection();
            connection.ConnectionString = _connectionString;

            try
            {
                connection.Open();
            }
            catch (Exception err)
            {
                throw err;
            }

            return connection;
        }
    }
}