﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace BH_Library.Database.MSSQL
{
    /// <summary>
    /// MSSQL의 데이타베이스 연결을 제공합니다.
    /// </summary>
    public class BasicConnector
    {
        private static BasicConnector m_DbConnector = null;
        private SqlConnection m_Conn = null;
        private string m_DbPath = string.Empty;
        private string m_DbName = string.Empty;
        private string m_UserId = string.Empty;
        private string m_UserPw = string.Empty;
        private string m_ConnectionString = string.Empty;

        /// <summary>
        /// 데이터베이스 연결을 위한 인스턴스 생성기능을 제공합니다.
        /// </summary>
        /// <returns></returns>
        public static BasicConnector GetInstance()
        {
            if (m_DbConnector == null)
            {
                m_DbConnector = new BasicConnector();
            }

            return m_DbConnector;
        }

        /// <summary>
        /// 데이타베이스 연결을 제공합니다.
        /// </summary>
        /// <param name="DbPath"></param>
        /// <param name="DbName"></param>
        /// <param name="UserId"></param>
        /// <param name="UserPw"></param>
        public void Open(string DbPath, string DbName, string UserId, string UserPw)
        {
            this.m_DbPath = DbPath;
            this.m_DbName = DbName;
            this.m_UserId = UserId;
            this.m_UserPw = UserPw;

            if (m_DbPath == "" || m_DbName == "" || m_UserId == "" || m_UserPw == "") throw new ApplicationException("데이터베이스 연결정보를 찾을수 없습니다.");

            m_ConnectionString = String.Format("Server={0};database={1};uid={2};pwd={3};", this.m_DbPath, this.m_DbName, this.m_UserId, this.m_UserPw);

            Open(m_ConnectionString);
        }

        /// <summary>
        /// 데이타베이스 연결을 제공합니다.
        /// </summary>
        /// <param name="connectionString"></param>
        public void Open(string connectionString)
        {
            this.m_ConnectionString = connectionString;

            if (m_ConnectionString == "") throw new ApplicationException("데이터베이스 연결정보를 찾을수 없습니다.");

            if (m_Conn != null)
            {
                m_Conn.Close();
            }

            m_Conn = new SqlConnection(connectionString);
        }

        /// <summary>
        /// 데이타베이스 연결을 종료하는 기능을 제공합니다.
        /// </summary>
        public void Close()
        {
            if (m_Conn != null)
            {
                m_Conn.Close();
                m_Conn.Dispose();
                m_Conn = null;
            }
        }

        /// <summary>
        /// 반환값이 없는 쿼리 실행 기능을 제공합니다.
        /// </summary>
        /// <param name="Query"></param>
        public void ExecuteQuery(string Query)
        {
            SqlCommand cmd = null;
            try
            {
                cmd = new SqlCommand(Query, m_Conn);
                if (cmd.Connection.State != ConnectionState.Open) cmd.Connection.Open();
                cmd.CommandTimeout = 60 * 10;
                cmd.ExecuteNonQuery();
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            finally
            {
                cmd = null;
            }
        }

        /// <summary>
        /// 반환값이 없는 쿼리 실행 기능(BLOB)을 제공합니다.
        /// </summary>
        /// <param name="Query"></param>
        /// <param name="Param1"></param>
        /// <param name="ByteArray"></param>
        public void ExecuteQuery_BLOB(string Query, string Param1, byte[] ByteArray)
        {
            SqlCommand cmd = null;
            try
            {
                if (m_Conn == null) Open(m_ConnectionString);

                cmd = new SqlCommand(Query, m_Conn);
                if (cmd.Connection.State != ConnectionState.Open) cmd.Connection.Open();
                cmd.CommandTimeout = 60 * 10;
                cmd.Parameters.AddWithValue(Param1, ByteArray);
                cmd.ExecuteNonQuery();
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            finally
            {
                cmd = null;
            }
        }

        /// <summary>
        /// 쿼리 실행결과를 DataSet으로 제공합니다.
        /// </summary>
        /// <param name="Query"></param>
        /// <returns></returns>
        public DataSet DataSetAdapter(string Query)
        {
            SqlDataAdapter adapter = null;
            DataSet ds = null;
            try
            {
                if (m_Conn == null) Open(m_ConnectionString);

                adapter = new SqlDataAdapter(Query, m_Conn);
                adapter.SelectCommand.CommandTimeout = 60 * 10;
                ds = new DataSet();
                adapter.Fill(ds);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            finally
            {
                adapter = null;
            }
            return (ds);
        }

        /// <summary>
        /// 쿼리 실행결과를 DataTable으로 제공합니다.
        /// </summary>
        /// <param name="Query"></param>
        /// <returns></returns>
        public DataTable DataTableAdapter(string Query)
        {
            SqlDataAdapter adapter = null;
            DataTable dt = null;

            try
            {
                if (m_Conn == null) Open(m_ConnectionString);

                adapter = new SqlDataAdapter(Query, m_Conn);
                dt = new DataTable();
                adapter.Fill(dt);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            finally
            {
                adapter.Dispose();
                adapter = null;
            }
            return dt;
        }

        /// <summary>
        /// 쿼리 실행결과를 SqlDataReader로 제공합니다.
        /// </summary>
        /// <param name="Query"></param>
        /// <returns></returns>
        public SqlDataReader DataReader(string Query)
        {
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            try
            {
                if (m_Conn == null) Open(m_ConnectionString);

                cmd = new SqlCommand();
                cmd.Connection = m_Conn;
                cmd.CommandText = Query;
                reader = cmd.ExecuteReader();
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            finally
            {
                cmd = null;
                reader.Close();
                reader = null;
            }

            return reader;
        }
    }
}