﻿using Microsoft.Win32.TaskScheduler;
using System;

namespace BH_Library.Utils.TaskScheduler
{
    public class TaskSchedulerHelper
    {
        public static void CreateDailyService(string title, string desc, DateTime ExecuteTime, int duration, string executeAction = null, string argument = null)
        {
            TaskService ts = new TaskService();
            TaskDefinition td = ts.NewTask(); //정의 생성.
            td.RegistrationInfo.Description = desc;
            td.Principal.UserId = string.Concat(Environment.UserDomainName, "\\", Environment.UserName); //계정
            td.Principal.LogonType = TaskLogonType.InteractiveToken;
            td.Principal.RunLevel = TaskRunLevel.Highest;
            DailyTrigger dt = new DailyTrigger();
            dt.StartBoundary = ExecuteTime;
            dt.Repetition.Duration = TimeSpan.FromDays(duration);
            td.Triggers.Add(dt);
            td.Actions.Add(new ExecAction(executeAction, argument)); //프로그램, 인자등록.
            ts.RootFolder.RegisterTaskDefinition(title, td); //"CTK-ICT Agent_" + AgentNum란 이름으로 등록.
        }

        public static void DeleteServices(string serviceName)
        {
            try
            {
                TaskService ts = new TaskService();
                ts.RootFolder.DeleteTask(serviceName);
            }
            catch (Exception ex)
            {
#if DEBUG
                BhMsgBox.ShowError(ex.Message);
#else
                Console.WriteLine(ex.Message);
#endif
            }
        }

        #region ref

        //TaskService ts = new TaskService();
        //TaskDefinition td = ts.NewTask(); //정의 생성.
        //td.RegistrationInfo.Description = AgentTitle; //설명.

        //td.Principal.UserId = string.Concat(Environment.UserDomainName, "\\", Environment.UserName); //계정
        //td.Principal.LogonType = TaskLogonType.InteractiveToken;
        //td.Principal.RunLevel = TaskRunLevel.Highest;

        //DailyTrigger dt = new DailyTrigger();
        //dt.StartBoundary = DateTime.Parse("2018-03-19 13:15:00");
        //dt.Repetition.Duration = TimeSpan.FromDays(1);
        //td.Triggers.Add(dt);

        //TimeTrigger tt = new TimeTrigger(new DateTime(2018, 03, 19, 12, 30, 00));
        //tt.Repetition.Interval = TimeSpan.FromDays(1);
        //td.Triggers.Add(tt);

        ///*DailyTrigger dt = new DailyTrigger(); //날짜별로 실행.
        //dt.StartBoundary = DateTime.Today + TimeSpan.FromSeconds(10);
        //dt.DaysInterval = 2;
        //td.Triggers.Add(dt);*/

        //LogonTrigger lt = new LogonTrigger(); //로그인할때 실행
        //td.Triggers.Add(lt);

        //BootTrigger bt = new BootTrigger(); //부팅으로 조건 설정.
        //bt.Delay = new TimeSpan(0, 3, 0); //부팅하고 3분 이후 실행.
        //td.Triggers.Add(bt);

        //td.Actions.Add(new ExecAction(string.Format(@"C:\CTK_ICT\{0}.exe", AgentNum), null)); //프로그램, 인자등록.
        //ts.RootFolder.RegisterTaskDefinition(AgentTitle, td); //"CTK-ICT Agent_" + AgentNum란 이름으로 등록.

        ////MyScheduler 작업 삭제.
        //ts.RootFolder.DeleteTask("MyScheduler");

        #endregion ref
    }
}