﻿//using Folder = Shell32.Folder;
using IWshRuntimeLibrary;
using System;
using System.IO;

//using Shell32;
using File = System.IO.File;

namespace BH_Library.Utils.FileEx
{
    public class ShortcutHelper
    {
        private string RootDirectory
        {
            get
            {
                string iconPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\BhSoftIcons";
                if (Directory.Exists(iconPath) == false) Directory.CreateDirectory(iconPath);
                return iconPath;
            }
        }

        public ShortcutHelper()
        {
        }

        /// <summary>
        /// 바로가기 만들기
        /// </summary>
        /// <param name="shortCutPath">바로가기가 위치할 폴더 경로</param>
        /// <param name="originalPath">파로가기 대상 프로그램의 전체 경로</param>
        /// <param name="file">바로가기 이름</param>
        /// <param name="description">바로가기의 설명</param>
        /// <param name="iconPath">바로가기 아이콘 전체 경로</param>
        public static void Make(string shortCutPath, string originalPath, string file, string description = null, string iconPath = null)
        {
            WshShellClass WshShell = new WshShellClass();
            IWshShortcut objShortcut;

            if (file.Contains(".")) file = file.Substring(0, file.LastIndexOf("."));

            // 바로가기를 저장할 경로를 지정한다.
            objShortcut = (IWshShortcut)WshShell.CreateShortcut(shortCutPath + "\\" + file + ".lnk");

            // 바로가기에 프로그램의 경로를 지정한다.
            objShortcut.TargetPath = originalPath;

            // 시작 위치를 지정한다.
            objShortcut.WorkingDirectory = Path.GetDirectoryName(originalPath);

            // 바로가기의 description을 지정한다.
            if (string.IsNullOrEmpty(description)) objShortcut.Description = description;

            // 바로가기 아이콘을 지정한다.
            if (iconPath != null && File.Exists(iconPath)) objShortcut.IconLocation = originalPath + "\\" + iconPath;

            // 바로가기를 저장한다.
            objShortcut.Save();
        }

        //public void MakeIcon(string description, string iconFile, bool firstTime, string argument = null, bool IsLuncherStart = false)
        //{
        //    if (File.Exists(Application.StartupPath + @"\" + iconFile) == false)
        //    {
        //        Console.WriteLine(Application.StartupPath + @"\" + iconFile + "파일이 없습니다.");
        //        return;
        //    }
        //    string folder = Application.StartupPath;
        //    string path;
        //    path = Environment.GetFolderPath(Environment.SpecialFolder.CommonDesktopDirectory);
        //    if (string.IsNullOrEmpty(argument))
        //        CreateShortcut(path, description, firstTime, iconFile);
        //    else
        //        CreateShortcut(path, description, firstTime, iconFile, argument);
        //}

        //private void CreateShortcut(string folderPath, string description, bool firstTime, string iconFile)
        //{
        //    bool existIcon = DeleteShortcuts(Application.StartupPath, folderPath, description, firstTime);
        //    if (firstTime == false && existIcon == true)
        //        return;
        //    try
        //    {
        //        WshShellClass wshShell = new WshShellClass();
        //        IWshShortcut shortcut;
        //        shortcut = (IWshShortcut)wshShell.CreateShortcut(folderPath + "\\" + description + ".lnk");
        //        shortcut.TargetPath = Application.ExecutablePath;
        //        shortcut.WorkingDirectory = Application.StartupPath;
        //        shortcut.Description = description;
        //        shortcut.IconLocation = Application.StartupPath + @"\" + iconFile;
        //        shortcut.Save();
        //    }
        //    catch (Exception ex)
        //    {
        //        Debug.WriteLine(ex.Message);
        //    }
        //}

        //private void CreateShortcut(string folderPath, string description, bool firstTime, string iconFile, string argument)
        //{
        //    bool existIcon = DeleteShortcuts(Application.StartupPath, folderPath, description, firstTime);
        //    if (firstTime == false && existIcon == true) // 동일 버젼 아이콘이 이미있는 경우는 리턴합니다. 2011.5.24
        //    {
        //        return;
        //    }
        //    try
        //    {
        //        WshShellClass wshShell = new WshShellClass();
        //        IWshShortcut shortcut;
        //        shortcut = (IWshShortcut)wshShell.CreateShortcut(folderPath + "\\" + description + ".lnk");
        //        shortcut.TargetPath = Application.ExecutablePath;
        //        shortcut.WorkingDirectory = Application.StartupPath;
        //        shortcut.Description = description;
        //        shortcut.Arguments = argument;
        //        shortcut.IconLocation = Application.StartupPath + @"\" + iconFile;
        //        shortcut.Save();
        //    }
        //    catch (Exception ex)
        //    {
        //        Debug.WriteLine(ex.Message);
        //    }
        //}

        //private void CreateForceShortcut(string folderPath, string description)
        //{
        //    try
        //    {
        //        WshShellClass wshShell = new WshShellClass();
        //        IWshShortcut shortcut;
        //        shortcut = (IWshShortcut)wshShell.CreateShortcut(folderPath + "\\" + description + ".lnk");
        //        shortcut.TargetPath = Application.ExecutablePath;
        //        shortcut.WorkingDirectory = Application.StartupPath;
        //        shortcut.Description = description;
        //        shortcut.IconLocation = Application.ExecutablePath; //  Application.StartupPath + @"\tong.ico";
        //        shortcut.Save();
        //    }
        //    catch (Exception ex)
        //    {
        //        Debug.WriteLine(ex.Message);
        //    }
        //}

        //private bool DeleteShortcuts(string targetPath, string folder, string description, bool forceDelete)
        //{
        //    bool existLink = false;
        //    try
        //    {
        //        DirectoryInfo di = new DirectoryInfo(folder);
        //        FileInfo[] files = di.GetFiles("*.lnk");

        //        foreach (FileInfo fi in files)
        //        {
        //            ShellLinkObject link = GetShortcutTargetFile(fi.FullName);
        //            if (link == null)
        //            {
        //                continue;
        //            }

        //            string dir = Path.GetDirectoryName(link.Path);
        //            if (dir == targetPath)
        //            {
        //                existLink = true;
        //                if (forceDelete)
        //                {
        //                    File.Delete(fi.FullName);
        //                }
        //                return true;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Debug.WriteLine(ex.Message);
        //    }
        //    return existLink;
        //}

        //private void DeleteForceShortcuts(string targetPath, string folder, string description)
        //{
        //    try
        //    {
        //        DirectoryInfo di = new DirectoryInfo(folder);
        //        FileInfo[] files = di.GetFiles("*.lnk");

        //        foreach (FileInfo fi in files)
        //        {
        //            ShellLinkObject link = GetShortcutTargetFile(fi.FullName);
        //            if (link == null)
        //            {
        //                continue;
        //            }
        //            string dir = Path.GetDirectoryName(link.Path);
        //            if (dir == targetPath)
        //            {
        //                File.Delete(fi.FullName);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Debug.WriteLine(ex.Message);
        //    }
        //}

        //private int GetShortcutsCount(string targetPath, string folder, string description)
        //{
        //    int existCount = 0;
        //    try
        //    {
        //        DirectoryInfo di = new DirectoryInfo(folder);
        //        FileInfo[] files = di.GetFiles("*");

        //        foreach (FileInfo fi in files)
        //        {
        //            ShellLinkObject link = GetShortcutTargetFile(fi.FullName);
        //            if (link == null)
        //            {
        //                continue;
        //            }

        //            string dir = Path.GetDirectoryName(link.Path);
        //            if (dir == targetPath)
        //            {
        //                existCount++;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Debug.WriteLine(ex.Message);
        //    }
        //    return existCount;
        //}

        //private ShellLinkObject GetShortcutTargetFile(string shortcutFilename)
        //{
        //    ShellLinkObject link = null;
        //    try
        //    {
        //        string pathOnly = Path.GetDirectoryName(shortcutFilename);
        //        string filenameOnly = Path.GetFileName(shortcutFilename);

        //        Shell shell = new ShellClass();
        //        Folder folder = shell.NameSpace(pathOnly);
        //        FolderItem folderItem = folder.ParseName(filenameOnly);
        //        if (folderItem != null)
        //        {
        //            link = (ShellLinkObject)folderItem.GetLink;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Debug.WriteLine(ex.Message);
        //    }
        //    return link;
        //}

        //public void MakeIcon(FileInfo OriginFile, string resultLinkPath, string LinkName, bool firstTime, string argument = null, bool IsLuncherStart = false)
        //{
        //    var extractor = new IconExtractor(OriginFile.FullName);
        //    Icon icon = extractor.GetIcon(0);
        //    Bitmap bmp = icon.ToBitmap();
        //    string iconName = string.Format(@"{0}\{1}.ico", RootDirectory, DateTime.Now.ToString("yyyyMMddhhmmssfff"));
        //    bmp.Save(iconName, System.Drawing.Imaging.ImageFormat.Icon);
        //    MakeIcon(OriginFile, resultLinkPath, LinkName, iconName, firstTime, argument, IsLuncherStart);
        //}

        //public void MakeIcon(FileInfo OriginFile, string resultLinkPath, string LinkName, string iconFile, bool firstTime, string argument = null, bool IsLuncherStart = false)
        //{
        //    if (File.Exists(iconFile) == false)
        //        throw new Exception(iconFile + "파일이 없습니다.");
        //    CreateShortcut(OriginFile, resultLinkPath, LinkName, firstTime, iconFile, argument);
        //}

        //private void CreateShortcut(FileInfo OriginFile, string resultLinkPath, string LinkName, bool firstTime, string iconFile, string argument)
        //{
        //    bool existIcon = DeleteShortcuts(Application.StartupPath, resultLinkPath, LinkName, firstTime);
        //    if (firstTime == false && existIcon == true) // 동일 버젼 아이콘이 이미있는 경우는 리턴합니다. 2011.5.24
        //        return;
        //    try
        //    {
        //        WshShellClass wshShell = new WshShellClass();
        //        IWshShortcut shortcut;
        //        shortcut = (IWshShortcut)wshShell.CreateShortcut(resultLinkPath + "\\" + LinkName + ".lnk");
        //        shortcut.TargetPath = OriginFile.FullName;
        //        shortcut.WorkingDirectory = OriginFile.DirectoryName;
        //        shortcut.Description = LinkName;
        //        if (argument != null)
        //            shortcut.Arguments = argument;
        //        shortcut.IconLocation = iconFile;
        //        shortcut.Save();
        //    }
        //    catch (Exception ex)
        //    {
        //        Debug.WriteLine(ex.Message);
        //    }
        //}
    }
}