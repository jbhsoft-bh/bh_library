﻿using System;
using System.Diagnostics;
using System.IO;

namespace BH_Library.Utils.FileEx
{
    /// <summary>
    /// 파일을 비교하는 기능을 제공합니다.
    /// </summary>
    public static class FileCompare
    {
        /// <summary>
        /// 첫번째 파일과 두번째 파일에 대한 버젼을 비교합니다.
        /// </summary>
        /// <param name="firstFileFullName"></param>
        /// <param name="secondFileFullName"></param>
        /// <returns>>결과값이 0 이면 동일한 파일버 동일함, 1이면 처음파일의 버젼이 높음, -1이면 두번째파일의 버젼이 높음</returns>
        public static int FileVersion(string firstFileFullName, string secondFileFullName)
        {
            try
            {
                if (File.Exists(firstFileFullName) == false || File.Exists(secondFileFullName) == false)
                {
                    return 1;
                }

                FileVersionInfo fv1 = FileVersionInfo.GetVersionInfo(firstFileFullName);
                FileVersionInfo fv2 = FileVersionInfo.GetVersionInfo(secondFileFullName);

                if (fv1.FileMajorPart > fv2.FileMajorPart)
                {
                    return 1;
                }
                if (fv1.FileMajorPart < fv2.FileMajorPart)
                {
                    return -1;
                }

                if (fv1.FileMinorPart > fv2.FileMinorPart)
                {
                    return 1;
                }
                if (fv1.FileMinorPart < fv2.FileMinorPart)
                {
                    return -1;
                }

                if (fv1.FileBuildPart > fv2.FileBuildPart)
                {
                    return 1;
                }
                if (fv1.FileBuildPart < fv2.FileBuildPart)
                {
                    return -1;
                }

                if (fv1.FilePrivatePart > fv2.FilePrivatePart)
                {
                    return 1;
                }
                if (fv1.FilePrivatePart < fv2.FilePrivatePart)
                {
                    return -1;
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            return 0;
        }
    }
}