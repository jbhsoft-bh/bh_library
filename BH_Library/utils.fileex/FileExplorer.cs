﻿using System;
using System.IO;

namespace BH_Library.Utils.FileEx
{
    /// <summary>
    /// 파일 브라우져 기능을 제공합니다.
    /// </summary>
    public class FileExplorer : MarshalByRefObject
    {
        /// <summary>
        /// 기본 생성자를 제공합니다.
        /// </summary>
        public FileExplorer() { }

        /// <summary>
        /// 디렉토리 목록을 제공합니다.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public FileExplorerItemInfo[] GetDirectories(string path)
        {
            DirectoryInfo dirInfo = new DirectoryInfo(path);
            DirectoryInfo[] dirList = dirInfo.GetDirectories();
            FileExplorerItemInfo[] items = new FileExplorerItemInfo[dirList.Length];
            for (int x = 0; x < dirList.Length; x++)
                items[x] = new FileExplorerItemInfo(dirList[x]);
            return items;
        }

        /// <summary>
        /// 파일목록을 제공합니다.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public FileExplorerItemInfo[] GetFiles(string path)
        {
            DirectoryInfo dirInfo = new DirectoryInfo(path);
            FileInfo[] files = dirInfo.GetFiles();
            FileExplorerItemInfo[] items = new FileExplorerItemInfo[files.Length];
            for (int x = 0; x < files.Length; x++)
                items[x] = new FileExplorerItemInfo(files[x]);
            return items;
        }
    }
}