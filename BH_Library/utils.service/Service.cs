﻿using System;
using System.Configuration.Install;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.ServiceProcess;

namespace BH_Library.Utils.Service
{
    public static class Service
    {
        //private static readonly string _exePath = Assembly.GetExecutingAssembly().Location;
        public static bool InstallMe(string ExcutePath)
        {
            try
            {
                ManagedInstallerClass.InstallHelper(
                    new string[] { ExcutePath });
            }
            catch
            {
                return false;
            }
            return true;
        }

        public static bool UninstallMe(string ExcutePath)
        {
            try
            {
                ManagedInstallerClass.InstallHelper(
                    new string[] { "/u", ExcutePath });
            }
            catch
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Print detailed information on a particular service.
        /// </summary>
        /// <param name="service">Service controller</param>
        public static void GetService(ServiceController service)
        {
            string serviceName = service.ServiceName;
            string displayName = service.DisplayName;
            ServiceControllerStatus status = service.Status;

            // print out detail service information
            Console.WriteLine(string.Format("Service Name                 : {0}", serviceName));
            Console.WriteLine(string.Format("Display Name                 : {0}", displayName));
            Console.WriteLine(string.Format("Service Status               : {0}", status.ToString()));
            Console.WriteLine(string.Format("Service Type                 : {0}", service.ServiceType.ToString()));
            Console.WriteLine(string.Format("Service Can Stop             : {0}", service.CanStop));
            Console.WriteLine(string.Format("Service Can Pause / Continue : {0}", service.CanPauseAndContinue));
            Console.WriteLine(string.Format("Service Can Shutdown         : {0}", service.CanShutdown));

            // print out dependent services
            ServiceController[] dependedServices = service.DependentServices;
            Console.Write(string.Format("{0} Depended Service(s)        : ", dependedServices.Length.ToString()));

            int pos = 0;
            foreach (ServiceController dService in dependedServices)
            {
                Console.Write(string.Format("{0}{1}",
                    ((dependedServices.Length > 1 && pos > 0) ? ", " : string.Empty), dService.ServiceName));

                pos++;
            }

            Console.WriteLine();
        }

        /// <summary>
        /// Get all available services on a computer.
        /// </summary>
        /// <param name="cName">Computer name.</param>
        public static ServiceController GetServices(string ServiceName)
        {
            ServiceController retService = null;
            ServiceController[] services = ServiceController.GetServices();
            foreach (ServiceController service in services)
            {
                if (service.ServiceName == ServiceName)
                {
                    retService = service;
                    break;
                }
            }

            return retService;
        }

        /// <summary>
        /// Restart a service.
        /// This action will in turn call StopService and StartService.
        /// If the service is not currently stopped, it will try to stop the service first.
        /// </summary>
        /// <param name="service">service controller.</param>
        /// <param name="timeout">timeout value used for stopping and restarting.</param>
        public static void RestartService(ServiceController service, int timeout)
        {
            if (ServiceControllerStatus.Stopped != service.Status)
            {
                StopService(service, timeout);
            }

            StartService(service, timeout);
        }

        /// <summary>
        /// Start a service.
        /// </summary>
        /// <param name="service">service controller.</param>
        /// <param name="timeout">timeout value for starting.</param>
        public static void StartService(ServiceController service, int timeout)
        {
            if (ServiceControllerStatus.Stopped == service.Status)
            {
                Console.WriteLine("Starting service '{0}' on '{1}' ...",
                    service.ServiceName, service.MachineName);

                service.Start();

                /*
                 if (int.MinValue != timeout)
                 {
                     TimeSpan t = TimeSpan.FromSeconds(timeout);
                     service.WaitForStatus(ServiceControllerStatus.Running, t);
                 }
                 else service.WaitForStatus(ServiceControllerStatus.Running);
                 */
                Console.WriteLine("Started service '{0}' on '{1}'\r\n",
                    service.ServiceName, service.MachineName);
            }
            else
            {
                Console.WriteLine("Can not start service '{0}' on '{1}'",
                    service.ServiceName, service.MachineName);

                Console.WriteLine("Service State '{0}'", service.Status.ToString());
            }
        }

        /// <summary>
        /// Pause a service.
        /// </summary>
        /// <param name="service">service controller.</param>
        /// <param name="timeout">timeout value for pausing.</param>
        public static void PauseService(ServiceController service, int timeout)
        {
            if (service.CanPauseAndContinue)
            {
                Console.WriteLine("Pausing service '{0}' on '{1}' ...",
                    service.ServiceName, service.MachineName);

                service.Pause();

                if (int.MinValue != timeout)
                {
                    TimeSpan t = TimeSpan.FromSeconds(timeout);
                    service.WaitForStatus(ServiceControllerStatus.Paused, t);
                }
                else service.WaitForStatus(ServiceControllerStatus.Paused);

                Console.WriteLine("Paused service '{0}' on '{1}'\r\n",
                    service.ServiceName, service.MachineName);
            }
            else
            {
                Console.WriteLine("Can not pause service '{0}' on '{1}'",
                    service.ServiceName, service.MachineName);

                Console.WriteLine("Service State '{0}'", service.Status.ToString());
            }
        }

        /// <summary>
        /// Continue a service.
        /// </summary>
        /// <param name="service">service controller.</param>
        /// <param name="timeout">timeout value for continuing.</param>
        public static void ContinueService(ServiceController service, int timeout)
        {
            if (service.CanPauseAndContinue)
            {
                Console.WriteLine("Continuing service '{0}' on '{1}' ...",
                    service.ServiceName, service.MachineName);

                service.Continue();

                if (int.MinValue != timeout)
                {
                    TimeSpan t = TimeSpan.FromSeconds(timeout);
                    service.WaitForStatus(ServiceControllerStatus.Running, t);
                }
                else service.WaitForStatus(ServiceControllerStatus.Running);

                Console.WriteLine("Continued service '{0}' on '{1}'\r\n",
                    service.ServiceName, service.MachineName);
            }
            else
            {
                Console.WriteLine("Can not continue service '{0}' on '{1}'",
                    service.ServiceName, service.MachineName);

                Console.WriteLine("Service State '{0}'", service.Status.ToString());
            }
        }

        /// <summary>
        /// Stop a service.
        /// </summary>
        /// <param name="service">service controller.</param>
        /// <param name="timeout">timeout for stopping the service.</param>
        public static void StopService(ServiceController service, int timeout)
        {
            if (service.CanStop)
            {
                Console.WriteLine("Stopping service '{0}' on '{1}' ...",
                    service.ServiceName, service.MachineName);

                service.Stop();

                if (int.MinValue != timeout)
                {
                    TimeSpan t = TimeSpan.FromSeconds(timeout);
                    service.WaitForStatus(ServiceControllerStatus.Stopped, t);
                }
                else service.WaitForStatus(ServiceControllerStatus.Stopped);

                Console.WriteLine("Stopped service '{0}' on '{1}'\r\n",
                    service.ServiceName, service.MachineName);
            }
            else
            {
                Console.WriteLine("Can not stop service '{0}' on '{1}'",
                    service.ServiceName, service.MachineName);

                Console.WriteLine("Service State '{0}'", service.Status.ToString());
            }
        }

        public static void GetInstruction()
        {
            Console.WriteLine(Instruction);
        }

        public static readonly string Instruction = "실행프로그램명 \r\n\r\n"
            + "사용법:\r\n"
            + "실행프로그램명 [ action ]\r\n\r\n"
            + Args.INSTALL + "          서비스의 설치\r\n"
            + Args.UNINSTALL + "      서비스의 제거\r\n"
            + Args.START + "             서비스의 시작\r\n"
            + Args.RESTART + "          서비스의 재시작\r\n"
            + Args.STOP + "              서비스의 중지\r\n"
            + Args.PAUSE + "            서비스의 일시정지\r\n";
    }

    public class Args
    {
        public const string STATUS = "-STATUS";
        public const string RESTART = "-RESTART";
        public const string STOP = "-STOP";
        public const string START = "-START";
        public const string PAUSE = "-PAUSE";
        public const string CONTINUE = "-CONTINUE";
        public const string TIMEOUT = "-TIMEOUT";
        public const string USER = "-USER";
        public const string PASSWORD = "-PASSWORD";
        public const string DOMAIN = "-DOMAIN";
        public const string INSTALL = "-INSTALL";
        public const string UNINSTALL = "-UNINSTALL";
    }

    /// <summary>
    /// Impersonate a windows logon.
    /// </summary>
    public class ImpersonationUtil
    {
        /// <summary>
        /// Impersonate given logon information.
        /// </summary>
        /// <param name="logon">Windows logon name.</param>
        /// <param name="password">password</param>
        /// <param name="domain">domain name</param>
        /// <returns></returns>
        public static bool Impersonate(string logon, string password, string domain)
        {
            WindowsIdentity tempWindowsIdentity;
            IntPtr token = IntPtr.Zero;
            IntPtr tokenDuplicate = IntPtr.Zero;

            if (LogonUser(logon, domain, password, LOGON32_LOGON_INTERACTIVE,
                    LOGON32_PROVIDER_DEFAULT, ref token) != 0)
            {
                if (DuplicateToken(token, 2, ref tokenDuplicate) != 0)
                {
                    tempWindowsIdentity = new WindowsIdentity(tokenDuplicate);
                    impersonationContext = tempWindowsIdentity.Impersonate();
                    if (null != impersonationContext) return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Unimpersonate.
        /// </summary>
        public static void UnImpersonate()
        {
            impersonationContext.Undo();
        }

        [DllImport("advapi32.dll", CharSet = CharSet.Auto)]
        public static extern int LogonUser(
            string lpszUserName,
            String lpszDomain,
            String lpszPassword,
            int dwLogonType,
            int dwLogonProvider,
            ref IntPtr phToken);

        [DllImport("advapi32.dll", CharSet = System.Runtime.InteropServices.CharSet.Auto, SetLastError = true)]
        public extern static int DuplicateToken(
            IntPtr hToken,
            int impersonationLevel,
            ref IntPtr hNewToken);

        private const int LOGON32_LOGON_INTERACTIVE = 2;
        private const int LOGON32_LOGON_NETWORK_CLEARTEXT = 4;
        private const int LOGON32_PROVIDER_DEFAULT = 0;
        private static WindowsImpersonationContext impersonationContext;
    }
}