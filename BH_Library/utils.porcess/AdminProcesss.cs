﻿using System;
using System.Diagnostics;
using System.Security.Principal;

namespace BH_Library.Utils.Porcess
{
    /// <summary>
    /// 관리자 권한으로 프로세스를 실행시키는 기능을 제공합니다.
    /// </summary>
    public class AdminProcesss
    {
        //관리자 권한으로 실행
        public static bool Excute(string fileName, string[] args)
        {
            bool retVal = true;

            //vista 미만일때는 관리자 권한으로 실행할 필요가 없음.
            if (Environment.OSVersion.Version.Major < 6) return true;

            // 파일이름이 존재하지 않을경우
            if (!System.IO.File.Exists(fileName)) throw new ApplicationException("지정된 파일이 존재하지 않습니다.");

            System.IO.FileInfo fi = new System.IO.FileInfo(fileName);

            // 실행할 프로세스
            ProcessStartInfo proc = new ProcessStartInfo();
            proc.UseShellExecute = true;
            proc.WorkingDirectory = fi.DirectoryName;
            proc.FileName = fi.Name;

            // 파라메터 처리
            string argument = string.Empty;
            if (args != null)
            {
                for (int i = 0; i < args.Length; i++)
                {
                    if (argument == string.Empty)
                    {
                        argument = args[i];
                    }
                    else
                    {
                        argument += " " + args[i];
                    }
                }
            }
            // 파라메터 설정
            proc.Arguments = argument;
            proc.UseShellExecute = true;
            proc.Verb = "runas";

            // 프로세스 실행
            Process.Start(proc);

            return retVal;
        }

        /// <summary>
        /// 현재 실행중인 프로세스가 관리자 권한으로 실행중인지 여부를 제공합니다.
        /// </summary>
        /// <returns></returns>
        public static bool IsRunAsAdmin()
        {
            bool isAdmin = false;

            WindowsIdentity id = WindowsIdentity.GetCurrent();
            WindowsPrincipal principal = new WindowsPrincipal(id);
            isAdmin = principal.IsInRole(WindowsBuiltInRole.Administrator);

            return isAdmin;
        }
    }
}