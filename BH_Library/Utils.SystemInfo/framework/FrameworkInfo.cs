﻿using System.Collections.Generic;

namespace BH_Library.Utils.SystemInfo.Framework
{
    public class FrameworkInfo
    {
        private List<FrameworkInfo> _subInfo;
        private string _version;
        private string _sp;
        private FrameworkTypes _frameworkType;

        /// <summary>
        /// 닷넷 프레임워크 정보를 제공합니다.
        /// </summary>
        public FrameworkInfo()
        {
            FrameworkType = FrameworkTypes.Full;
            ServicePack = string.Empty;
        }

        /// <summary>
        /// 프레임워크 타입(Client, Full)을 제공합니다.
        /// </summary>
        public enum FrameworkTypes
        {
            Full,
            Client
        }

        /// <summary>
        /// 프레임워크 버전을 제공합니다.
        /// </summary>
        public enum FrameworkVersions
        {
            Other,
            V2,
            V3,
            V3_5,
            V4,
            V4Later
        }

        /// <summary>
        /// 프레임워크 타입을 제공합니다.
        /// </summary>
        public FrameworkTypes FrameworkType
        {
            get
            {
                if (SubFrameworkInfo.Count > 0)
                {
                    return SubFrameworkInfo[0].FrameworkType;
                }
                else
                {
                    return _frameworkType;
                }
            }
            set
            {
                _frameworkType = value;
            }
        }

        /// <summary>
        /// 프레임워크 버젼을 제공합니다.
        /// </summary>
        public FrameworkVersions FrameworkVersion
        {
            get
            {
                FrameworkVersions frameworkVersion = FrameworkVersions.Other;

                if (!string.IsNullOrEmpty(Version))
                {
                    if (Version.StartsWith("2"))
                    {
                        frameworkVersion = FrameworkVersions.V2;
                    }
                    else if (Version.StartsWith("3.0"))
                    {
                        frameworkVersion = FrameworkVersions.V3;
                    }
                    else if (Version.StartsWith("3.5"))
                    {
                        frameworkVersion = FrameworkVersions.V3_5;
                    }
                    else if (Version.StartsWith("4"))
                    {
                        frameworkVersion = FrameworkVersions.V4;
                        if (Version.StartsWith("4.5"))
                        {
                            frameworkVersion = FrameworkVersions.V4Later;
                        }
                    }
                }

                return frameworkVersion;
            }
        }

        /// <summary>
        /// 버젼을 제공합니다.
        /// </summary>
        public string Version
        {
            get
            {
                if (SubFrameworkInfo.Count > 0)
                {
                    return SubFrameworkInfo[0].Version;
                }
                else
                {
                    return _version;
                }
            }
            set
            {
                _version = value;
            }
        }

        /// <summary>
        /// 서비스팩 정보를 제공합니다.
        /// </summary>
        public string ServicePack
        {
            get
            {
                if (SubFrameworkInfo.Count > 0)
                {
                    return SubFrameworkInfo[0].ServicePack;
                }
                else
                {
                    return _sp;
                }
            }
            set
            {
                _sp = value;
            }
        }

        /// <summary>
        /// 프레임워크 정보를 리스트형태로 제공합니다.
        /// </summary>
        public List<FrameworkInfo> SubFrameworkInfo
        {
            get
            {
                if (_subInfo == null)
                {
                    _subInfo = new List<FrameworkInfo>();
                }

                return _subInfo;
            }
            set
            {
                _subInfo = value;
            }
        }

        /// <summary>
        /// 프레임워크 정보를 문자열 형태로 제공합니다.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("v{0} {1}({2})", Version, (string.IsNullOrEmpty(ServicePack) ? string.Empty : ServicePack), FrameworkType);
        }
    }
}