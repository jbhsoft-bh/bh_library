﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;

namespace BH_Library.Utils.SystemInfo.Framework
{
    public class FrameworkAnalyzer
    {
        public FrameworkAnalyzer()
        {
        }

        /// <summary>
        /// PC에 설치된 닷넷프레임웍 정보를 얻어온다.
        /// </summary>
        public List<FrameworkInfo> InstalledFrameWorks
        {
            get
            {
                List<FrameworkInfo> frameworkInfoList = new List<FrameworkInfo>();
                try
                {
                    frameworkInfoList = GetDotNetFrameworkInfo();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                return frameworkInfoList;
            }
        }

        private List<FrameworkInfo> GetDotNetFrameworkInfo()
        {
            List<FrameworkInfo> frameworkInfoList = new List<FrameworkInfo>();

            using (RegistryKey ndpKey = RegistryKey.OpenRemoteBaseKey(RegistryHive.LocalMachine, string.Empty).OpenSubKey(@"SOFTWARE\Microsoft\NET Framework Setup\NDP\"))
            {
                foreach (string versionKeyName in ndpKey.GetSubKeyNames())
                {
                    if (versionKeyName.StartsWith("v"))
                    {
                        RegistryKey versionKey = ndpKey.OpenSubKey(versionKeyName);
                        string name = (string)versionKey.GetValue("Version", "");
                        string sp = versionKey.GetValue("SP", "").ToString();
                        string install = versionKey.GetValue("Install", "").ToString();
                        if (install != "") //no install info, ust be later
                        {
                            if (install == "1")
                            {
                                frameworkInfoList.Add(new FrameworkInfo()
                                {
                                    FrameworkType = FrameworkInfo.FrameworkTypes.Full,
                                    Version = name,
                                    ServicePack = (sp != string.Empty ? "SP" + sp : string.Empty)
                                });
                            }
                        }
                        else
                        {
                            FrameworkInfo parentFrameworkInfo = new FrameworkInfo()
                            {
                                FrameworkType = FrameworkInfo.FrameworkTypes.Full,
                                Version = string.Empty,
                                ServicePack = string.Empty
                            };

                            frameworkInfoList.Add(parentFrameworkInfo);

                            if (name != "")
                            {
                                continue;
                            }

                            foreach (string subKeyName in versionKey.GetSubKeyNames())
                            {
                                RegistryKey subKey = versionKey.OpenSubKey(subKeyName);
                                name = (string)subKey.GetValue("Version", "");
                                if (name != "")
                                    sp = subKey.GetValue("SP", "").ToString();
                                install = subKey.GetValue("Install", "").ToString();
                                if (install == "1")
                                {
                                    parentFrameworkInfo.SubFrameworkInfo.Add(new FrameworkInfo()
                                    {
                                        FrameworkType = (subKeyName.ToUpper().Equals("CLIENT") ? FrameworkInfo.FrameworkTypes.Client : FrameworkInfo.FrameworkTypes.Full),
                                        Version = name,
                                        ServicePack = (sp != string.Empty ? "SP" + sp : string.Empty)
                                    });
                                }
                            }
                        }
                    }
                }
            }

            return frameworkInfoList;
        }
    }
}