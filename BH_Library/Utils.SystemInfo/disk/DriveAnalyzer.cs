﻿using System.Collections.Generic;

namespace BH_Library.Utils.SystemInfo.Disk
{
    /// <summary>
    /// 디스크 정보를 제공합니다.
    /// </summary>
    public class DriveAnalyzer
    {
        private List<Result> searchResults = HardwareInfoSearcher.SearchInfo("Win32_LogicalDisk");

        /// <summary>
        /// 로컬 하드디스크 리스트를 제공합니다.
        /// </summary>
        public List<DriveInfo> HDDInfo
        {
            get
            {
                List<DriveInfo> hDDObjectList = new List<DriveInfo>();

                foreach (var searchResult in searchResults)
                {
                    DriveInfo hDDObject = new DriveInfo();
                    hDDObject.Drive = searchResult.Name;
                    hDDObject.TotalSpace = GetTotalSpace(searchResult.SearchSubResults);
                    hDDObject.FreeSpace = GetFreeSpace(searchResult.SearchSubResults);
                    hDDObject.FileSystem = GetFileSystem(searchResult.SearchSubResults);
                    hDDObject.DiskType = GetDiskType(searchResult.SearchSubResults);
                    hDDObject.SerialNumber = GetVolumeSerialNumber(searchResult.SearchSubResults);
                    hDDObjectList.Add(hDDObject);
                }

                return hDDObjectList;
            }
        }

        /// <summary>
        /// 디스크의 유일한 시리얼번호를제공합니다.
        /// </summary>
        /// <returns></returns>
        public string UniqeDiskSerialNumber()
        {
            string retVal = "Unknown";

            List<DriveInfo> drvieList = HDDInfo;

            foreach (DriveInfo driver in drvieList)
            {
                if (driver.SerialNumber.ToUpper() != "UNKNOWN")
                {
                    retVal = driver.SerialNumber;
                    break;
                }
            }

            return retVal;
        }

        private string GetVolumeSerialNumber(List<SubResult> searchSubResult)
        {
            string ret = "Unknown";

            for (int i = 0; i < searchSubResult.Count; i++)
            {
                if (searchSubResult[i].Name == "VolumeSerialNumber")
                {
                    ret = searchSubResult[i].Value;

                    break;
                }
            }

            return ret;
        }

        private long GetTotalSpace(List<SubResult> searchSubResult)
        {
            long ret = 0L;

            for (int i = 0; i < searchSubResult.Count; i++)
            {
                if (searchSubResult[i].Name == "Size")
                {
                    long val = 0L;
                    long.TryParse(searchSubResult[i].Value, out val);
                    ret = val;

                    break;
                }
            }

            return ret;
        }

        private long GetFreeSpace(List<SubResult> searchSubResult)
        {
            long ret = 0L;

            for (int i = 0; i < searchSubResult.Count; i++)
            {
                if (searchSubResult[i].Name == "FreeSpace")
                {
                    long val = 0L;
                    long.TryParse(searchSubResult[i].Value, out val);
                    ret = val;

                    break;
                }
            }

            return ret;
        }

        private string GetFileSystem(List<SubResult> searchSubResult)
        {
            string ret = "Unknown";

            for (int i = 0; i < searchSubResult.Count; i++)
            {
                if (searchSubResult[i].Name == "FileSystem")
                {
                    ret = searchSubResult[i].Value;

                    break;
                }
            }

            return ret;
        }

        private string GetDiskType(List<SubResult> searchSubResult)
        {
            string ret = "Unknown";

            for (int i = 0; i < searchSubResult.Count; i++)
            {
                if (searchSubResult[i].Name == "Description")
                {
                    ret = searchSubResult[i].Value;

                    break;
                }
            }

            return ret;
        }
    }
}