﻿namespace BH_Library.Utils.SystemInfo.Disk
{
    /// <summary>
    /// 디스크 드라이브 정보를 제공합니다.
    /// </summary>
    public class DriveInfo
    {
        /// <summary>
        /// 드라이브 명을 제공합니다.
        /// </summary>
        public string Drive
        {
            get;
            set;
        }

        /// <summary>
        /// 전체 용량을 제공합니다.
        /// </summary>
        public long TotalSpace
        {
            get;
            set;
        }

        /// <summary>
        /// 전체 남은 용량을 제공합니다.
        /// </summary>
        public long FreeSpace
        {
            get;
            set;
        }

        /// <summary>
        /// 파일 시스템 구조를 제공합니다.
        /// </summary>
        public string FileSystem
        {
            get;
            set;
        }

        /// <summary>
        /// 디스크 타입을 제공합니다.
        /// </summary>
        public string DiskType
        {
            get;
            set;
        }

        /// <summary>
        /// 디스크의 시리얼 넘버를 설정하거나 제공합니다.
        /// </summary>
        public string SerialNumber
        {
            get;
            set;
        }
    }
}