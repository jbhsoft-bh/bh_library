﻿using System;
using System.Net;
using System.Net.NetworkInformation;

namespace BH_Library.Utils.SystemInfo.Network
{
    public class NetworkAnalyzer
    {
        /// <summary>
        /// 네크워크 정보를 제공합니다.
        /// </summary>
        public NetworkAnalyzer()
        {
            try
            {
                IPAddress ipAddress = GetRealIpAddress();
                if (ipAddress != null)
                {
                    IPAddress = ipAddress.ToString();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            try
            {
                //GatewayAddress = GetGatewayID();
                IPAddress gatewayAddress = FindGetGatewayAddress();
                if (gatewayAddress != null)
                {
                    GatewayAddress = gatewayAddress.ToString();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            try
            {
                NetworkInterface[] adapters = NetworkInterface.GetAllNetworkInterfaces();
                foreach (NetworkInterface adapter in adapters)
                {
                    PhysicalAddress pa = adapter.GetPhysicalAddress();
                    if (pa != null && !pa.ToString().Equals(""))
                    {
                        MacAddress = pa.ToString();
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// IP 주소를 제공합니다.
        /// </summary>
        public string IPAddress
        {
            get;
            set;
        }

        /// <summary>
        /// 게이트웨이 주소를 제공합니다.
        /// </summary>
        public string GatewayAddress
        {
            get;
            set;
        }

        /// <summary>
        /// 네트워크의 기본 MAC Address를 제공합니다.
        /// </summary>
        public string MacAddress
        {
            get;
            set;
        }

        #region GetRealIpAddress

        private IPAddress GetRealIpAddress()
        {
            IPAddress gateway = FindGetGatewayAddress();

            if (gateway == null)
                return null;

            IPAddress[] pIPAddress = Dns.GetHostAddresses(Dns.GetHostName());

            foreach (IPAddress address in pIPAddress)
            {
                if (IsAddressOfGateway(address, gateway))
                {
                    return address;
                }
            }

            return null;
        }

        private bool IsAddressOfGateway(IPAddress address, IPAddress gateway)
        {
            if (address != null && gateway != null)
            {
                return IsAddressOfGateway(address.GetAddressBytes(), gateway.GetAddressBytes());
            }

            return false;
        }

        private bool IsAddressOfGateway(byte[] address, byte[] gateway)
        {
            if (address != null && gateway != null)
            {
                int gwLen = gateway.Length;

                if (gwLen > 0)
                {
                    if (address.Length == gateway.Length)
                    {
                        --gwLen;
                        int counter = 0;

                        for (int i = 0; i < gwLen; i++)
                        {
                            if (address[i] == gateway[i])
                            {
                                ++counter;
                            }
                        }

                        return (counter == gwLen);
                    }
                }
            }

            return false;
        }

        private IPAddress FindGetGatewayAddress()
        {
            IPGlobalProperties ipGlobProps = IPGlobalProperties.GetIPGlobalProperties();

            foreach (NetworkInterface ni in NetworkInterface.GetAllNetworkInterfaces())
            {
                IPInterfaceProperties ipInfProps = ni.GetIPProperties();

                foreach (GatewayIPAddressInformation gi in ipInfProps.GatewayAddresses)
                {
                    return gi.Address;
                }
            }

            return null;
        }

        #endregion GetRealIpAddress
    }
}