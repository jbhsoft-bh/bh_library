﻿using System;
using System.Collections.Generic;
using System.Management;

namespace BH_Library.Utils.SystemInfo.CPU
{
    /// <summary>
    /// CPU 정보를 제공합니다.
    /// </summary>
    public class CPUAnalyzer
    {
        private List<Result> searchResults = HardwareInfoSearcher.SearchInfo("Win32_Processor");

        /// <summary>
        /// CPU 프로세스 정보를 제공합니다.
        /// </summary>
        public string ProcessorName
        {
            get
            {
                string cpuInfo = String.Empty;

                //Win32_Processor class
                ManagementClass mgmt = new ManagementClass("Win32_Processor");

                //create a ManagementObjectCollection to loop through
                ManagementObjectCollection objCol = mgmt.GetInstances();

                //start our loop for all processors found
                foreach (ManagementObject obj in objCol)

                {
                    if (cpuInfo == String.Empty)
                    {
                        // only return cpuInfo from first CPU
                        cpuInfo = obj.Properties["ProcessorId"].Value.ToString();
                        break;
                    }
                }
                return cpuInfo;
            }
        }
    }
}