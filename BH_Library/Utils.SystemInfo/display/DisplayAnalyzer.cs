﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace BH_Library.Utils.SystemInfo.Display
{
    /// <summary>
    /// 디스플레이 정보를 제공합니다.
    /// </summary>
    public class DisplayAnalyzer
    {
        /// <summary>
        /// 디스플레이 목록을 제공합니다.
        /// </summary>
        public List<DisplayInfo> Displays
        {
            get
            {
                List<DisplayInfo> displayInfoList = new List<DisplayInfo>();
                foreach (Screen scr in Screen.AllScreens)
                {
                    DisplayInfo info = new DisplayInfo();
                    info.Name = scr.DeviceName;
                    info.IsPrimary = scr.Primary;
                    info.Height = scr.Bounds.Height;
                    info.Width = scr.Bounds.Width;

                    displayInfoList.Add(info);
                }

                return displayInfoList;
            }
        }
    }
}