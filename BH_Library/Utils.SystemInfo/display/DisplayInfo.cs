﻿namespace BH_Library.Utils.SystemInfo.Display
{
    /// <summary>
    /// 디스플레이 정보를 제공합니다.
    /// </summary>
    public class DisplayInfo
    {
        /// <summary>
        /// 디스플레이 이름을 제공합니다.
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// 주 디스플레이 여부를 제공합니다.
        /// </summary>
        public bool IsPrimary
        {
            get;
            set;
        }

        /// <summary>
        /// 디스플레이의 높이를 제공합니다.
        /// </summary>
        public int Height
        {
            get;
            set;
        }

        /// <summary>
        /// 디스플레이의 너비를 제공합니다.
        /// </summary>
        public int Width
        {
            get;
            set;
        }

        /// <summary>
        /// 디스플레이의 가로*세로 정보를 제공합니다.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0}*{1}", Width, Height);
        }
    }
}