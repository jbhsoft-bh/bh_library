﻿using System;

namespace BH_Library.Utils.SystemInfo.Product
{
    /// <summary>
    /// 얼마에요 제품정보를 제공합니다.
    /// </summary>
    public static class ProductAnalyzer
    {
        /// <summary>
        /// 얼마에요 3E 의 제품정보를 제공합니다.
        /// </summary>
        /// <returns></returns>
        public static HowMuch3EInfo HOWMUCH3E()
        {
            HowMuch3EInfo Info = new HowMuch3EInfo();
            try
            {
                Info.CheckShortcutFirst = Convert.ToBoolean(BH_Library.Utils.Register.Registry.LocalMachineRead(@"SOFTWARE\IQUEST\HOWMUCH3E\", "CheckShortcutFirst", "False"));
                Info.DBID = BH_Library.Utils.Register.Registry.LocalMachineRead(@"SOFTWARE\IQUEST\HOWMUCH3E\", "DBID", "");
                Info.DBName = BH_Library.Utils.Register.Registry.LocalMachineRead(@"SOFTWARE\IQUEST\HOWMUCH3E\", "DBName", "");
                Info.DBSecurity = BH_Library.Utils.Register.Registry.LocalMachineRead(@"SOFTWARE\IQUEST\HOWMUCH3E\", "DBSecurity", "");
                Info.ExeName = BH_Library.Utils.Register.Registry.LocalMachineRead(@"SOFTWARE\IQUEST\HOWMUCH3E\", "ExeName", "");
                Info.frmMainRibbonHeight = Convert.ToInt64(BH_Library.Utils.Register.Registry.LocalMachineRead(@"SOFTWARE\IQUEST\HOWMUCH3E\", "frmMainRibbonHeight", "0"));
                Info.frmMainRibbonLeft = Convert.ToInt64(BH_Library.Utils.Register.Registry.LocalMachineRead(@"SOFTWARE\IQUEST\HOWMUCH3E\", "frmMainRibbonLeft", "0"));
                Info.frmMainRibbonMaximized = BH_Library.Utils.Register.Registry.LocalMachineRead(@"SOFTWARE\IQUEST\HOWMUCH3E\", "frmMainRibbonMaximized", "");
                Info.frmMainRibbonTop = Convert.ToInt64(BH_Library.Utils.Register.Registry.LocalMachineRead(@"SOFTWARE\IQUEST\HOWMUCH3E\", "frmMainRibbonTop", "0"));
                Info.frmMainRibbonWidth = Convert.ToInt64(BH_Library.Utils.Register.Registry.LocalMachineRead(@"SOFTWARE\IQUEST\HOWMUCH3E\", "frmMainRibbonWidth", "0"));
                Info.HM3LauncherUpdateTime = BH_Library.Utils.Register.Registry.LocalMachineRead(@"SOFTWARE\IQUEST\HOWMUCH3E\", "HM3LauncherUpdateTime", "");
                Info.InstanceName = BH_Library.Utils.Register.Registry.LocalMachineRead(@"SOFTWARE\IQUEST\HOWMUCH3E\", "InstanceName", "");
                Info.MaxUser = Convert.ToInt64(BH_Library.Utils.Register.Registry.LocalMachineRead(@"SOFTWARE\IQUEST\HOWMUCH3E\", "MaxUser", "0"));
                Info.MyMagicBill = BH_Library.Utils.Register.Registry.LocalMachineRead(@"SOFTWARE\IQUEST\HOWMUCH3E\", "MyMagicBill", "");
                Info.SQLCompanyCode = BH_Library.Utils.Register.Registry.LocalMachineRead(@"SOFTWARE\IQUEST\HOWMUCH3E\", "SQLCompanyCode", "");
                Info.SQLCompanyName = BH_Library.Utils.Register.Registry.LocalMachineRead(@"SOFTWARE\IQUEST\HOWMUCH3E\", "SQLCompanyName", "");
                Info.SqlCompanySerialNumber = BH_Library.Utils.Register.Registry.LocalMachineRead(@"SOFTWARE\IQUEST\HOWMUCH3E\", "SqlCompanySerialNumber", "");
                Info.SQLCurrentCBit = BH_Library.Utils.Register.Registry.LocalMachineRead(@"SOFTWARE\IQUEST\HOWMUCH3E\", "SQLCurrentCBit", "");
                Info.SQLIPAddress = BH_Library.Utils.Register.Registry.LocalMachineRead(@"SOFTWARE\IQUEST\HOWMUCH3E\", "SQLIPAddress", "");
                Info.SQLLocalDBPath = BH_Library.Utils.Register.Registry.LocalMachineRead(@"SOFTWARE\IQUEST\HOWMUCH3E\", "SQLLocalDBPath", "");
                Info.SQLPort = BH_Library.Utils.Register.Registry.LocalMachineRead(@"SOFTWARE\IQUEST\HOWMUCH3E\", "SQLPort", "0");
                Info.SQLServerString = BH_Library.Utils.Register.Registry.LocalMachineRead(@"SOFTWARE\IQUEST\HOWMUCH3E\", "SQLServerString", "");
                Info.SQLServerType = BH_Library.Utils.Register.Registry.LocalMachineRead(@"SOFTWARE\IQUEST\HOWMUCH3E\", "SQLServerType", "");
                Info.SQLTAXNo = BH_Library.Utils.Register.Registry.LocalMachineRead(@"SOFTWARE\IQUEST\HOWMUCH3E\", "SQLTAXNo", "");
                Info.SQLUserID = BH_Library.Utils.Register.Registry.LocalMachineRead(@"SOFTWARE\IQUEST\HOWMUCH3E\", "SQLID", "");
                Info.SQLUserPassword = BH_Library.Utils.Register.Registry.LocalMachineRead(@"SOFTWARE\IQUEST\HOWMUCH3E\", "SQLPassword", "");
                Info.SQLSavedPath3E = BH_Library.Utils.Register.Registry.LocalMachineRead(@"SOFTWARE\IQUEST\BackupPath\", "3E", "");
                Info.SQLSavedPath3EWOORI = BH_Library.Utils.Register.Registry.LocalMachineRead(@"SOFTWARE\IQUEST\BackupPath\", "3EWOORI", "");
                Info.WebBackupUse3E = Convert.ToBoolean(BH_Library.Utils.Register.Registry.LocalMachineRead(@"SOFTWARE\IQUEST\BackupPath\", "3E_Webbak", "false"));
                Info.WebBackupUse3EWOORI = Convert.ToBoolean(BH_Library.Utils.Register.Registry.LocalMachineRead(@"SOFTWARE\IQUEST\BackupPath\", "3EWOORI_Webbak", "false"));
                Info.SQLInstanceName = BH_Library.Utils.Register.Registry.LocalMachineRead(@"SOFTWARE\IQUEST\HOWMUCH3E\", "SQLInstance", "");
                Info.SQLDatabaseName = BH_Library.Utils.Register.Registry.LocalMachineRead(@"SOFTWARE\IQUEST\HOWMUCH3E\", "SQLDBName", "");

                // 마이매직빌은 CBIt가 의미가 없기때문
                if (Info.MyMagicBill == "1")
                {
                    Info.ProductName = "My매직빌";
                }
                else
                {
                    switch (Info.SQLCurrentCBit)
                    {
                        case "02": Info.ProductName = "e-편한장부"; break;
                        case "11": Info.ProductName = "얼마에요 2000"; break;
                        case "15": Info.ProductName = "얼마에요 ME"; break;
                        case "17": Info.ProductName = "얼마에요 PLUS"; break;
                        case "20": Info.ProductName = "얼마에요 LITE"; break;
                        case "22": Info.ProductName = "얼마에요 PRO"; break;
                        case "24": Info.ProductName = "얼마에요 EASY"; break;
                        case "25": Info.ProductName = "얼마에요 ACE"; break;
                        case "35": Info.ProductName = "얼마에요 X2 PEx"; break;
                        case "36": Info.ProductName = "얼마에요 X2 PEx 2년선납"; break;
                        case "42": Info.ProductName = "얼마에요 ERP"; break;
                        case "44": Info.ProductName = "얼마에요 X2"; break;
                        case "43": Info.ProductName = "얼마에요 x2 PE USB"; break;
                        case "45": Info.ProductName = "얼마에요 X2 SE USB"; break;
                        case "48": Info.ProductName = "얼마에요 ERP 포터블"; break;
                        case "49": Info.ProductName = "얼마에요 x2 포터블"; break;
                        case "47": Info.ProductName = "얼마에요 X2 재고물류"; break;
                        case "50": Info.ProductName = "얼마에요 X2 급여관리"; break;
                        case "52": Info.ProductName = "얼마에요 X2 고객관리"; break;
                        case "53": Info.ProductName = "얼마에요 X2 고정자산"; break;
                        case "54": Info.ProductName = "우리ERP"; break;
                        case "55": Info.ProductName = "얼마에요 통"; break;
                        case "56": Info.ProductName = "얼마에요ERP(신한)"; break;
                        case "62": Info.ProductName = "롯데 e-편한장부"; break;
                        case "63": Info.ProductName = "이지세무(세무지원서비스)"; break;
                        case "65": Info.ProductName = "WinCMS 세무지원"; break;
                        case "67": Info.ProductName = "매직노트"; break;
                        case "70": Info.ProductName = "얼마에요 3E"; break;
                        case "71": Info.ProductName = "아이퀘스트 ERP-1"; break;
                        case "72": Info.ProductName = "아이퀘스트 ERP"; break;
                        case "74": Info.ProductName = "우리ERP 3E"; break;
                        case "75": Info.ProductName = "My Company ERP"; break;
                        case "76": Info.ProductName = "InsideBank ERP"; break;
                        case "77": Info.ProductName = "LG U+ Biz ERP"; break;
                        case "79": Info.ProductName = "얼마에요 ERP(PC)"; break;
                        case "80": Info.ProductName = "얼마에요 ERP(서버)"; break;
                    }
                }
            }
            catch (Exception ex)
            {
                Info = null;
                Console.WriteLine(ex.Message);
            }

            return Info;
        }

        /// <summary>
        /// 얼마에요 2E 의 제품정보를 제공합니다.
        /// </summary>
        /// <returns></returns>
        public static HowMuch2EInfo HOWMUCH2E()
        {
            HowMuch2EInfo Info = new HowMuch2EInfo();
            try
            {
                Info.SerialNumber = BH_Library.Utils.Register.Registry.LocalMachineRead(@"SOFTWARE\IQUEST\HOWMUCH2E\", "SerialNumber", "");
                Info.CBit = BH_Library.Utils.Register.Registry.LocalMachineRead(@"SOFTWARE\IQUEST\HOWMUCH2E\", "CBit", "");
                Info.SQLSavedPath2E = BH_Library.Utils.Register.Registry.LocalMachineRead(@"SOFTWARE\IQUEST\BackupPath\", "2E", "");
                Info.DataType = BH_Library.Utils.Register.Registry.LocalMachineRead(@"SOFTWARE\IQUEST\HOWMUCH2E\", "DataType", "");
                Info.SQLSavedPath2EWOORI = BH_Library.Utils.Register.Registry.LocalMachineRead(@"SOFTWARE\IQUEST\BackupPath\", "2EWOORI", "");
                Info.WebBackupUse2E = Convert.ToBoolean(BH_Library.Utils.Register.Registry.LocalMachineRead(@"SOFTWARE\IQUEST\BackupPath\", "2E_Webbak", "false"));
                Info.WebBackupUse2EWOORI = Convert.ToBoolean(BH_Library.Utils.Register.Registry.LocalMachineRead(@"SOFTWARE\IQUEST\BackupPath\", "2EWOORI_Webbak", "false"));
                Info.CompanyNo = BH_Library.Utils.Register.Registry.LocalMachineRead(@"SOFTWARE\IQUEST\HOWMUCH2E\", "CompanyNo", "");
                Info.SqlIpAddress = BH_Library.Utils.Register.Registry.LocalMachineRead(@"SOFTWARE\IQUEST\HOWMUCH2E\", "SqlIpAddress", "");
                Info.SqlPort = BH_Library.Utils.Register.Registry.LocalMachineRead(@"SOFTWARE\IQUEST\HOWMUCH2E\", "SqlPort", "");
                Info.SqlId = BH_Library.Utils.Register.Registry.LocalMachineRead(@"SOFTWARE\IQUEST\HOWMUCH2E\", "SqlId", "");
                Info.SqlPassword = BH_Library.Utils.Register.Registry.LocalMachineRead(@"SOFTWARE\IQUEST\HOWMUCH2E\", "SqlPassword", "");

                switch (Info.CBit)
                {
                    case "02": Info.ProductName = "e-편한장부"; break;
                    case "11": Info.ProductName = "얼마에요 2000"; break;
                    case "15": Info.ProductName = "얼마에요 ME"; break;
                    case "17": Info.ProductName = "얼마에요 PLUS"; break;
                    case "20": Info.ProductName = "얼마에요 LITE"; break;
                    case "22": Info.ProductName = "얼마에요 PRO"; break;
                    case "24": Info.ProductName = "얼마에요 EASY"; break;
                    case "25": Info.ProductName = "얼마에요 ACE"; break;
                    case "35": Info.ProductName = "얼마에요 X2 PEx"; break;
                    case "36": Info.ProductName = "얼마에요 X2 PEx 2년선납"; break;
                    case "42": Info.ProductName = "얼마에요 ERP"; break;
                    case "44": Info.ProductName = "얼마에요 X2"; break;
                    case "43": Info.ProductName = "얼마에요 x2 PE USB"; break;
                    case "45": Info.ProductName = "얼마에요 X2 SE USB"; break;
                    case "48": Info.ProductName = "얼마에요 ERP 포터블"; break;
                    case "49": Info.ProductName = "얼마에요 x2 포터블"; break;
                    case "47": Info.ProductName = "얼마에요 X2 재고물류"; break;
                    case "50": Info.ProductName = "얼마에요 X2 급여관리"; break;
                    case "52": Info.ProductName = "얼마에요 X2 고객관리"; break;
                    case "53": Info.ProductName = "얼마에요 X2 고정자산"; break;
                    case "54": Info.ProductName = "우리ERP"; break;
                    case "55": Info.ProductName = "얼마에요 통"; break;
                    case "56": Info.ProductName = "얼마에요ERP(신한)"; break;
                    case "62": Info.ProductName = "롯데 e-편한장부"; break;
                    case "63": Info.ProductName = "이지세무(세무지원서비스)"; break;
                    case "65": Info.ProductName = "WinCMS 세무지원"; break;
                    case "67": Info.ProductName = "매직노트"; break;
                    case "70": Info.ProductName = "얼마에요 3E"; break;
                    case "71": Info.ProductName = "아이퀘스트 ERP-1"; break;
                    case "72": Info.ProductName = "아이퀘스트 ERP"; break;
                    case "74": Info.ProductName = "우리ERP 3E"; break;
                    case "75": Info.ProductName = "My Company ERP"; break;
                    case "76": Info.ProductName = "InsideBank ERP"; break;
                    case "77": Info.ProductName = "LG U+ Biz ERP"; break;
                    case "79": Info.ProductName = "얼마에요 ERP(PC)"; break;
                    case "80": Info.ProductName = "얼마에요 ERP(서버)"; break;
                }
            }
            catch (Exception ex)
            {
                Info = null;
                Console.WriteLine(ex.Message);
            }

            return Info;
        }
    }

    /// <summary>
    /// 얼마에요3E 데이터 클래스
    /// </summary>
    public class HowMuch3EInfo
    {
        /// <summary>
        /// 얼마에요3E의 제품이름을 제공합니다.
        /// </summary>
        public string ProductName { get; set; }

        /// <summary>
        /// 쇼컷 생성 여부를 제공합니다.
        /// </summary>
        public bool CheckShortcutFirst { get; set; }

        /// <summary>
        /// 데이터베이스 ID를 제공합니다.
        /// </summary>
        public string DBID { get; set; }

        /// <summary>
        /// 데이터베이스 이름을 제공합니다.
        /// </summary>
        public string DBName { get; set; }

        /// <summary>
        /// 데이터베이스 암호를 제공합니다.
        /// </summary>
        public string DBSecurity { get; set; }

        /// <summary>
        /// 실행된 프로그램이름을 제공합니다.
        /// </summary>
        public string ExeName { get; set; }

        /// <summary>
        /// 리본의 높이정보를 제공합니다.
        /// </summary>
        public long frmMainRibbonHeight { get; set; }

        /// <summary>
        /// 리본의 Left정보를 제공합니다.
        /// </summary>
        public long frmMainRibbonLeft { get; set; }

        /// <summary>
        /// 리본의 상태정보를 제공합니다.
        /// </summary>
        public string frmMainRibbonMaximized { get; set; }

        /// <summary>
        /// 리본의 Top정보를 제공합니다.
        /// </summary>
        public long frmMainRibbonTop { get; set; }

        /// <summary>
        /// 리본의 너비정보를 제공합니다.
        /// </summary>
        public long frmMainRibbonWidth { get; set; }

        /// <summary>
        /// 업데이트 마지막 시간정보를 제공합니다.
        /// </summary>
        public string HM3LauncherUpdateTime { get; set; }

        /// <summary>
        /// 인스턴스 이름을 제공합니다.
        /// </summary>
        public string InstanceName { get; set; }

        /// <summary>
        /// 최대 사용자 정보를 제공합니다.
        /// </summary>
        public long MaxUser { get; set; }

        /// <summary>
        /// 매직빌 여부정보를 제공합니다.
        /// </summary>
        public string MyMagicBill { get; set; }

        /// <summary>
        /// 실행중인 회사코드 정보를 제공합니다.
        /// </summary>
        public string SQLCompanyCode { get; set; }

        /// <summary>
        /// 실행중인 회사이름 정보를 제공합니다.
        /// </summary>
        public string SQLCompanyName { get; set; }

        /// <summary>
        /// 제품 일련번호를 제공합니다.
        /// </summary>
        public string SqlCompanySerialNumber { get; set; }

        /// <summary>
        /// 제품 코드정보를 제공합니다.
        /// </summary>
        public string SQLCurrentCBit { get; set; }

        /// <summary>
        /// SQL 서버의 IP정보를 제공합니다.
        /// </summary>
        public string SQLIPAddress { get; set; }

        /// <summary>
        /// 로컬DB의 경로정보를 제공합니다.
        /// </summary>
        public string SQLLocalDBPath { get; set; }

        /// <summary>
        /// SQL서버의 포트정보를 제공합니다.
        /// </summary>
        public string SQLPort { get; set; }

        /// <summary>
        /// SQL 서버의 이름정보를 제공합니다.
        /// </summary>
        public string SQLServerString { get; set; }

        /// <summary>
        /// SQL서버의 타입정보를 제공합니다.
        /// </summary>
        public string SQLServerType { get; set; }

        /// <summary>
        /// 실행중인 회사의 사업자 번호를 제공합니다.
        /// </summary>
        public string SQLTAXNo { get; set; }

        /// <summary>
        /// 프로그램 실행 ID정보를 제공합니다.
        /// </summary>
        public string SQLUserID { get; set; }

        /// <summary>
        /// 프로그램 실행 패스워드 정보를 제공합니다.
        /// </summary>
        public string SQLUserPassword { get; set; }

        /// <summary>
        /// SQL 인스턴스 명을 제공합니다.
        /// </summary>
        public string SQLInstanceName { get; set; }

        /// <summary>
        /// SQL 데이타베이스명을 제공합니다.
        /// </summary>
        public string SQLDatabaseName { get; set; }

        /// <summary>
        /// 3E의 SQL백업 경로정보를 제공합니다.
        /// </summary>
        public string SQLSavedPath3E { get; set; }

        /// <summary>
        /// 3E(우리)의 SQL백업 경로정보를 제공합니다.
        /// </summary>
        public string SQLSavedPath3EWOORI { get; set; }

        /// <summary>
        /// 3E의 웹 백업 여부 정보를 제공합니다.
        /// </summary>
        public bool WebBackupUse3E { get; set; }

        /// <summary>
        /// 3E(우리)의 웹 백업 여부 정보를 제공합니다.
        /// </summary>
        public bool WebBackupUse3EWOORI { get; set; }

        public HowMuch3EInfo()
        {
        }
    }

    /// <summary>
    /// 얼마에요 2E 데이터 클래스
    /// </summary>
    public class HowMuch2EInfo
    {
        /// <summary>
        /// 얼마에요2E의 제품이름을 제공합니다.
        /// </summary>
        public string ProductName { get; set; }

        /// <summary>
        /// 제품 일련번호를 제공합니다.
        /// </summary>
        public string SerialNumber { get; set; }

        /// <summary>
        /// 데이타베이스 타입을 제공합니다.
        /// </summary>
        public string DataType { get; set; }

        /// <summary>
        /// 제품 코드정보를 제공합니다.
        /// </summary>
        public string CBit { get; set; }

        /// <summary>
        /// 2E의 SQL백업 경로정보를 제공합니다.
        /// </summary>
        public string SQLSavedPath2E { get; set; }

        /// <summary>
        /// 2E(우리)의 SQL백업 경로정보를 제공합니다.
        /// </summary>
        public string SQLSavedPath2EWOORI { get; set; }

        /// <summary>
        /// 2E의 웹 백업 여부 정보를 제공합니다.
        /// </summary>
        public bool WebBackupUse2E { get; set; }

        /// <summary>
        /// 2E(우리)의 웹 백업 여부 정보를 제공합니다.
        /// </summary>
        public bool WebBackupUse2EWOORI { get; set; }

        /// <summary>
        /// 회사의 사업자번호를 설정하거나 제공합니다.
        /// </summary>
        public string CompanyNo { get; set; }

        /// <summary>
        /// SQL서버의 IP Address를 설정하거나 제공합니다.
        /// </summary>
        public string SqlIpAddress { get; set; }

        /// <summary>
        /// SQL서버의 Port를 설정하거나 제공합니다.
        /// </summary>
        public string SqlPort { get; set; }

        /// <summary>
        /// SQL서버의 Id를 설정하거나 제공합니다.
        /// </summary>
        public string SqlId { get; set; }

        /// <summary>
        /// SQL서버의 패스워드를 설정하거나 제공합니다.
        /// </summary>
        public string SqlPassword { get; set; }

        /// <summary>
        /// SQL 인스턴스 명을 제공합니다.
        /// </summary>
        public string SQLInstanceName { get; set; }

        /// <summary>
        /// SQL 데이타베이스명을 제공합니다.
        /// </summary>
        public string SQLDatabaseName { get; set; }

        public HowMuch2EInfo()
        {
        }
    }
}