﻿namespace BH_Library.Utils.SystemInfo.Memory
{
    public class MemoryInfo
    {
        /// <summary>
        /// 메모리 이름을 제공합니다.
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// 메모리 사이즈를 제공합니다.
        /// </summary>
        public long Size
        {
            get;
            set;
        }
    }
}