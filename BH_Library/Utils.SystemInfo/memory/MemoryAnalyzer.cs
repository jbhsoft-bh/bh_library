﻿using System.Collections.Generic;

namespace BH_Library.Utils.SystemInfo.Memory
{
    public class MemoryAnalyzer
    {
        private List<Result> results = HardwareInfoSearcher.SearchInfo("Win32_PhysicalMemory");

        /// <summary>
        /// 메모리 리스트를 제공합니다.
        /// </summary>
        public List<MemoryInfo> Memories
        {
            get
            {
                List<MemoryInfo> memories = new List<MemoryInfo>();

                foreach (var searchResult in results)
                {
                    MemoryInfo memoryInfo = new MemoryInfo();
                    memoryInfo.Name = searchResult.Name;
                    memoryInfo.Size = GetMemorySize(searchResult.SearchSubResults);

                    memories.Add(memoryInfo);
                }

                return memories;
            }
        }

        /// <summary>
        /// 전체 메모리 사이즈를 제공합니다.
        /// </summary>
        public long TotalMemorySize
        {
            get
            {
                long retVal = 0;
                for (int i = 0; i < Memories.Count; i++)
                {
                    retVal = retVal + Memories[i].Size;
                }
                return retVal;
            }
        }

        private long GetMemorySize(List<SubResult> subResult)
        {
            long ret = 0L;

            for (int i = 0; i < subResult.Count; i++)
            {
                if (subResult[i].Name == "Capacity")
                {
                    long val = 0L;
                    long.TryParse(subResult[i].Value, out val);
                    ret = val;
                }
            }

            return ret;
        }
    }
}