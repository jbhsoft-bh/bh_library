﻿using System;

namespace BH_Library.Utils.SystemInfo.OS
{
    /// <summary>
    /// OS 정보를 제공합니다.
    /// </summary>
    public class OsAnalyzer
    {
        /// <summary>
        /// 버젼을 제공합니다.
        /// </summary>
        public Version Version
        {
            get
            {
                return System.Environment.OSVersion.Version;
            }
        }

        /// <summary>
        /// OS 이름을 제공합니다.
        /// </summary>
        public string Name
        {
            get
            {
                return OSVersionInfo.Name;
            }
        }

        /// <summary>
        /// OS 버젼을 제공합니다.
        /// </summary>
        public string Edition
        {
            get
            {
                return OSVersionInfo.Edition;
            }
        }

        /// <summary>
        /// OS에 설치된 서비스팩 정보를 제공합니다.
        /// </summary>
        public string ServicePack
        {
            get
            {
                return OSVersionInfo.ServicePack;
            }
        }

        /// <summary>
        /// OS의 BIT구분을 제공합니다.
        /// </summary>
        public OSVersionInfo.SoftwareArchitecture Bits
        {
            get
            {
                return OSVersionInfo.OSBits;
            }
        }

        /// <summary>
        /// OS의 설치경로를 제공합니다.
        /// </summary>
        public string InstalledPath
        {
            get
            {
                return System.Environment.GetFolderPath(System.Environment.SpecialFolder.System).ToString().Replace(@"\system32", "");
            }
        }
    }
}