﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;

namespace BH_Library.Utils.SystemInfo.SqlServer
{
    /// <summary>
    /// SQL 서버 정보를 제공합니다.
    /// </summary>
    public class SqlSeverAnalyzer
    {
        public SqlSeverAnalyzer()
        {
            try
            {
                SqlServers = GetSqlServerList();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private List<SqlServerInfo> _sqlServers;

        public List<SqlServerInfo> SqlServers
        {
            get
            {
                return _sqlServers;
            }
            private set
            {
                _sqlServers = value;
            }
        }

        private List<SqlServerInfo> GetSqlServerList()
        {
            List<SqlServerInfo> list = new List<SqlServerInfo>();
            string name = "VerSpecificRootDir";

            SqlServerInfo.Platforms platform = SqlServerInfo.Platforms.Bit32;

            //레지스트리 조사(64비트 OS일 경우 이 경로가 64비트용 레지스트리)
            if (Utils.Win32API.ApiMethods.Is64Bit())
            {
                platform = SqlServerInfo.Platforms.Bit64;
            }

            RegistryKey sqlKey = RegistryKey.OpenRemoteBaseKey(RegistryHive.LocalMachine, "").OpenSubKey(@"SOFTWARE\MICROSOFT\MICROSOFT SQL SERVER\");
            RegistryKey subKey;
            RegistryKey subKey1;
            if (sqlKey != null)
            {
                subKey = sqlKey.OpenSubKey(@"90");//90은 2005와 호환을 위해
                subKey1 = sqlKey.OpenSubKey(@"90\Machines");
                if (subKey != null)
                {
                    if (subKey1 != null)
                    {
                        list.Add(new SqlServerInfo
                        {
                            Platform = platform,
                            SqlVersion = SqlServerInfo.SqlVersions.SqlServer2005,
                            InstalledDir = subKey.GetValue(name, "").ToString()
                        });
                    }
                }

                subKey = sqlKey.OpenSubKey(@"100");//100은 2008과 호환을 위해
                subKey1 = sqlKey.OpenSubKey(@"100\Machines");

                if (subKey != null)
                {
                    if (subKey1 != null)
                    {
                        list.Add(new SqlServerInfo
                        {
                            Platform = platform,
                            SqlVersion = SqlServerInfo.SqlVersions.SqlServer2008,
                            InstalledDir = subKey.GetValue(name, "").ToString()
                        });
                    }
                }

                subKey = sqlKey.OpenSubKey(@"110");//110은 2012가 사용하는 폴더
                subKey1 = sqlKey.OpenSubKey(@"110\Machines");
                if (subKey != null)
                {
                    if (subKey1 != null)
                    {
                        list.Add(new SqlServerInfo
                        {
                            Platform = platform,
                            SqlVersion = SqlServerInfo.SqlVersions.SqlServer2012,
                            InstalledDir = subKey.GetValue(name, "").ToString()
                        });
                    }
                }
            }

            //64비트 OS일 경우 32비트 레지스트리도 조사
            if (!Utils.Win32API.ApiMethods.Is64Bit())
            {
                platform = SqlServerInfo.Platforms.Bit32;
                sqlKey = RegistryKey.OpenRemoteBaseKey(RegistryHive.LocalMachine, "").OpenSubKey(@"SOFTWARE\WOW6432NODE\MICROSOFT\MICROSOFT SQL SERVER\");
                if (sqlKey != null)
                {
                    subKey = sqlKey.OpenSubKey(@"90");
                    subKey1 = sqlKey.OpenSubKey(@"90\Machines");

                    if (subKey != null)
                    {
                        if (subKey1 != null)
                        {
                            list.Add(new SqlServerInfo
                            {
                                Platform = platform,
                                SqlVersion = SqlServerInfo.SqlVersions.SqlServer2005,
                                InstalledDir = subKey.GetValue(name, "").ToString()
                            });
                        }
                    }

                    subKey = sqlKey.OpenSubKey(@"100");
                    subKey1 = sqlKey.OpenSubKey(@"100\Machines");

                    if (subKey != null)
                    {
                        if (subKey1 != null)
                        {
                            list.Add(new SqlServerInfo
                            {
                                Platform = platform,
                                SqlVersion = SqlServerInfo.SqlVersions.SqlServer2008,
                                InstalledDir = subKey.GetValue(name, "").ToString()
                            });
                        }
                    }

                    subKey = sqlKey.OpenSubKey(@"110");
                    subKey1 = sqlKey.OpenSubKey(@"110\Machines");
                    if (subKey != null)
                    {
                        if (subKey1 != null)
                        {
                            list.Add(new SqlServerInfo
                            {
                                Platform = platform,
                                SqlVersion = SqlServerInfo.SqlVersions.SqlServer2012,
                                InstalledDir = subKey.GetValue(name, "").ToString()
                            });
                        }
                    }
                }
            }
            return list;
        }

        public string GetSqlServers()
        {
            string str = string.Empty;

            foreach (var sql in SqlServers)
            {
                str += string.Format("{0}({1});", sql.SqlVersion, sql.Platform);
            }

            return str;
        }

        public string GetSSMS()
        {
            string str = string.Empty;

            foreach (var sql in SqlServers)
            {
                if (sql.InstalledSSMS)
                {
                    str += string.Format("SSMS{0};", sql.VersionString);
                }
            }

            return str;
        }
    }
}