﻿using System.IO;

namespace BH_Library.Utils.SystemInfo.SqlServer
{
    /// <summary>
    /// MS SQL 서버정보를 제공합니다.
    /// </summary>
    public class SqlServerInfo
    {
        private string _2005SSMS_Path = @"Tools\Binn\VSShell\Common7\IDE\ssms.exe";
        private string _2008SSMS_Path = @"Tools\Binn\VSShell\Common7\IDE\ssms.exe";
        private string _2012SSMS_Path = @"Tools\Binn\ManagementStudio\Ssms.exe";

        /// <summary>
        /// 플랫폼 정보를 제공합니다.
        /// </summary>
        public enum Platforms
        {
            Bit32,
            Bit64
        }

        /// <summary>
        /// SQL 버젼을 제공합니다.
        /// </summary>
        public enum SqlVersions
        {
            None,
            SqlServer2005,
            SqlServer2008,
            SqlServer2012
        }

        private Platforms _platform;

        /// <summary>
        /// SQLServer 프로그램 Bit정보를 제공합니다.
        /// </summary>
        public Platforms Platform
        {
            get
            {
                return _platform;
            }
            set
            {
                _platform = value;
            }
        }

        private SqlVersions _sqlVersion;

        /// <summary>
        /// SQLServer 프로그램 버전을 제공합니다.
        /// </summary>
        public SqlVersions SqlVersion
        {
            get
            {
                return _sqlVersion;
            }
            internal set
            {
                _sqlVersion = value;
            }
        }

        /// <summary>
        /// SQL버전을 문자열로 제공합니다.
        /// </summary>
        public string VersionString
        {
            get
            {
                string verstionString = "Unknown";

                if (SqlVersion == SqlVersions.SqlServer2005)
                {
                    verstionString = "2005";
                }
                else if (SqlVersion == SqlVersions.SqlServer2008)
                {
                    verstionString = "2008";
                }
                else if (SqlVersion == SqlVersions.SqlServer2012)
                {
                    verstionString = "2012";
                }

                return verstionString;
            }
        }

        private string _installedDir;

        /// <summary>
        /// SQLServer 설치경로 경로를 제공합니다.
        /// </summary>
        public string InstalledDir
        {
            get
            {
                return _installedDir;
            }
            set
            {
                _installedDir = value;
            }
        }

        /// <summary>
        /// SSMS 설치여부을 제공합니다.
        /// </summary>
        public bool InstalledSSMS
        {
            get
            {
                bool bRet = false;

                if (!string.IsNullOrEmpty(InstalledDir))
                {
                    string ssmsPath = SSMSInstallPath;

                    if (!string.IsNullOrEmpty(ssmsPath))
                    {
                        bRet = File.Exists(ssmsPath);
                    }
                }

                return bRet;//트루/폴스
            }
        }

        /// <summary>
        /// SSMS 설치경로을 제공합니다.
        /// </summary>
        public string SSMSInstallPath
        {
            get
            {
                string ssmsPath = GetSSMSInstallPath();

                if (!string.IsNullOrEmpty(ssmsPath))
                {
                    if (File.Exists(ssmsPath))
                    {
                        return ssmsPath;
                    }
                }

                return string.Empty;
            }
        }

        private string GetSSMSInstallPath()//버전별로 경로 추가삽입
        {
            string ssmsPath = InstalledDir;

            if (SqlVersion == SqlVersions.None)
            {
                ssmsPath = string.Empty;
            }
            else if (SqlVersion == SqlVersions.SqlServer2005)
            {
                ssmsPath += _2005SSMS_Path;
            }
            else if (SqlVersion == SqlVersions.SqlServer2008)
            {
                ssmsPath += _2008SSMS_Path;
            }
            else if (SqlVersion == SqlVersions.SqlServer2012)
            {
                ssmsPath += _2012SSMS_Path;
            }

            return ssmsPath;
        }
    }
}