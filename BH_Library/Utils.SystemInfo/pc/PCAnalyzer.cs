﻿using System;

namespace BH_Library.Utils.SystemInfo.PC
{
    /// <summary>
    /// PC정보를 제공합니다.
    /// </summary>
    public class PCAnalyzer
    {
        /// <summary>
        /// PC 정보를 제공합니다.
        /// </summary>
        public PCAnalyzer()
        {
            PCManufacturer = "Unknown";

            try
            {
                string org = (string)Microsoft.Win32.Registry.GetValue(@"HKEY_LOCAL_MACHINE\Software\Microsoft\Windows NT\CurrentVersion", "RegisteredOrganization", "");
                if (!string.IsNullOrEmpty(org))
                {
                    PCManufacturer = org;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private string _pcManufacturer;

        /// <summary>
        /// PC제조사 정보를 제공합니다.
        /// </summary>
        public string PCManufacturer
        {
            get
            {
                return _pcManufacturer;
            }
            private set
            {
                _pcManufacturer = value;
            }
        }

        /// <summary>
        /// PC의 이름을 설정하거나 제공합니다.
        /// </summary>
        public string PCName
        {
            get
            {
                return System.Environment.MachineName;
            }
        }
    }
}