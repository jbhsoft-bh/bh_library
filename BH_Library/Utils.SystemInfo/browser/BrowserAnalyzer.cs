﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;

//using System.Linq;

namespace BH_Library.Utils.SystemInfo.Browser
{
    /// <summary>
    /// 브라우져 정보를 제공합니다.
    /// </summary>
    public class BrowserAnalyzer
    {
        private string name = string.Empty;
        private string version = string.Empty;

        /// <summary>
        /// 기본 브라우저 정보를 제공합니다.
        /// </summary>
        public string GetSystemDefaultBrowser()
        {
            // string name = string.Empty;

            RegistryKey regKey = null;
            try
            {
                //set the registry key we want to open
                regKey = Registry.ClassesRoot.OpenSubKey(@"http\shell\open\command", false);

                //get rid of the enclosing quotes
                // name = regKey.GetValue(null).ToString().ToLower().Replace("" + (char)34, "");
                name = regKey.GetValue(null).ToString();//.ToLower();
                if (name.Contains("Internet Explorer") || name.Contains("Firefox"))
                {
                    if (!name.EndsWith("exe"))
                        name = name.Substring(0, name.LastIndexOf(@"\"));
                    name = name.Substring(name.LastIndexOf(@"\") + 1);
                }
                else if (name.Contains("Chrome"))
                {
                    name = name.Substring(0, name.LastIndexOf(@"\"));
                    name = name.Substring(0, name.LastIndexOf(@"\"));

                    name = name.Substring(name.LastIndexOf(@"\") + 1);
                }

                //check to see if the value ends with .exe (this way we can remove any command line arguments)
                //if (!name.EndsWith("exe"))
                //get rid of all command line arguments (anything after the .exe must go)
                // name = name.Substring(0, name.LastIndexOf(".exe") + 4);
                //name = name.Substring(0, name.LastIndexOf(".exe"));
                //name = name.Substring(0, name.LastIndexOf(@"\"));
                // name = name.Substring(name.LastIndexOf(@"\")+1);
                //internet explorer
            }
            catch (Exception ex)
            {
                name = string.Format("ERROR: An exception of type: {0} occurred in method: {1} in the following module: {2}", ex.GetType(), ex.TargetSite, this.GetType());
            }
            finally
            {
                //check and see if the key is still open, if so
                //then close it
                if (regKey != null)
                    regKey.Close();
            }
            //return the value
            return name;
        }

        public string GetBrowserVersion() //버전정보제공
        {
            RegistryKey regKey1 = null;
            //RegistryKey regKey2 = null;

            try
            {
                if (name.Contains("Internet Explorer"))
                {
                    regKey1 = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Internet Explorer", false);

                    version = regKey1.GetValue("svcVersion").ToString().ToLower();
                }
                else if (name.Contains("Chrome"))
                {
                    regKey1 = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\Google Chrome", false);
                    version = regKey1.GetValue("Version").ToString().ToLower();
                }
                else if (name.Contains("Firefox"))
                {
                    /*
                     regKey1 = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall", false);
                     var a =Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall", false).GetSubKeyNames();
                     string findThisString = "Mozilla Firefox";

                     int strNumber;
                     int strIndex = 0;
                     for (strNumber = 0; strNumber < a.Length; strNumber++)
                     {
                         strIndex = a[strNumber].IndexOf(findThisString);
                         if (strIndex >= 0)
                             break;
                     }

                     string b = a[strNumber];
                     regKey2 = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\"+b, false);
                     version = regKey2.GetValue("DisplayVersion").ToString().ToLower();

                     */

                    //version = regKey1.GetValue("DisplayVersion").ToString().ToLower();
                    //Console.WriteLine(a);
                    regKey1 = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\Mozilla\Mozilla Firefox", false);
                    version = regKey1.GetValue("CurrentVersion").ToString().ToLower();
                }
            }
            catch (Exception ex)
            {
                version = string.Format("ERROR: An exception of type: {0} occurred in method: {1} in the following module: {2}", ex.GetType(), ex.TargetSite, this.GetType());
            }
            finally
            {
                //check and see if the key is still open, if so
                //then close it
                if (regKey1 != null)
                    regKey1.Close();
            }
            return version;
        }

        public string GetBrowserVersion2() //버전정보제공 32비트
        {
            RegistryKey regKey1 = null;
            //RegistryKey regKey2 = null;

            try
            {
                if (name.Contains("Internet Explorer"))
                {
                    regKey1 = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Internet Explorer", false);

                    version = regKey1.GetValue("Version").ToString().ToLower();
                }
                else if (name.Contains("Chrome"))
                {
                    regKey1 = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Google Chrome", false);
                    version = regKey1.GetValue("Version").ToString().ToLower();
                }
                else if (name.Contains("Firefox"))
                {
                    /*
                     regKey1 = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall", false);
                     var a =Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall", false).GetSubKeyNames();
                     string findThisString = "Mozilla Firefox";

                     int strNumber;
                     int strIndex = 0;
                     for (strNumber = 0; strNumber < a.Length; strNumber++)
                     {
                         strIndex = a[strNumber].IndexOf(findThisString);
                         if (strIndex >= 0)
                             break;
                     }

                     string b = a[strNumber];
                     regKey2 = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\"+b, false);
                     version = regKey2.GetValue("DisplayVersion").ToString().ToLower();

                     */

                    //version = regKey1.GetValue("DisplayVersion").ToString().ToLower();
                    //Console.WriteLine(a);
                    regKey1 = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Mozilla\Mozilla Firefox", false);
                    version = regKey1.GetValue("CurrentVersion").ToString().ToLower();
                }
            }
            catch (Exception ex)
            {
                version = string.Format("ERROR: An exception of type: {0} occurred in method: {1} in the following module: {2}", ex.GetType(), ex.TargetSite, this.GetType());
            }
            finally
            {
                //check and see if the key is still open, if so
                //then close it
                if (regKey1 != null)
                    regKey1.Close();
            }
            return version;
        }

        /// <summary>
        /// 기본 브라우저 정보를 제공합니다.
        /// </summary>
        public List<string> GetBrowserList()
        {
            List<string> list = new List<string>();
            RegistryKey regKey3 = null;

            if (!Utils.Win32API.ApiMethods.Is64Bit())//32비트인경우
            {
                regKey3 = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Internet Explorer", false);
                if (regKey3 != null)
                {
                    list.Add("Internet Explorer");
                }

                regKey3 = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Google Chrome", false);
                if (regKey3 != null)
                {
                    list.Add("Chrome");
                }

                regKey3 = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Mozilla\Mozilla Firefox", false);

                if (regKey3 != null)
                {
                    list.Add("Firefox");
                }
            }

            if (Utils.Win32API.ApiMethods.Is64Bit())//64비트인경우
            {
                regKey3 = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Internet Explorer", false);
                if (regKey3 != null)
                {
                    list.Add("Internet Explorer");
                }

                regKey3 = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\Google Chrome", false);
                if (regKey3 != null)
                {
                    list.Add("Chrome");
                }

                regKey3 = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\Mozilla\Mozilla Firefox", false);

                if (regKey3 != null)
                {
                    list.Add("Firefox");
                }
            }

            return list;
        }

        /*
                public string GetBrowser()
                {
                    ArrayList list1 = new ArrayList();

                    RegistryKey regKey3 = null;

                    regKey3 = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Internet Explorer", false);
                    if (regKey3 != null)
                    {
                        list1.Add("Internet Explorer");
                    }

                    regKey3 = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\Google Chrome", false);
                    if (regKey3 != null)
                    {
                        list1.Add("Chrome");
                    }

                    regKey3 = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\Mozilla\Mozilla Firefox", false);

                    if (regKey3 != null)
                    {
                        list1.Add("Firefox");
                    }

                    string list2 = "";

                    for (int i = 0; i < list1.Count; i++)
                    {
                        list2 += list1[i] + " " ;
                    }

                    return list2;
                }
         */
    }
}