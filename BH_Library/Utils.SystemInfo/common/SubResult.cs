﻿namespace BH_Library.Utils.SystemInfo
{
    public class SubResult
    {
        public string Name
        {
            get;
            set;
        }

        public string Value
        {
            get;
            set;
        }
    }
}