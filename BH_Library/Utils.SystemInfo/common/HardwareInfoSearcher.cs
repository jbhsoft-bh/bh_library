﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Management;

namespace BH_Library.Utils.SystemInfo
{
    public class HardwareInfoSearcher
    {
        public static List<Result> SearchInfo(string Key)
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(string.Format("SELECT * FROM {0}", Key));
            List<Result> searchResults = new List<Result>();

            try
            {
                foreach (ManagementObject share in searcher.Get())
                {
                    Result result = new Result();
                    result.Name = share["Name"].ToString();

                    foreach (PropertyData PC in share.Properties)
                    {
                        SubResult subResult = new SubResult();

                        subResult.Name = PC.Name;

                        if (PC.Value != null && PC.Value.ToString() != "")
                        {
                            switch (PC.Value.GetType().ToString())
                            {
                                case "System.String[]":
                                    string[] str = (string[])PC.Value;

                                    string str2 = "";
                                    foreach (string st in str)
                                        str2 += st + " ";

                                    subResult.Value = str2;

                                    break;

                                case "System.UInt16[]":
                                    ushort[] shortData = (ushort[])PC.Value;

                                    string tstr2 = "";
                                    foreach (ushort st in shortData)
                                        tstr2 += st.ToString() + " ";

                                    subResult.Value = tstr2;

                                    break;

                                default:
                                    subResult.Value = PC.Value.ToString();
                                    break;
                            }
                        }

                        if (!string.IsNullOrEmpty(subResult.Value))
                        {
                            result.SearchSubResults.Add(subResult);
                        }
                    }

                    searchResults.Add(result);
                }
            }
            catch (Exception exp)
            {
                //MessageBox.Show("can't get data because of the followeing error \n" + exp.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Debug.WriteLine("can't get data because of the followeing error \n" + exp.Message);
            }

            return searchResults;
        }
    }
}