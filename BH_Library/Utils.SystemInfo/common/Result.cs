﻿using System.Collections.Generic;

namespace BH_Library.Utils.SystemInfo
{
    public class Result
    {
        private List<SubResult> searchSubResults = new List<SubResult>();

        public string Name
        {
            get;
            set;
        }

        public List<SubResult> SearchSubResults
        {
            get
            {
                return searchSubResults;
            }
            set
            {
                searchSubResults = value;
            }
        }
    }
}