﻿using System;
using System.Management;

namespace BH_Library.Utils.SystemInfo.MotherBoard
{
    public class MotherBoardAnalyzer
    {
        /// <summary>
        /// 메인보드 정보를 제공합니다.
        /// </summary>
        public MotherBoardAnalyzer()
        {
            MotherBoardSerial = string.Empty;
            try
            {
                ManagementObjectSearcher MOS = new ManagementObjectSearcher("Select * From Win32_BaseBoard");

                foreach (ManagementObject getserial in MOS.Get())
                {
                    string serial = getserial["SerialNumber"].ToString();
                    if (!string.IsNullOrEmpty(serial))
                    {
                        MotherBoardSerial = serial;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private string _motherBoardSerial;

        /// <summary>
        /// 메인보드 시리얼넘버을 제공합니다.
        /// </summary>
        public string MotherBoardSerial
        {
            get
            {
                return _motherBoardSerial;
            }
            private set
            {
                _motherBoardSerial = value;
            }
        }
    }
}