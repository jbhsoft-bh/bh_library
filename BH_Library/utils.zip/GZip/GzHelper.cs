﻿using System;
using System.IO;
using System.IO.Compression;
using System.Windows.Forms;

namespace BH_Library.Utils.Zip.GZip
{
    public static class GzHelper
    {
        public static bool Compress(FileInfo fi)
        {
            if (fi == null)
            {
                return false;
            }
            try
            {
                // Get the stream of the source file.
                using (FileStream inFile = fi.OpenRead())
                {
                    // Prevent compressing hidden and
                    // already compressed files.
                    if ((File.GetAttributes(fi.FullName) & FileAttributes.Hidden) != FileAttributes.Hidden & fi.Extension != ".gz")
                    {
                        // Create the compressed file.
                        using (FileStream outFile = File.Create(fi.FullName + ".gz"))
                        {
                            using (GZipStream Compress = new GZipStream(outFile, CompressionMode.Compress))
                            {
                                // Copy the source file into
                                // the compression stream.
                                inFile.CopyTo(Compress);

                                Console.WriteLine("Compressed {0} from {1} to {2} bytes.",
                                    fi.Name, fi.Length.ToString(), outFile.Length.ToString());
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public static bool Decompress(string fileName, string resultFileName)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                return false;
            }
            try
            {
                FileInfo fi = new FileInfo(fileName);
                return (Decompress(fi, resultFileName));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public static bool Decompress(FileInfo fi, string resultFileName)
        {
            if (fi == null)
            {
                return false;
            }
            try
            {
                //
                // Get the stream of the source file.
                //
                using (FileStream inFile = fi.OpenRead())
                {
                    // Get original file extension, for example
                    // "doc" from report.doc.gz.
                    string curFile = fi.FullName;
                    string origName = resultFileName;// curFile.Remove(curFile.Length - fi.Extension.Length);

                    //Create the decompressed file.
                    using (FileStream outFile = File.Create(origName))
                    {
                        using (GZipStream Decompress = new GZipStream(inFile, CompressionMode.Decompress))
                        {
                            // Copy the decompression stream
                            // into the output file.
                            Decompress.CopyTo(outFile);

                            Console.WriteLine("압축풀기: {0}", fi.Name);
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                string str = "에러: 압축풀기 오류가 발생했습니다. (" + ex.Message + ")";
                MessageBox.Show(str);
                return false;
            }
        }
    }
}