﻿using ICSharpCode.SharpZipLib.Zip;
using System;
using System.Collections;
using System.IO;

namespace BH_Library.Utils.Zip
{
    /// <summary>
    /// 압축 진행에 대한 상태정보의 이벤트를 제공합니다.
    /// </summary>
    /// <param name="sender">호출자</param>
    /// <param name="e">이벤트</param>
    public delegate void SharpZipProgressChangedEventHandler(object sender, SharpZipProgressChangedEventArgs e);

    /// <summary>
    /// 이벤트에 대한 파라메터를 제공합니다.
    /// </summary>
    public class SharpZipProgressChangedEventArgs : EventArgs
    {
        private long m_Count;
        private long m_TotalCounts;

        /// <summary>
        /// 압축의 진행상태에 대한 정보를 제공합니다.
        /// </summary>
        /// <param name="TatalCounts">전체 크기</param>
        /// <param name="Count">현재 진행된 크기</param>
        public SharpZipProgressChangedEventArgs(long TatalCounts, long Count)
        {
            this.m_Count = Count;
            this.m_TotalCounts = TatalCounts;
        }

        /// <summary>
        /// 완료된 크기
        /// </summary>
        public long ByteCompleted
        {
            get { return m_Count; }
        }

        /// <summary>
        /// 전체크기
        /// </summary>
        public long TotalBytes
        {
            get { return m_TotalCounts; }
        }
    }

    /// <summary>
    /// 파일 압축기능을 제공합니다.
    /// </summary>
    public class SharpZip
    {
        /// <summary>
        /// 압축진행 상태에 대한 이벤트를 제공합니다.
        /// </summary>
        public event SharpZipProgressChangedEventHandler OnProgressChanged;

        /// <summary>
        /// 생성자
        /// </summary>
        public SharpZip()
        {
        }

        /// <summary>
        /// 파일 스트림 쓰기(ZIP)을 지원합니다.
        /// </summary>
        /// <param name="source">Source</param>
        /// <param name="destination">Destination</param>
        private void ZipCopyStream(Stream source, Stream destination)
        {
            byte[] buffer = new byte[4096];
            int countBytesRead;
            long count = 0;
            long totalCount = source.Length;
            while ((countBytesRead = source.Read(buffer, 0, buffer.Length)) > 0)
            {
                count += countBytesRead;
                SharpZipProgressChangedEventArgs e = new SharpZipProgressChangedEventArgs(totalCount, count);
                ProgressChanged(e);

                destination.Write(buffer, 0, countBytesRead);
            }
        }

        /// <summary>
        /// 파일 스트림 쓰기(UNZIP)을 지원합니다.
        /// </summary>
        /// <param name="source">Source</param>
        /// <param name="destination">Destination</param>
        /// <param name="TotalCount">전체 크기</param>
        private void UnZipCopyStream(Stream source, Stream destination, long TotalCount)
        {
            byte[] buffer = new byte[4096];
            int countBytesRead;
            long count = 0;
            while ((countBytesRead = source.Read(buffer, 0, buffer.Length)) > 0)
            {
                count += countBytesRead;
                SharpZipProgressChangedEventArgs e = new SharpZipProgressChangedEventArgs(TotalCount, count);
                ProgressChanged(e);

                destination.Write(buffer, 0, countBytesRead);
            }
        }

        /// <summary>
        /// 상태가 변경될때 호출됩니다.
        /// </summary>
        /// <param name="e"></param>
        protected virtual void ProgressChanged(SharpZipProgressChangedEventArgs e)
        {
            if (OnProgressChanged != null)
            {
                OnProgressChanged(this, e);
            }
        }

        /// <summary>
        /// 파일 압축(ZIP)을 수행합니다.
        /// </summary>
        /// <param name="SrcFileList">압축할 파일 리스트</param>
        /// <param name="DstFile">저장할 파일 이름</param>
        /// <param name="Password">패스워드</param>
        /// <param name="Comment">코멘트</param>
        /// <returns></returns>
        public bool Zip(ArrayList SrcFileList, string DstFile, string Password, string Comment)
        {
            bool retVal = false;
            FileStream inputStream = null;
            FileStream outputStream = null;
            ZipOutputStream zipStream = null;
            ZipEntry zipEntry = null;
            bool isCrypted = false;

            try
            {
                outputStream = new FileStream(DstFile, FileMode.Create);
                zipStream = new ZipOutputStream(outputStream);

                if (Password != null && Password.Length > 0)
                {
                    zipStream.Password = Password;

                    isCrypted = true;
                }

                if (Comment != null && Comment.Length > 0)
                {
                    zipStream.SetComment(Comment);
                }
                foreach (string srcFile in SrcFileList)
                {
                    try
                    {
                        inputStream = new FileStream(srcFile, FileMode.Open);
                        zipEntry = new ZipEntry(Path.GetFileName(srcFile));

                        zipEntry.IsCrypted = isCrypted;
                        zipEntry.CompressionMethod = CompressionMethod.Deflated;

                        zipStream.PutNextEntry(zipEntry);
                        ZipCopyStream(inputStream, zipStream);
                    }
                    finally
                    {
                        inputStream.Close();
                        inputStream.Dispose();
                        zipStream.CloseEntry();
                    }
                }

                retVal = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                zipStream.Finish();
                outputStream.Close();
                zipStream.Close();
            }

            return retVal;
        }

        /// <summary>
        /// 파일에 대한 압축풀기(Unzip)을 수행합니다.
        /// </summary>
        /// <param name="SrcFile">압축을 풀 파일</param>
        /// <param name="DestPath">압축이 풀린 경로</param>
        /// <param name="Password">패스워드</param>
        /// <returns></returns>
        public ArrayList UnZip(string SrcFile, string DestPath, string Password)
        {
            ArrayList FileList = new ArrayList(); ;
            ZipFile zipFile = null;
            ZipEntry zipEntry = null;
            ArrayList zipFileList = null;

            try
            {
                zipFile = new ZipFile(SrcFile);
                zipFile.Password = Password;

                System.IO.Directory.CreateDirectory(DestPath);

                zipFileList = new ArrayList();

                for (int n = 0; n < zipFile.Count; n++)
                {
                    zipEntry = zipFile[n];
                    zipFileList.Add(zipFile[n].ToString());

                    if (zipEntry != null)
                    {
                        Stream inputStream = null;
                        FileStream fileStream = null;

                        try
                        {
                            try
                            {
                                inputStream = zipFile.GetInputStream(zipEntry);
                            }
                            catch (Exception e1)
                            {
                                BH_Library.Utils.Log.BhLogManager.Error(
                                    string.Format("{0}.{1}", System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.ToString(),
                                    System.Reflection.MethodBase.GetCurrentMethod().Name),
                                    e1.Message);
                                throw new Exception("패스워드가 정확하지 않습니다");
                            }
                            fileStream = new FileStream(Path.Combine(DestPath, zipEntry.Name), FileMode.Create);

                            UnZipCopyStream(inputStream, fileStream, zipEntry.Size);
                        }
                        catch (Exception e2)
                        {
                            throw new Exception(e2.Message);
                        }
                        finally
                        {
                            FileList.Add(zipEntry.Name.ToString());

                            if (fileStream != null) fileStream.Close();
                            if (inputStream != null) inputStream.Close();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                if (zipFile != null) zipFile.Close();
                if (zipFileList != null) zipFileList.Clear();
            }

            return FileList;
        }

        /// <summary>
        /// 패스워드 존재여부 및 패스워드를 확인합니다.
        /// </summary>
        /// <param name="SrcFile">압축파일 경로</param>
        /// <param name="Password">패스워드</param>
        /// <returns>패스워드 진위 여부</returns>
        public bool ExitsPassword(string SrcFile, string Password)
        {
            bool retVal = false;
            ArrayList FileList = new ArrayList(); ;
            ZipFile zipFile = null;
            ZipEntry zipEntry = null;

            try
            {
                zipFile = new ZipFile(SrcFile);
                zipFile.Password = Password;

                for (int n = 0; n < zipFile.Count; n++)
                {
                    zipEntry = zipFile[n];

                    if (zipEntry != null)
                    {
                        Stream inputStream = null;
                        FileStream fileStream = null;

                        try
                        {
                            try
                            {
                                inputStream = zipFile.GetInputStream(zipEntry);

                                retVal = false;
                            }
                            catch (Exception e3)
                            {
                                BH_Library.Utils.Log.BhLogManager.Debug(
                                    string.Format("{0}.{1}", System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.ToString(),
                                    System.Reflection.MethodBase.GetCurrentMethod().Name),
                                    e3.Message);
                                retVal = true;
                            }
                        }
                        catch (Exception e2)
                        {
                            throw new Exception(e2.Message);
                        }
                        finally
                        {
                            if (fileStream != null) fileStream.Close();
                            if (inputStream != null) inputStream.Close();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                if (zipFile != null) zipFile.Close();
            }

            return retVal;
        }

        /// <summary>
        /// 압축된 파일에 들어있는 코멘트를 가져옵니다.
        /// </summary>
        /// <param name="SrcFile">압축파일 경로</param>
        /// <param name="Password">패스워드</param>
        /// <param name="Comment">코멘트</param>
        /// <returns>코멘트 존배 여부</returns>
        public bool GetComment(string SrcFile, string Password, ref string Comment)
        {
            bool retVal = true;
            ArrayList FileList = new ArrayList(); ;
            ZipFile zipFile = null;
            ZipEntry zipEntry = null;

            try
            {
                zipFile = new ZipFile(SrcFile);
                zipFile.Password = Password;
                Comment = zipFile.ZipFileComment;

                for (int n = 0; n < zipFile.Count; n++)
                {
                    zipEntry = zipFile[n];

                    if (zipEntry != null)
                    {
                        Stream inputStream = null;
                        FileStream fileStream = null;

                        try
                        {
                            try
                            {
                                inputStream = zipFile.GetInputStream(zipEntry);
                                retVal = true;
                            }
                            catch (Exception e3)
                            {
                                BH_Library.Utils.Log.BhLogManager.Debug(
                                    string.Format("{0}.{1}", System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.ToString(),
                                    System.Reflection.MethodBase.GetCurrentMethod().Name),
                                    e3.Message);
                                retVal = false;
                            }
                        }
                        catch (Exception e2)
                        {
                            throw new Exception(e2.Message);
                        }
                        finally
                        {
                            if (fileStream != null) fileStream.Close();
                            if (inputStream != null) inputStream.Close();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                if (zipFile != null) zipFile.Close();
            }

            return retVal;
        }
    }
}