﻿using System;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;

namespace BH_Library.Utils.Win32API
{
    /// <summary>
    /// Win32Api 함수들을 제공하는 래퍼 클래스입니다.
    /// </summary>
    public class ApiMethods
    {
        /// <summary>
        /// 윈도우 클래스와 캡션으로 윈도우를 검색하여 핸들을 얻어옵니다.
        /// </summary>
        /// <param name="lpClassName">검색에 사용될 윈도우 클래스 이름.</param>
        /// <param name="lpWindowName">검색에 사용할 윈도우의 캡션 문자열</param>
        /// <returns>검색한 윈도우의 핸들을 리턴합니다. 실패시 null을 리턴합니다.</returns>
        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        /// <summary>
        /// 지정한 차일드 윈도우의 부모 윈도우를 변경합니다.
        /// </summary>
        /// <param name="hWndChild">차일드 윈도우의 핸들입니다.</param>
        /// <param name="hWndNewParent">새로운 부모 윈도우의 핸들입니다.</param>
        /// <returns>변경에 성공하면 이전 부모윈도우의 핸들을 리턴합니다. 실패시 null을 리턴합니다.</returns>
        [DllImport("user32.dll", SetLastError = true)]
        public static extern long SetParent(IntPtr hWndChild, IntPtr hWndNewParent);

        /// <summary>
        /// CreateWindow(Ex)함수로 윈도우를 생성할 때 지정했던 윈도우의 속성을 조사합니다.
        /// </summary>
        /// <param name="hwnd">속성을 조사하고자 하는 윈도우의 핸들</param>
        /// <param name="nIndex">조사하고자 하는 속성을 지정</param>
        /// <returns></returns>
        [DllImport("user32.dll", EntryPoint = "GetWindowLongA", SetLastError = true)]
        public static extern long GetWindowLong(IntPtr hwnd, int nIndex);

        /// <summary>
        /// 윈도우의 속성을 변경합니다.
        /// </summary>
        /// <param name="hwnd">속성을 변경하고자 하는 윈도우의 핸들입니다.</param>
        /// <param name="nIndex">변경하고자 하는 속성을 지정합니다.</param>
        /// <param name="dwNewLong">새고 변경할 32비트 값이며 nIndex에 따라 값의 의미가 달라집니다.</param>
        /// <returns>성공시 이전에 설정되어있던 32비트 값을 리턴하며, 값이 설정되어 있지 않거나 에러가 발생하면 0을 리턴합니다.</returns>
        [DllImport("user32.dll", EntryPoint = "SetWindowLongA", SetLastError = true)]
        public static extern long SetWindowLong(IntPtr hwnd, int nIndex, long dwNewLong);

        /// <summary>
        /// 윈도우의 위치, 크기, Z순서를 동시에 또는 일부만 변경할 때 사용됩니다.
        /// </summary>
        /// <param name="hwnd">이동 대상 윈도우의 핸들입니다.</param>
        /// <param name="hWndInsertAfter">윈도우의 Z순서를 지정합니다.</param>
        /// <param name="x">윈도우의 새로운 좌상단 좌표를 지정합니다.</param>
        /// <param name="y">윈도우의 새로운 좌상단 좌표를 지정합니다.</param>
        /// <param name="cx">윈도우의 새로운 폭을 지정합니다.</param>
        /// <param name="cy">윈도우의 새로운 높이를 지정합니다.</param>
        /// <param name="wFlags">위치와 크기 변경에 대한 여러가지 옵션들이며 플래그들을 조합하여 지정할 수 있습니다.</param>
        /// <returns>성공시 0이 아닌 값을 리턴하며, 에러 발생시 0을 리턴합니다.</returns>
        [DllImport("user32.dll", SetLastError = true)]
        public static extern long SetWindowPos(IntPtr hwnd, long hWndInsertAfter, long x, long y, long cx, long cy, long wFlags);

        /// <summary>
        /// 윈도우의 위치와 크기를 변경합니다.
        /// </summary>
        /// <param name="hwnd"> 이동 대상 윈도우의 핸들입니다.</param>
        /// <param name="x">이동할 좌상단 좌표를 지정합니다.</param>
        /// <param name="y">이동할 좌상단 좌표를 지정합니다.</param>
        /// <param name="cx">윈도우의 폭을 지정합니다.</param>
        /// <param name="cy">윈도우의 높이를 지정합니다.</param>
        /// <param name="repaint">이동후 윈도우를 다시 그릴지 여부를 지정합니다.</param>
        /// <returns>성공하면 0이 아닌 값을 리턴하며 에러 발생시 0을 리턴합니다.</returns>
        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool MoveWindow(IntPtr hwnd, int x, int y, int cx, int cy, bool repaint);

        /// <summary>
        /// 윈도우의 보이기 상태를 지정합니다.
        /// </summary>
        /// <param name="hWnd">대상 윈도우 핸들</param>
        /// <param name="nCmdShow">지정하고자 하는 보이기 상태입니다.</param>
        /// <returns>윈도우가 이전에 보이는 상태였다면 0이 아닌 값을 리턴하며, 보이지 않는 상태였다면 0을 리턴합니다.</returns>
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool ShowWindow(IntPtr hWnd, ApiArguments.ShowWindowCommands nCmdShow);

        /// <summary>
        /// 지정한 윈도우의 프로세스ID를 얻어옵니다.
        /// </summary>
        /// <param name="hWnd">윈도우의 핸들입니다.</param>
        /// <param name="ProcessId">프로세스ID를 얻어올 포인터입니다.</param>
        /// <returns>생성된 윈도우 스레드의 ID를 리턴합니다.</returns>
        [DllImport("user32.dll")]
        public static extern IntPtr GetWindowThreadProcessId(IntPtr hWnd, IntPtr ProcessId);

        /// <summary>
        /// 지정된 INI 파일의 섹션으로 문자열을 복사합니다.
        /// </summary>
        /// <param name="section">문자열이 복사될 섹션의 이름입니다.</param>
        /// <param name="key">키의 이름입니다.</param>
        /// <param name="val">복사할 문자열입니다.</param>
        /// <param name="filePath">ini파일의 이름입니다.</param>
        /// <returns>실행에 성공하면 0이 아닌 값이 리턴됩니다.</returns>
        [DllImport("kernel32")]
        public static extern long WritePrivateProfileString(string section, string key, string val, string filePath);

        /// <summary>
        /// INI파일의 지정된 섹션으로 부터 문자열을 얻어옵니다.
        /// </summary>
        /// <param name="section">키 이름을 포함한 섹션의 이름입니다.</param>
        /// <param name="key">키 이름입니다.</param>
        /// <param name="def">디폴트 문자열입니다.</param>
        /// <param name="retVal">문자열을 받을 Buffer입니다.</param>
        /// <param name="size">Buffer의 크기입니다.</param>
        /// <param name="filePath">ini파일의 이름입니다.</param>
        /// <returns>Buffer에 복사된 문자의 수가 리턴됩니다.</returns>
        [DllImport("kernel32")]
        public static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);

        /// <summary>
        /// 작업이 완료되는 것을 기다리지 않고, 윈도우의 보이기 상태를 변경합니다.
        /// </summary>
        /// <param name="hWnd">윈도우의 핸들입니다.</param>
        /// <param name="nCmdShow">윈도우의 보이기 상태를 지정합니다.</param>
        /// <returns>실행에 성공하면 0이 아닌 값이 리턴됩니다.</returns>
        [DllImport("user32.dll")]
        public static extern bool ShowWindowAsync(IntPtr hWnd, ApiArguments.ShowWindowCommands nCmdShow);

        /// <summary>
        /// 지정된 윈도우의 부모 윈도우의 핸들을 얻어옵니다.
        /// </summary>
        /// <param name="hWnd">부모 윈도우를 얻어올 윈도우의 핸들입니다.</param>
        /// <returns>실행에 성공하면 부모윈도우의 핸들을 리턴하며, 실패시 null을 리턴합니다.</returns>
        [DllImport("user32.dll", ExactSpelling = true, CharSet = CharSet.Auto)]
        public static extern IntPtr GetParent(IntPtr hWnd);

        /// <summary>
        /// 메시지를 해당 윈도우의 메시지 큐에 붙입니다.
        /// </summary>
        /// <param name="hwnd">메시지를 받을 윈도우의 핸들입니다.</param>
        /// <param name="Msg">붙일 메시지를 지정합니다.</param>
        /// <param name="wParam">메시지의 추가정보입니다.</param>
        /// <param name="lParam">메시지의 추가정보입니다.</param>
        /// <returns>메시지 붙이기에 성공하면 0이 아닌 값을, 실패시 0을 리턴합니다.</returns>
        [DllImport("user32.dll", EntryPoint = "PostMessageA", SetLastError = true)]
        public static extern bool PostMessage(IntPtr hwnd, int Msg, long wParam, long lParam);

        /// <summary>
        /// 메시지를 윈도우에 보냅니다. 메시지가 완전히 처리되기 전에는 리턴하지 않습니다.
        /// </summary>
        /// <param name="hWnd">메시지를 받을 윈도우의 핸들입니다.</param>
        /// <param name="Msg">전달할 메시지입니다.</param>
        /// <param name="wParam">메시지의 추가정보입니다.</param>
        /// <param name="lParam">메시지의 추가정보입니다.</param>
        /// <returns>메시지를 처리한 결과가 리턴됩니다.</returns>
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr SendMessage(IntPtr hWnd, int Msg, IntPtr wParam, IntPtr lParam);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr SendMessage(IntPtr hWnd, int Msg, IntPtr wParam, ref BH_Library.Utils.Win32API.ApiArguments.COPYDATASTRUCT lParam);

        [DllImport("kernel32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern IntPtr GetProcAddress(IntPtr hModule, string methodName);

        [ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail), DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr GetModuleHandle(string moduleName);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        internal static extern IntPtr GetCurrentProcess();

        [SecurityCritical]
        internal static bool DoesWin32MethodExist(string moduleName, string methodName)
        {
            IntPtr moduleHandle = GetModuleHandle(moduleName);
            if (moduleHandle == IntPtr.Zero)
            {
                return false;
            }
            return (GetProcAddress(moduleHandle, methodName) != IntPtr.Zero);
        }

        [return: MarshalAs(UnmanagedType.Bool)]
        [DllImport("kernel32.dll", SetLastError = true)]
        internal static extern bool IsWow64Process([In] IntPtr hSourceProcessHandle, [MarshalAs(UnmanagedType.Bool)] out bool isWow64);

        /// <summary>
        /// OS의 BIT구분이 64비트인지여부를 제공합니다.
        /// </summary>
        /// <returns>64비트인지 여부</returns>
        [SecuritySafeCritical]
        public static bool Is64Bit()
        {
            bool flag;
            return (IntPtr.Size == 8) ||
                ((DoesWin32MethodExist("kernel32.dll", "IsWow64Process") &&
                IsWow64Process(GetCurrentProcess(), out flag)) && flag);
        }
    }
}