﻿using Microsoft.Win32;
using System;

namespace BH_Library.Utils.Register
{
    public static class Registry
    {
        /// <summary>
        /// HKEY_CURRENT_USER 레지스터 정보를 읽을수 있는 기능을 제공합니다.
        /// </summary>
        /// <param name="Path">HKEY_CURRENT_USER 하위 경로</param>
        /// <param name="Name">이름</param>
        /// <param name="DefaultValue">디폴트 갑</param>
        /// <returns></returns>
        public static string CurrentUserRead(string Path, string Name, string DefaultValue)
        {
            object value = null;
            try
            {
                value = RegistryHelper.GetKeyValue(RegistryHive.CurrentUser, string.Empty, Path, Name);
                if (value == null)
                {
                    if (value == null) value = DefaultValue;
                }
            }
            catch (Exception ex)
            {
                value = DefaultValue;
                Console.WriteLine(ex.Message);
            }
            return value.ToString();
        }

        /// <summary>
        /// HKEY_CURRENT_USER 레지스터 정보를 쓸수있는 있는 기능을 제공합니다.
        /// </summary>
        /// <param name="Path">HKEY_CURRENT_USER 하위 경로</param>
        /// <param name="Name">이름</param>
        /// <param name="DefaultValue">디폴트 갑</param>
        public static void CurrentUserWrite(string Path, string Name, string DefaultValue)
        {
            try
            {
                RegistryHelper.SaveSetting(RegistryHive.CurrentUser, Path, Name, DefaultValue);
            }
            catch (Exception ex)
            {
                // Skip Error
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// HKEY_CURRENT_USER 레지스터 정보를 읽을수 있는 기능을 제공합니다.
        /// </summary>
        /// <param name="ExcuteProcess"></param>
        /// <param name="Path"></param>
        /// <param name="Name"></param>
        /// <param name="DefaultValue"></param>
        /// <returns></returns>
        public static string UserRead(string ExcuteProcess, string Path, string Name, string DefaultValue)
        {
            object value = null;
            string retVal = string.Empty;
            string key = "";
            try
            {
                // Explorer 프로세스의 레지스터 검색
                key = String.Format("{0}\\{1}", BH_Library.Utils.Register.RegistryHelper.CurrentUserSid(ExcuteProcess), Path);

                value = RegistryHelper.GetKeyValue(RegistryHive.Users, string.Empty, key, Name);

                if (value == null)
                {
                    if (value == null) value = DefaultValue;
                }
            }
            catch (Exception ex)
            {
                value = DefaultValue;
                Console.WriteLine(ex.Message);
                // Skip Error
            }
            if (value != null) retVal = value.ToString();

            return retVal;
        }

        /// <summary>
        /// HKEY_CURRENT_USER 레지스터 정보를 쓸수있는 있는 기능을 제공합니다.
        /// </summary>
        /// <param name="ExcuteProcess">실행중인 파일명</param>
        /// <param name="Path">레지스터 경로</param>
        /// <param name="Name">레지스터 이름</param>
        /// <param name="DefaultValue"></param>
        public static void UserWrite(string ExcuteProcess, string Path, string Name, string DefaultValue)
        {
            try
            {
                string key = String.Format("{0}\\{1}", BH_Library.Utils.Register.RegistryHelper.CurrentUserSid(ExcuteProcess), Path);

                RegistryHelper.SaveSetting(RegistryHive.Users, key, Name, DefaultValue);
            }
            catch (Exception ex)
            {
                // Skip Error
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// HKEY_LOCAL_MACHINE 레지스터 정보를 읽을수 있는 기능을 제공합니다.
        /// </summary>
        /// <param name="Path">HKEY_LOCAL_MACHINE 하위 경로</param>
        /// <param name="Name">이름</param>
        /// <param name="DefaultValue">디폴트 갑</param>
        /// <returns></returns>
        public static string LocalMachineRead(string Path, string Name, string DefaultValue)
        {
            object value = null;
            string retVal = string.Empty;
            try
            {
                string Path64 = Path.Replace("SOFTWARE", @"SOFTWARE\Wow6432Node");

                if (Utils.Win32API.ApiMethods.Is64Bit())
                {
                    value = RegistryHelper.GetKeyValue(RegistryHive.LocalMachine, string.Empty, Path64, Name);
                }
                else
                {
                    value = RegistryHelper.GetKeyValue(RegistryHive.LocalMachine, string.Empty, Path, Name);
                }

                if (value == null)
                {
                    value = DefaultValue;
                }
            }
            catch (Exception ex)
            {
                // Skip Error
                value = DefaultValue;
                Console.WriteLine(ex.Message);
            }

            if (value != null) retVal = value.ToString();

            return retVal;
        }

        /// <summary>
        /// HKEY_LOCAL_MACHINE 레지스터 정보를 쓸수있는 있는 기능을 제공합니다.
        /// </summary>
        /// <param name="Path">HKEY_LOCAL_MACHINE 하위 경로</param>
        /// <param name="Name">이름</param>
        /// <param name="DefaultValue">디폴트 갑</param>
        public static void LocalMachineWrite(string Path, string Name, string DefaultValue)
        {
            try
            {
                string Path64 = Path.Replace("SOFTWARE", @"SOFTWARE\Wow6432Node");

                if (Utils.Win32API.ApiMethods.Is64Bit())
                {
                    // 64bit 기록
                    RegistryHelper.SaveSetting(RegistryHive.LocalMachine, Path64, Name, DefaultValue);
                }
                else
                {
                    RegistryHelper.SaveSetting(RegistryHive.LocalMachine, Path, Name, DefaultValue);
                }
            }
            catch (Exception ex)
            {
                // Skip Error
                Console.WriteLine(ex.Message);
            }
        }
    }
}