﻿using System.Collections.Generic;
using System.Threading;

namespace Hik.Collections
{
    /// <summary>
    /// This class is used to store key-value based items in a thread safe manner.
    /// It uses System.Collections.Generic.SortedList internally.
    /// </summary>
    /// <typeparam name="TK">Key type</typeparam>
    /// <typeparam name="TV">Value type</typeparam>
    public class ThreadSafeSortedList<TK, TV>
    {
        /// <summary>
        /// Gets/adds/replaces an item by key.
        /// </summary>
        /// <param name="key">Key to get/set value</param>
        /// <returns>Item associated with this key</returns>
        public TV this[TK key]
        {
            get
            {
                m_lock.EnterReadLock();
                try
                {
                    return m_items.ContainsKey(key) ? m_items[key] : default(TV);
                }
                finally
                {
                    m_lock.ExitReadLock();
                }
            }

            set
            {
                m_lock.EnterWriteLock();
                try
                {
                    m_items[key] = value;
                }
                finally
                {
                    m_lock.ExitWriteLock();
                }
            }
        }

        /// <summary>
        /// Gets count of items in the collection.
        /// </summary>
        public int Count
        {
            get
            {
                m_lock.EnterReadLock();
                try
                {
                    return m_items.Count;
                }
                finally
                {
                    m_lock.ExitReadLock();
                }
            }
        }

        /// <summary>
        /// Internal collection to store items.
        /// </summary>
        protected readonly SortedList<TK, TV> m_items;

        /// <summary>
        /// Used to synchronize access to _items list.
        /// </summary>
        protected readonly ReaderWriterLockSlim m_lock;

        /// <summary>
        /// Creates a new ThreadSafeSortedList object.
        /// </summary>
        public ThreadSafeSortedList()
        {
            m_items = new SortedList<TK, TV>();
            m_lock = new ReaderWriterLockSlim(LockRecursionPolicy.NoRecursion);
        }

        /// <summary>
        /// Checks if collection contains spesified key.
        /// </summary>
        /// <param name="key">Key to check</param>
        /// <returns>True; if collection contains given key</returns>
        public bool ContainsKey(TK key)
        {
            m_lock.EnterReadLock();
            try
            {
                return m_items.ContainsKey(key);
            }
            finally
            {
                m_lock.ExitReadLock();
            }
        }

        /// <summary>
        /// Checks if collection contains spesified item.
        /// </summary>
        /// <param name="item">Item to check</param>
        /// <returns>True; if collection contains given item</returns>
        public bool ContainsValue(TV item)
        {
            m_lock.EnterReadLock();
            try
            {
                return m_items.ContainsValue(item);
            }
            finally
            {
                m_lock.ExitReadLock();
            }
        }

        /// <summary>
        /// Removes an item from collection.
        /// </summary>
        /// <param name="key">Key of item to remove</param>
        public bool Remove(TK key)
        {
            m_lock.EnterWriteLock();
            try
            {
                if (!m_items.ContainsKey(key))
                {
                    return false;
                }

                m_items.Remove(key);
                return true;
            }
            finally
            {
                m_lock.ExitWriteLock();
            }
        }

        /// <summary>
        /// Gets all items in collection.
        /// </summary>
        /// <returns>Item list</returns>
        public List<TV> GetAllItems()
        {
            m_lock.EnterReadLock();
            try
            {
                return new List<TV>(m_items.Values);
            }
            finally
            {
                m_lock.ExitReadLock();
            }
        }

        /// <summary>
        /// Removes all items from list.
        /// </summary>
        public void ClearAll()
        {
            m_lock.EnterWriteLock();
            try
            {
                m_items.Clear();
            }
            finally
            {
                m_lock.ExitWriteLock();
            }
        }

        /// <summary>
        /// Gets then removes all items in collection.
        /// </summary>
        /// <returns>Item list</returns>
        public List<TV> GetAndClearAllItems()
        {
            m_lock.EnterWriteLock();
            try
            {
                var list = new List<TV>(m_items.Values);
                m_items.Clear();
                return list;
            }
            finally
            {
                m_lock.ExitWriteLock();
            }
        }
    }
}