﻿using Hik.Communication.Scs.Client;
using Hik.Communication.Scs.Communication.EndPoints.Tcp;
using Hik.Communication.Scs.Communication.Messages;
using Hik.Communication.Scs.Server;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace BH_Library.Utils.Socket
{
    /// <summary>
    /// Event 정보를 제공합니다.
    /// </summary>
    public class EventInfo
    {
        /// <summary>
        /// Event의 상태정보를 제공합니다.
        /// </summary>
        public enum StatusType { NONE, CONNECTED, DISCONNECTED, MESSAGERECEIVED, MESSAGESENT };

        private StatusType m_Status = StatusType.NONE;
        private Object m_Sender = null;
        private Object m_EventArgs = null;

        /// <summary>
        /// EvnetInfo의 생성자를 제공합니다.
        /// </summary>
        public EventInfo() { }

        /// <summary>
        /// 이벤트의 상태정보를 제공합니다.
        /// </summary>
        public StatusType Status
        {
            get { return this.m_Status; }
            set { this.m_Status = value; }
        }

        /// <summary>
        /// 이벤트를 보낸 객체정보를 제공합니다.
        /// </summary>
        public Object Sender
        {
            get { return this.m_Sender; }
            set { this.m_Sender = value; }
        }

        /// <summary>
        /// 이벤트의 파라메터 정보를 제공합니다.
        /// </summary>
        public Object EventArgs
        {
            get { return this.m_EventArgs; }
            set { this.m_EventArgs = value; }
        }
    }

    #region TCP Client Event

    /// <summary>
    /// TCP 클라이언트가 서버에 접속할때의 이벤트를 제공합니다.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void TcpClientConnectedEventHandler(object sender, TcpClientConnectedEventArgs e);

    /// <summary>
    /// TCP 클라이언트가 서버에 접속이 종료될때의 이벤트를 제공합니다.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void TcpClientDisConnectedEventHandler(object sender, TcpClientDisConnectedEventArgs e);

    /// <summary>
    /// TCP 클라이언트가 메세지를 받을때의 이벤트를 제공합니다.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void TcpClientMessageReceivedEventHandler(object sender, TcpClientMessageReceivedEventArgs e);

    /// <summary>
    /// TCP 클라이언트가 메세지를 보냈을때의 이벤트를 제공합니다.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void TcpClientMessageSentEventHandler(object sender, TcpClientMessageSentEventArgs e);

    /// <summary>
    /// TCP 클라이언트가 메세지를 보낼때의 이벤트를 제공합니다.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    //public delegate void TcpClientMessageSentEventHandler(object sender, TcpClientMessageSentEventArgs e);

    /// <summary>
    /// TCP 클라이언트가 서버접속시의 이벤트 파라메터를 제공합니다.
    /// </summary>
    public class TcpClientConnectedEventArgs : EventArgs
    {
        /// <summary>
        /// 생성자를 제공합니다.
        /// </summary>
        /// <param name="Client"></param>
        public TcpClientConnectedEventArgs() { }
    }

    /// <summary>
    /// TCP서버에 클라이언트가 접속시 발생하는 이벤트 파라메터를 제공합니다.
    /// </summary>
    public class TcpClientDisConnectedEventArgs : EventArgs
    {
        /// <summary>
        /// 생성자를 제공합니다.
        /// </summary>
        /// <param name="Client"></param>
        public TcpClientDisConnectedEventArgs() { }
    }

    /// <summary>
    /// TCP클라이언트에서 메세지를 수신할때 이벤트 파라메터를 제공합니다.
    /// </summary>
    public class TcpClientMessageReceivedEventArgs : EventArgs
    {
        private IScsMessage m_Message;

        /// <summary>
        /// 생성자를 제공합니다.
        /// </summary>
        /// <param name="Client"></param>
        public TcpClientMessageReceivedEventArgs(IScsMessage Message)
        {
            this.m_Message = Message;
        }

        /// <summary>
        /// TCP클라이언트에서 수신한 메세지 객체를 제공합니다.
        /// </summary>
        public IScsMessage Message
        {
            get { return m_Message; }
        }

        /// <summary>
        /// TCP클라이언트에서 수신한 메세지 텍스트를 제공합니다.
        /// </summary>
        public string MessageText
        {
            get
            {
                string retVal = string.Empty;
                var message = m_Message as ScsTextMessage; //Server only accepts text messages
                if (message != null) retVal = message.Text;
                return retVal;
            }
        }
    }

    /// <summary>
    /// TCP클라이언트에서 메세지를 보냈을때의 이벤트 파라메터를 제공합니다.
    /// </summary>
    public class TcpClientMessageSentEventArgs : EventArgs
    {
        private IScsMessage m_Message;

        /// <summary>
        /// 생성자를 제공합니다.
        /// </summary>
        /// <param name="Client"></param>
        public TcpClientMessageSentEventArgs(IScsMessage Message)
        {
            this.m_Message = Message;
        }

        /// <summary>
        /// TCP클라이언트에서 보낸 메세지 객체를 제공합니다.
        /// </summary>
        public IScsMessage Message
        {
            get { return m_Message; }
        }

        /// <summary>
        /// TCP클라이언트에서 보낸 메세지 텍스트를 제공합니다.
        /// </summary>
        public string MessageText
        {
            get
            {
                string retVal = string.Empty;
                var message = m_Message as ScsTextMessage; //Server only accepts text messages
                if (message != null) retVal = message.Text;
                return retVal;
            }
        }
    }

    #endregion TCP Client Event

    #region TCP Server Event

    /// <summary>
    /// TCP서버에 클라이언트가 접속시 발생하는 이벤트를 제공합니다.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void TcpServerClientConnectedEventHandler(object sender, TcpServerClientConnectedEventArgs e);

    /// <summary>
    /// TCP서버에 클라이언트가 접속종료시 발생하는 이벤트를 제공합니다.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void TcpServerClientDisConnectedEventHandler(object sender, TcpServerClientDisConnectedEventArgs e);

    /// <summary>
    /// TCP서버에서 메세지를 수신할때 이벤트를 제공합니다.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void TcpServerMessageReceivedEventHandler(object sender, TcpServerMessageReceivedEventArgs e);

    /// <summary>
    /// TCP서버에 클라이언트가 접속시 발생하는 이벤트 파라메터를 제공합니다.
    /// </summary>
    public class TcpServerClientConnectedEventArgs : EventArgs
    {
        private IScsServerClient m_Client;

        /// <summary>
        /// 생성자를 제공합니다.
        /// </summary>
        /// <param name="Client"></param>
        public TcpServerClientConnectedEventArgs(IScsServerClient Client)
        {
            this.m_Client = Client;
        }

        /// <summary>
        /// TCP서버에 접속한 클라이언트 정보를 제공합니다.
        /// </summary>
        public IScsServerClient Client
        {
            get { return m_Client; }
        }
    }

    /// <summary>
    /// TCP서버에 클라이언트가 접속시 발생하는 이벤트 파라메터를 제공합니다.
    /// </summary>
    public class TcpServerClientDisConnectedEventArgs : EventArgs
    {
        private IScsServerClient m_Client;

        /// <summary>
        /// 생성자를 제공합니다.
        /// </summary>
        /// <param name="Client"></param>
        public TcpServerClientDisConnectedEventArgs(IScsServerClient Client)
        {
            this.m_Client = Client;
        }

        /// <summary>
        /// TCP서버에 접속한 클라이언트 정보를 제공합니다.
        /// </summary>
        public IScsServerClient Client
        {
            get { return m_Client; }
        }
    }

    /// <summary>
    /// TCP서버에서 메세지를 수신할때 이벤트 파라메터를 제공합니다.
    /// </summary>
    public class TcpServerMessageReceivedEventArgs : EventArgs
    {
        private IScsServerClient m_Client;
        private IScsMessage m_Message;

        /// <summary>
        /// 생성자를 제공합니다.
        /// </summary>
        /// <param name="Client"></param>
        public TcpServerMessageReceivedEventArgs(IScsServerClient Client, IScsMessage Message)
        {
            this.m_Client = Client;
            this.m_Message = Message;
        }

        /// <summary>
        /// TCP서버에 접속한 클라이언트 정보를 제공합니다.
        /// </summary>
        public IScsServerClient Client
        {
            get { return m_Client; }
        }

        /// <summary>
        /// TCP서버에서 수신한 메세지 객체를 제공합니다.
        /// </summary>
        public IScsMessage Message
        {
            get { return m_Message; }
        }

        /// <summary>
        /// TCP서버에서 수신한 메세지 텍스트를 제공합니다.
        /// </summary>
        public string MessageText
        {
            get
            {
                string retVal = string.Empty;
                var message = m_Message as ScsTextMessage; //Server only accepts text messages
                if (message != null) retVal = message.Text;
                return retVal;
            }
        }

        /// <summary>
        /// TCP서버의 클라이언트에게 응답메세지를 보내는 기능을 제공합니다.
        /// </summary>
        /// <param name="Message"></param>
        /// <returns></returns>
        public bool ReplyMessage(string Message)
        {
            bool retVal = true;
            try
            {
                var client = (IScsServerClient)m_Client;
                client.SendMessage(new ScsTextMessage(Message, m_Message.MessageId));
            }
            catch (Exception ex)
            {
                retVal = false;
                Console.WriteLine(ex.Message);
            }

            return retVal;
        }
    }

    #endregion TCP Server Event

    /// <summary>
    /// TCP 서버를 제공합니다.
    /// </summary>
    public class TcpServer
    {
        /// <summary>
        /// TCP 서버에 클라이언트가 접속시 발생하는 이벤트를 제공합니다.
        /// </summary>
        public event TcpServerClientConnectedEventHandler OnClientConnected;

        /// <summary>
        /// TCP 서버에 클라이언트가 접속종료시 발생하는 이벤트를 제공합니다.
        /// </summary>
        public event TcpServerClientDisConnectedEventHandler OnClientDisConnected;

        /// <summary>
        /// TCP 서버에 메세지를 수신할때의 이벤트를 제공합니다.
        /// </summary>
        public event TcpServerMessageReceivedEventHandler OnMessageReceived;

        private TcpServerClientConnectedEventArgs m_ClientConnectedEvent = null;
        private TcpServerClientDisConnectedEventArgs m_ClientDisConnectedEvent = null;
        private TcpServerMessageReceivedEventArgs m_MessageReceivedEvent = null;

        private Queue<EventInfo> m_EventList = new Queue<EventInfo>();

        //private System.Windows.Forms.Timer m_WinTimer = null;
        private System.Timers.Timer m_Timer = null;

        private Hik.Communication.Scs.Server.IScsServer m_Server = null;

        /// <summary>
        /// 생성자를 제공합니다.
        /// </summary>
        /// <param name="Port">포트정보</param>
        /*
        public TcpServer(int Port)
        {
            try
            {
                m_WinTimer = new System.Windows.Forms.Timer();
                m_WinTimer.Interval = 100;
                m_WinTimer.Enabled = true;
                m_WinTimer.Tick += m_WinTimer_Tick;

                m_Server = ScsServerFactory.CreateServer(new ScsTcpEndPoint(Port));

                m_Server.ClientConnected += m_Server_ClientConnected;
                m_Server.ClientDisconnected += m_Server_ClientDisconnected;
            }
            catch (Exception err)
            {
                m_Server = null;
                throw new Exception(err.Message);
            }
        }
        */

        public TcpServer(ISynchronizeInvoke synchronizingObject, int Port)
        {
            try
            {
                m_Timer = new System.Timers.Timer(100);
                m_Timer.SynchronizingObject = synchronizingObject;
                m_Timer.Enabled = true;
                m_Timer.Elapsed += m_Timer_Elapsed;

                m_Server = ScsServerFactory.CreateServer(new ScsTcpEndPoint(Port));

                m_Server.ClientConnected += m_Server_ClientConnected;
                m_Server.ClientDisconnected += m_Server_ClientDisconnected;
            }
            catch (Exception err)
            {
                m_Server = null;
                throw new Exception(err.Message);
            }
        }

        /// <summary>
        /// TCP 서버를 시작하는 기능을 제공합니다.
        /// </summary>
        public void Start()
        {
            try
            {
                m_Server.Start();
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
        }

        /// <summary>
        /// TCP 서버를 멈추는 기능을 제공합니다.
        /// </summary>
        public void Stop()
        {
            try
            {
                m_Server.Stop();
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
        }

        /// <summary>
        /// TCP 서버에 접속한 클라이언트 정보를 제공합니다.
        /// </summary>
        public Hik.Collections.ThreadSafeSortedList<long, IScsServerClient> Clients
        {
            get { return m_Server.Clients; }
        }

        /// <summary>
        /// 클라이언트가 접속시 발생하는 이벤트를 제공합니다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_Server_ClientConnected(object sender, ServerClientEventArgs e)
        {
            EventCreate(EventInfo.StatusType.CONNECTED, sender, (IScsServerClient)e.Client);

            e.Client.MessageReceived += Client_MessageReceived;
        }

        /// <summary>
        /// 클라이언트가 접속종료시 발생하는 이벤트를 제공합니다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_Server_ClientDisconnected(object sender, ServerClientEventArgs e)
        {
            EventCreate(EventInfo.StatusType.DISCONNECTED, sender, (IScsServerClient)e.Client);
        }

        /// <summary>
        /// 메세지를 수신받을때의 이벤트를 제공합니다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Client_MessageReceived(object sender, MessageEventArgs e)
        {
            EventCreate(EventInfo.StatusType.MESSAGERECEIVED, sender, (IScsMessage)e.Message);
        }

        /// <summary>
        /// 이벤트를 생성해서 큐에 넣는 기능을 제공합니다.
        /// </summary>
        /// <param name="Status"></param>
        /// <param name="Sender"></param>
        /// <param name="EventArgs"></param>
        private void EventCreate(EventInfo.StatusType Status, Object Sender, Object EventArgs)
        {
            EventInfo eventInfo = new EventInfo();
            eventInfo.Status = Status;
            eventInfo.Sender = Sender;
            eventInfo.EventArgs = EventArgs;

            m_EventList.Enqueue(eventInfo);
        }

        /// <summary>
        /// 기본타이머
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Event_Work();
        }

        /// <summary>
        /// Windows Timer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_WinTimer_Tick(object sender, EventArgs e)
        {
            Event_Work();
        }

        /// <summary>
        /// 큐에 넣어져 있는 이벤트를 가져와서 실제 이벤트를 발생시키는 기능을 수행합니다.
        /// </summary>
        private void Event_Work()
        {
            try
            {
                while (m_EventList.Count > 0)
                {
                    EventInfo eventInfo = new EventInfo();

                    eventInfo = (EventInfo)m_EventList.Dequeue();
                    IScsServerClient client = null;
                    IScsMessage message = null;

                    switch (eventInfo.Status)
                    {
                        // Client Connect
                        case EventInfo.StatusType.CONNECTED:
                            client = (IScsServerClient)eventInfo.EventArgs;
                            m_ClientConnectedEvent = new TcpServerClientConnectedEventArgs(client);
                            if (OnClientConnected != null)
                            {
                                OnClientConnected(this, m_ClientConnectedEvent);
                            }
                            break;

                        // Client DisConnect
                        case EventInfo.StatusType.DISCONNECTED:
                            client = (IScsServerClient)eventInfo.EventArgs;
                            m_ClientDisConnectedEvent = new TcpServerClientDisConnectedEventArgs(client);
                            if (OnClientDisConnected != null)
                            {
                                OnClientDisConnected(this, m_ClientDisConnectedEvent);
                            }
                            break;

                        // MessageReceived
                        case EventInfo.StatusType.MESSAGERECEIVED:
                            client = (IScsServerClient)eventInfo.Sender;
                            message = (IScsMessage)eventInfo.EventArgs;
                            m_MessageReceivedEvent = new TcpServerMessageReceivedEventArgs(client, message);
                            if (OnMessageReceived != null)
                            {
                                OnMessageReceived(this, m_MessageReceivedEvent);
                            }
                            break;
                    }
                }
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
        }
    }

    /// <summary>
    /// TCP 클라이언트를 제공합니다.
    /// </summary>
    public class TcpClient
    {
        /// <summary>
        /// TCP 클라이언트가 서버에 접속시 발생하는 이벤트를 제공합니다.
        /// </summary>
        public event TcpClientConnectedEventHandler OnConnected;

        /// <summary>
        /// TCP 클라이언트가 서버 접속종료시 발생하는 이벤트를 제공합니다.
        /// </summary>
        public event TcpClientDisConnectedEventHandler OnDisConnected;

        /// <summary>
        /// TCP 클라이언트가 메세지를 수신할때의 이벤트를 제공합니다.
        /// </summary>
        public event TcpClientMessageReceivedEventHandler OnMessageReceived;

        /// <summary>
        /// TCP 클라이언트가 메세지를 보냈을때의 이벤트를 제공합니다.
        /// </summary>
        public event TcpClientMessageSentEventHandler OnMessageSent;

        public enum STATUS { CONNECTED, DISCONNECTED };

        private TcpClientConnectedEventArgs m_ConnectedEvent = null;
        private TcpClientDisConnectedEventArgs m_DisConnectedEvent = null;
        private TcpClientMessageReceivedEventArgs m_MessageReceivedEvent = null;
        private TcpClientMessageSentEventArgs m_MessageSentEvent = null;

        private Queue<EventInfo> m_EventList = new Queue<EventInfo>();

        //private System.Windows.Forms.Timer m_WinTimer = null;
        private System.Timers.Timer m_Timer = null;

        private Hik.Communication.Scs.Client.IScsClient m_Client = null;

        /// <summary>
        /// 생성자를 제공합니다.
        /// </summary>
        /// <param name="Port">포트정보</param>
        /*
        public TcpClient(string ServerIp, int Port)
        {
            try
            {
                m_WinTimer = new System.Windows.Forms.Timer();
                m_WinTimer.Interval = 100;
                m_WinTimer.Enabled = true;
                m_WinTimer.Tick += m_WinTimer_Tick;

                m_Client = ScsClientFactory.CreateClient(new ScsTcpEndPoint(ServerIp, Port));

                m_Client.Connected += m_Client_Connected;
                m_Client.Disconnected += m_Client_Disconnected;
                m_Client.MessageReceived += m_Client_MessageReceived;
                m_Client.MessageSent += m_Client_MessageSent;
            }
            catch (Exception err)
            {
                m_Client = null;
                throw new Exception(err.Message);
            }
        }
        */

        public TcpClient(ISynchronizeInvoke synchronizingObject, string ServerIp, int Port)
        {
            try
            {
                m_Timer = new System.Timers.Timer(100);
                m_Timer.SynchronizingObject = synchronizingObject;
                m_Timer.Enabled = true;
                m_Timer.Elapsed += m_Timer_Elapsed;

                m_Client = ScsClientFactory.CreateClient(new ScsTcpEndPoint(ServerIp, Port));

                m_Client.Connected += m_Client_Connected;
                m_Client.Disconnected += m_Client_Disconnected;
                m_Client.MessageReceived += m_Client_MessageReceived;
                m_Client.MessageSent += m_Client_MessageSent;
            }
            catch (Exception err)
            {
                m_Client = null;
                throw new Exception(err.Message);
            }
        }

        /// <summary>
        /// TCP 클라이언트가 서버에 메세지를 보냈을때 이벤트를 제공합니다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_Client_MessageSent(object sender, MessageEventArgs e)
        {
            EventCreate(EventInfo.StatusType.MESSAGESENT, sender, (IScsMessage)e.Message);
        }

        /// <summary>
        /// TCP 클라이언트가 서버에 접속을 종료할때 이벤트를 제공합니다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_Client_Disconnected(object sender, EventArgs e)
        {
            EventCreate(EventInfo.StatusType.DISCONNECTED, sender, e);
        }

        /// <summary>
        /// TCP 클라이언트가 서버에 접속을 시작할때 이벤트를 제공합니다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_Client_Connected(object sender, EventArgs e)
        {
            EventCreate(EventInfo.StatusType.CONNECTED, sender, e);
        }

        /// <summary>
        /// TCP 클라이언트가 서버에서 메세지를 받을때 이벤트를 제공합니다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_Client_MessageReceived(object sender, MessageEventArgs e)
        {
            EventCreate(EventInfo.StatusType.MESSAGERECEIVED, sender, (IScsMessage)e.Message);
        }

        /// <summary>
        /// TCP 클라이언트가 서버에 접속하는 기능을 제공합니다.
        /// </summary>
        public void Connect()
        {
            try
            {
                m_Client.Connect();
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
        }

        /// <summary>
        /// TCP 클라이언트가 서버에 접속을 종료하는 기능을 제공합니다.
        /// </summary>
        public void DisConnect()
        {
            try
            {
                m_Client.Disconnect();
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
        }

        /// <summary>
        /// TCP 클라이언트가 서버에 메세지를 전송하는 기능을 제공합니다.
        /// </summary>
        /// <param name="Message"></param>
        public void SendMessage(string Message)
        {
            m_Client.SendMessage(new ScsTextMessage(Message));
        }

        /// <summary>
        /// TCP 클라이언트가 서버로부터 마지막 메세지를 받은 시간정보를 제공합니다.
        /// </summary>
        /// <returns></returns>
        public DateTime LastReceivedMessageTime()
        {
            return m_Client.LastReceivedMessageTime;
        }

        /// <summary>
        /// TCP 클라이언트가 서버에게 마지막 메세지를 보낸 시간정보를 제공합니다.
        /// </summary>
        /// <returns></returns>
        public DateTime LastSentMessageTime()
        {
            return m_Client.LastSentMessageTime;
        }

        /// <summary>
        /// TCP 클라이언트의 현재의 접속 상태정보를 제공합니다.
        /// </summary>
        public STATUS CommunicationState
        {
            get
            {
                STATUS retVal = STATUS.DISCONNECTED;

                if (m_Client.CommunicationState == Hik.Communication.Scs.Communication.CommunicationStates.Connected)
                {
                    return STATUS.CONNECTED;
                }

                return retVal;
            }
        }

        /// <summary>
        /// 이벤트를 생성해서 큐에 넣는 기능을 제공합니다.
        /// </summary>
        /// <param name="Status"></param>
        /// <param name="Sender"></param>
        /// <param name="EventArgs"></param>
        private void EventCreate(EventInfo.StatusType Status, Object Sender, Object EventArgs)
        {
            EventInfo eventInfo = new EventInfo();
            eventInfo.Status = Status;
            eventInfo.Sender = Sender;
            eventInfo.EventArgs = EventArgs;

            m_EventList.Enqueue(eventInfo);
        }

        /// <summary>
        /// Windows 타이머
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_WinTimer_Tick(object sender, EventArgs e)
        {
            Event_Work();
        }

        /// <summary>
        /// 기본타이머
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Event_Work();
        }

        /// <summary>
        /// 큐에 넣어져 있는 이벤트를 가져와서 실제 이벤트를 발생시키는 기능을 수행합니다.
        /// </summary>
        private void Event_Work()
        {
            try
            {
                while (m_EventList.Count > 0)
                {
                    EventInfo eventInfo = new EventInfo();

                    eventInfo = (EventInfo)m_EventList.Dequeue();
                    IScsMessage message = null;

                    switch (eventInfo.Status)
                    {
                        // Client Connect
                        case EventInfo.StatusType.CONNECTED:
                            m_ConnectedEvent = new TcpClientConnectedEventArgs();
                            if (OnConnected != null)
                            {
                                OnConnected(this, m_ConnectedEvent);
                            }
                            break;

                        // Client DisConnect
                        case EventInfo.StatusType.DISCONNECTED:
                            m_DisConnectedEvent = new TcpClientDisConnectedEventArgs();
                            if (OnDisConnected != null)
                            {
                                OnDisConnected(this, m_DisConnectedEvent);
                            }
                            break;

                        // MessageReceived
                        case EventInfo.StatusType.MESSAGERECEIVED:
                            message = (IScsMessage)eventInfo.EventArgs;
                            m_MessageReceivedEvent = new TcpClientMessageReceivedEventArgs(message);
                            if (OnMessageReceived != null)
                            {
                                OnMessageReceived(this, m_MessageReceivedEvent);
                            }
                            break;

                        // MessageSent
                        case EventInfo.StatusType.MESSAGESENT:
                            message = (IScsMessage)eventInfo.EventArgs;
                            m_MessageSentEvent = new TcpClientMessageSentEventArgs(message);
                            if (OnMessageSent != null)
                            {
                                OnMessageSent(this, m_MessageSentEvent);
                            }
                            break;
                    }
                }
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
        }
    }
}