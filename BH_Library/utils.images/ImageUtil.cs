﻿using System.Drawing;
using System.Drawing.Drawing2D;

namespace BH_Library.Utils.Images
{
    /// <summary>
    /// Image 파일과 관련된 유틸리티를 제공하는 클래스입니다.
    /// </summary>
    public class ImageUtil
    {
        /// <summary>
        /// 비트맵의 사이즈를 변경합니다.
        /// </summary>
        /// <param name="img">사이즈를 변경할 이미지입니다.</param>
        /// <param name="width">변경할 Width입니다.</param>
        /// <param name="height">변경할 Height입니다.</param>
        /// <returns>사이즈가 변경된 Bitmap 입니다.</returns>
        public static Bitmap ResizeImage(Bitmap img, int width, int height)
        {
            // get the height and width of the image
            int originalW = img.Width;
            int originalH = img.Height;

            // create a new Bitmap the size of the new image
            Bitmap bmp = new Bitmap(img, width, height);

            // create a new graphic from the Bitmap
            Graphics graphic = Graphics.FromImage((Image)bmp);
            graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;

            // draw the newly resized image
            graphic.DrawImage(img, 0, 0, width, height);

            // dispose and free up the resources
            graphic.Dispose();

            // return the image
            return bmp;
        }

        /// <summary>
        /// ResizeImageBy 메서드에서 사용되는 나열자입니다.
        /// 사이즈 변경시 기준을 나타냅니다.
        /// </summary>
        public enum ResizeCriteria
        {
            /// <summary>
            /// 가로폭을 기준으로 합니다.
            /// </summary>
            ByWidth,

            /// <summary>
            /// 세로높이를 기준으로 합니다.
            /// </summary>
            ByHeight
        }

        /// <summary>
        /// 비트맵의 세로 사이즈를 지정된 크기만큼 변경합니다.
        /// </summary>
        /// <param name="img">사이즈를 변경할 비트맵입니다.</param>
        /// <param name="criteriaLength">변경시킬 크기를 나타내는 int값 입니다.</param>
        /// <returns>사이즈가 변경된 비트맵을 리턴합니다.</returns>
        public static Bitmap ResizeImageBy(Bitmap img, int criteriaLength)
        {
            return ResizeImageBy(img, criteriaLength, ResizeCriteria.ByHeight);
        }

        /// <summary>
        /// 비트맵의 사이즈를 지정된 크기만큼 변경합니다.
        /// </summary>
        /// <param name="img">사이즈를 변경할 비트맵입니다.</param>
        /// <param name="criteriaLength">변경시킬 크기를 나타내는 int값 입니다.</param>
        /// <param name="byCriteria">가로폭 또는 세로 높이를 지정합니다.</param>
        /// <returns>사이즈가 변경된 비트맵을 리턴합니다.</returns>
        public static Bitmap ResizeImageBy(Bitmap img, int criteriaLength, ResizeCriteria byCriteria)
        {
            // get the height and width of the image
            int originalW = img.Width;
            int originalH = img.Height;

            // get the new size based on the percentage change
            int width = originalW;
            int height = originalH;

            //getRatio
            float ratio = 0f;

            if (byCriteria == ResizeCriteria.ByHeight)
            {
                float a = (float)criteriaLength / (float)originalH;
                ratio = a - 1f;

                width = originalW + (int)(originalW * ratio);
                height = criteriaLength;
            }
            else
            {
                float a = (float)criteriaLength / (float)originalW;
                ratio = a - 1f;

                width = criteriaLength;
                height = originalH + (int)(originalH * ratio);
            }

            // create a new Bitmap the size of the new image
            Bitmap bmp = new Bitmap(img, width, height);

            // create a new graphic from the Bitmap
            Graphics graphic = Graphics.FromImage((Image)bmp);
            graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;

            // draw the newly resized image
            graphic.DrawImage(img, 0, 0, width, height);

            // dispose and free up the resources
            graphic.Dispose();

            return bmp;
        }
    }
}